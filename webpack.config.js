let Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .autoProvidejQuery()
    .addEntry('admin', './assets/js/admin.js')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

if (Encore.isProduction()) {
    Encore.copyFiles([
        {
            from: 'assets/img',
            to: 'img/[path][name].[hash:8].[ext]'
        },
        {
            from: 'vendor/puikinsh/concept/assets/images',
            to: 'images/[path][name].[hash:8].[ext]'
        }
    ]);
} else {
    Encore.copyFiles([
        {
            from: 'assets/img',
            to: 'img/[path][name].[ext]'
        },
        {
            from: 'vendor/puikinsh/concept/assets/images',
            to: 'images/[path][name].[ext]'
        }
    ]);
}

const config = Encore.getWebpackConfig();
config.watchOptions = {
    poll: true,
};

config.resolve = {
    alias: {
        "markjs": "mark.js/dist/jquery.mark.js"
    }
};

module.exports = config;
