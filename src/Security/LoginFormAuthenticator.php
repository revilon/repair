<?php

declare(strict_types=1);

namespace App\Security;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\LoginForm;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private RouterInterface $router;
    private UserPasswordEncoderInterface $passwordEncoder;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;

    /**
     * @param RouterInterface $router
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        RouterInterface $router,
        UserPasswordEncoderInterface $passwordEncoder,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request): bool
    {
        return 'app_auth_logincheck' === $request->attributes->get('_route') && $request->isMethod('POST');
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        $loginForm = $this->formFactory->create(LoginForm::class);
        $loginForm->handleRequest($request);

        if (!$loginForm->isSubmitted()) {
            $loginForm->addError(new FormError('Форма не была отправлена'));
        }

        if (!$loginForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($loginForm);

            throw new FormBadRequestHttpException($errorList, 'Форма отправлена некорректно');
        }

        $session = $request->getSession();

        if ($session) {
            $session->set(Security::LAST_USERNAME, $loginForm->getData()['username']);
        }

        return $loginForm->getData();
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($loginFormData, UserProviderInterface $userProvider): UserInterface
    {
        return $userProvider->loadUserByUsername($loginFormData['username']);
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($loginFormData, UserInterface $user): bool
    {
        if (!$this->passwordEncoder->isPasswordValid($user, $loginFormData['password'])) {
            throw new AuthenticationException('Неверный пароль');
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new JsonResponse([
                'redirect_url' => $targetPath,
            ]);
        }

        return new JsonResponse([
            'redirect_url' => $this->router->generate('app_profile_index'),
        ]);
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): void
    {
        throw new FormBadRequestHttpException([], $exception->getMessage());
    }

    /**
     * {@inheritdoc}
     */
    protected function getLoginUrl(): string
    {
        return $this->router->generate('app_auth_login');
    }
}
