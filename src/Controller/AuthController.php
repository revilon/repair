<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\LoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    private AuthenticationUtils $authenticationUtils;
    private FormErrorSerializer $formErrorSerializer;

    /**
     * @required
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function dependencyInjection(
        AuthenticationUtils $authenticationUtils,
        FormErrorSerializer $formErrorSerializer
    ): void {
        $this->authenticationUtils = $authenticationUtils;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @Route("/admin/login", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request): Response
    {
        $loginForm = $this->createForm(LoginForm::class);

        $lastAuthenticationError = $this->authenticationUtils->getLastAuthenticationError();

        if ($lastAuthenticationError) {
            $message = $lastAuthenticationError->getMessage();

            $loginForm->addError(new FormError($message));
        }

        $loginForm->handleRequest($request);

        return $this->render('Admin/login.html.twig', [
            'form' => $loginForm->createView(),
        ]);
    }

    /**
     * @Route("/admin/api/login/check", methods={"POST"})
     */
    public function loginCheck(): void
    {
    }

    /**
     * @Route("/admin/logout", methods={"GET"})
     */
    public function logout(): void
    {
    }
}
