<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\StatisticCalculateCostSparePart\StatisticCalculateCostSparePartHandler;
use App\UseCase\StatisticCalculateNetProfit\StatisticCalculateNetProfitHandler;
use App\UseCase\StatisticCalculateRepairerPay\StatisticCalculateRepairerPayHandler;
use App\UseCase\StatisticCalculateTotalPrice\StatisticCalculateTotalPriceHandler;
use App\UseCase\StatisticCostSparePart\StatisticCostSparePartHandler;
use App\UseCase\StatisticEfficiencyBar\StatisticEfficiencyBarHandler;
use App\UseCase\StatisticIndex\StatisticIndexHandler;
use App\UseCase\StatisticNetProfitChart\StatisticNetProfitChartHandler;
use App\UseCase\StatisticTotalPrice\StatisticTotalPriceHandler;
use Doctrine\DBAL\DBALException;
use Exception;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatisticController extends AbstractController
{
    /**
     * @Route("/admin/statistic", methods={"GET"})
     *
     * @param StatisticIndexHandler $handler
     *
     * @return Response
     *
     * @throws Exception
     */
    public function index(StatisticIndexHandler $handler): Response
    {
        $result = $handler->handle();

        return $this->render('Admin/statistic.html.twig', [
            'name_page' => 'Статистика',
            'statistic_repairer_pay_form' => $result['statistic_repairer_pay_form'],
            'statistic_net_profit_form' => $result['statistic_net_profit_form'],
            'statistic_cost_spare_part_form' => $result['statistic_cost_spare_part_form'],
            'statistic_total_price_form' => $result['statistic_total_price_form'],
        ]);
    }

    /**
     * @Route("/admin/api/statistic/efficiencyBar", methods={"GET"})
     *
     * @param StatisticEfficiencyBarHandler $handler
     *
     * @return JsonResponse
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function efficiencyBar(StatisticEfficiencyBarHandler $handler): JsonResponse
    {
        $result = $handler->handle();

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/calculateRepairerPay", methods={"GET"})
     *
     * @param StatisticCalculateRepairerPayHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function calculateRepairerPay(StatisticCalculateRepairerPayHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/netProfitChart", methods={"GET"})
     *
     * @param StatisticNetProfitChartHandler $handler
     *
     * @return JsonResponse
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function netProfitChart(StatisticNetProfitChartHandler $handler): JsonResponse
    {
        $result = $handler->handle();

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/calculateNetProfit", methods={"GET"})
     *
     * @param StatisticCalculateNetProfitHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function calculateNetProfit(StatisticCalculateNetProfitHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/costSparePartChart", methods={"GET"})
     *
     * @param StatisticCostSparePartHandler $handler
     *
     * @return JsonResponse
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function costSparePartChart(StatisticCostSparePartHandler $handler): JsonResponse
    {
        $result = $handler->handle();

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/calculateCostSparePart", methods={"GET"})
     *
     * @param StatisticCalculateCostSparePartHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function calculateCostSparePart(
        StatisticCalculateCostSparePartHandler $handler,
        Request $request
    ): JsonResponse {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/totalPriceChart", methods={"GET"})
     *
     * @param StatisticTotalPriceHandler $handler
     *
     * @return JsonResponse
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function totalPriceChart(StatisticTotalPriceHandler $handler): JsonResponse
    {
        $result = $handler->handle();

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/statistic/calculateTotalPrice", methods={"GET"})
     *
     * @param StatisticCalculateTotalPriceHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function calculateTotalPrice(
        StatisticCalculateTotalPriceHandler $handler,
        Request $request
    ): JsonResponse {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }
}
