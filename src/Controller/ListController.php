<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\ListIndex\ListIndexHandler;
use App\UseCase\ListTableFilter\ListTableHandler;
use App\UseCase\ListTableWaitFilter\ListTableWaitHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/admin/list", methods={"GET"})
     *
     * @param ListIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     */
    public function index(ListIndexHandler $handler, Request $request): Response
    {
        $handler->handle($request);

        return $this->render('Admin/list.html.twig', [
            'name_page' => 'Список',
        ]);
    }

    /**
     * @Route("/admin/api/list/table", methods={"GET"})
     *
     * @param ListTableHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function table(ListTableHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/api/list/tableWait", methods={"GET"})
     *
     * @param ListTableWaitHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function tableWait(ListTableWaitHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }
}
