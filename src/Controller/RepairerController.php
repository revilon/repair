<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\RepairerIndex\RepairerIndexHandler;
use App\UseCase\RepairerReceiptAgo\RepairerReceiptAgoHandler;
use App\UseCase\RepairerSave\RepairerSaveHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RepairerController extends AbstractController
{
    /**
     * @Route("/admin/repairer", methods={"GET"})
     *
     * @param RepairerIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function repairer(RepairerIndexHandler $handler, Request $request): Response
    {
        $result = $handler->handle($request);

        return $this->render('Admin/repairer.html.twig', [
            'name_page' => 'Комната мастера',
            'search_receipt_form' => $result['search_receipt_form'],
            'receipt' => $result['receipt'] ?? null,
            'repairer_work_form' => $result['repairer_work_form'] ?? null,
            'repairer_name' => $result['repairer_name'] ?? null,
            'receipt_status' => $result['receipt_status'] ?? null,
        ]);
    }

    /**
     * @Route("/admin/api/repairer/save", methods={"POST"})
     *
     * @param RepairerSaveHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function repairerSave(RepairerSaveHandler $handler, Request $request): JsonResponse
    {
        $handler->handle($request);

        return new JsonResponse();
    }

    /**
     * @Route("/admin/api/repairer/receiptAgo", methods={"GET"})
     *
     * @param RepairerReceiptAgoHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptAgo(RepairerReceiptAgoHandler $handler, Request $request): JsonResponse
    {
        $orderId = $request->query->getInt('orderId');

        $result = $handler->handle($orderId);

        return new JsonResponse([
            'ago_text' => $result,
        ]);
    }
}
