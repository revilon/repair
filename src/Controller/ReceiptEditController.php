<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\EditIndex\EditIndexHandler;
use App\UseCase\EditReceiptAgo\EditReceiptAgoHandler;
use App\UseCase\EditReceiptSave\EditReceiptSaveHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReceiptEditController extends AbstractController
{
    /**
     * @Route("/admin/edit", methods={"GET"})
     *
     * @param EditIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     * @throws DBALException
     */
    public function edit(EditIndexHandler $handler, Request $request): Response
    {
        $result = $handler->handle($request);

        return $this->render('Admin/edit.html.twig', [
            'name_page' => 'Редактор',
            'receipt_edit_search_form' => $result['receipt_edit_search_form'],
            'receipt_edit_form' => $result['receipt_edit_form'] ?? null,
            'receipt_order_id' => $result['receipt_order_id'] ?? null,
            'receipt' => $result['receipt'] ?? null,
        ]);
    }

    /**
     * @Route("/admin/api/edit/receiptAgo", methods={"GET"})
     *
     * @param EditReceiptAgoHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptAgo(EditReceiptAgoHandler $handler, Request $request): JsonResponse
    {
        $orderId = $request->query->getInt('orderId');

        $result = $handler->handle($orderId);

        return new JsonResponse([
            'ago_text' => $result,
        ]);
    }

    /**
     * @Route("/admin/api/edit/saveReceipt", methods={"POST"})
     *
     * @param EditReceiptSaveHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function editReceiptSave(EditReceiptSaveHandler $handler, Request $request): JsonResponse
    {
        $handler->handle($request);

        return new JsonResponse();
    }
}
