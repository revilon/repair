<?php

namespace App\Controller;

use App\UseCase\CalculateRepairerPay\CalculateRepairerPayHandler;
use App\UseCase\ProfileIndex\ProfileIndexHandler;
use Doctrine\DBAL\DBALException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/admin")
     *
     * @param ProfileIndexHandler $handler
     *
     * @return Response
     *
     * @throws Exception
     */
    public function index(ProfileIndexHandler $handler): Response
    {
        $result = $handler->handle();

        return $this->render('Admin/profile.html.twig', [
            'name_page' => 'Профиль',
            'repairer_name' => $result['repairer_name'],
            'repairer_pay' => $result['repairer_pay'],
            'repairer_pay_total' => $result['repairer_pay_total'],
            'repairer_efficiency' => $result['efficiency_repairer'],
            'current_month' => $result['current_month'],
        ]);
    }

    /**
     * @Route("/admin/api/calculate/repairerPay")
     *
     * @param CalculateRepairerPayHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function calculateRepairerPay(CalculateRepairerPayHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }
}
