<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\AdministrationActiveUser\AdministrationActiveUserHandler;
use App\UseCase\AdministrationCreateUser\AdministrationCreateUserHandler;
use App\UseCase\AdministrationDisableUser\AdministrationDisableUserHandler;
use App\UseCase\AdministrationEditUser\AdministrationEditUserHandler;
use App\UseCase\AdministrationEditUserIndex\AdministrationEditUserIndexHandler;
use App\UseCase\AdministrationIndex\AdministrationIndexHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdministrationController extends AbstractController
{
    /**
     * @Route("/admin/administration", methods={"GET"})
     *
     * @param AdministrationIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function index(AdministrationIndexHandler $handler, Request $request): Response
    {
        $result = $handler->handle();

        return $this->render('Admin/administration.html.twig', [
            'name_page' => 'Администрирование',
            'create_user_form' => $result['create_user_form'],
            'user_list' => $result['user_list'],
        ]);
    }

    /**
     * @Route("/admin/administration/editUser/{userId}", methods={"GET"})
     *
     * @param AdministrationEditUserIndexHandler $handler
     * @param string $userId
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function editUserIndex(AdministrationEditUserIndexHandler $handler, string $userId): Response
    {
        $result = $handler->handle($userId);

        return $this->render('Admin/administration_edit_user.html.twig', [
            'name_page' => 'Редактирование пользователя',
            'edit_user_form' => $result['edit_user_form'],
        ]);
    }

    /**
     * @Route("/admin/api/administration/createUser", methods={"POST"})
     *
     * @param AdministrationCreateUserHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function createUser(AdministrationCreateUserHandler $handler, Request $request): JsonResponse
    {
        $handler->handle($request);

        return new JsonResponse();
    }

    /**
     * @Route("/admin/api/administration/disableUser/{userId}", methods={"POST"})
     *
     * @param AdministrationDisableUserHandler $handler
     * @param string $userId
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function disableUser(AdministrationDisableUserHandler $handler, string $userId): JsonResponse
    {
        $handler->handle($userId);

        return new JsonResponse();
    }

    /**
     * @Route("/admin/api/administration/activeUser/{userId}", methods={"POST"})
     *
     * @param AdministrationActiveUserHandler $handler
     * @param string $userId
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function activeUser(AdministrationActiveUserHandler $handler, string $userId): JsonResponse
    {
        $handler->handle($userId);

        return new JsonResponse();
    }

    /**
     * @Route("/admin/api/administration/editUser/{userId}", methods={"POST"})
     *
     * @param AdministrationEditUserHandler $handler
     * @param string $userId
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function editUser(AdministrationEditUserHandler $handler, string $userId, Request $request): JsonResponse
    {
        $handler->handle($userId, $request);

        return new JsonResponse();
    }
}
