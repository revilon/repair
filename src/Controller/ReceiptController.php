<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Manager\Executer\RowManager;
use App\UseCase\DeviceOwnerNameList\DeviceOwnerNameListHandler;
use App\UseCase\ReceiptBrandList\ReceiptBrandListHandler;
use App\UseCase\ReceiptDeviceImeiList\ReceiptDeviceImeiListHandler;
use App\UseCase\ReceiptDeviceOwnerAddressList\ReceiptDeviceOwnerAddressListHandler;
use App\UseCase\ReceiptDeviceSerialNumberList\ReceiptDeviceSerialNumberListHandler;
use App\UseCase\ReceiptIndex\ReceiptIndexHandler;
use App\UseCase\ReceiptModelList\ReceiptModelListHandler;
use App\UseCase\ReceiptPrint\ReceiptPrintHandler;
use App\UseCase\ReceiptSave\ReceiptSaveHandler;
use App\UseCase\RepairerWorkPrint\RepairerWorkPrintHandler;
use Doctrine\DBAL\DBALException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class ReceiptController extends AbstractController
{
    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/admin/receipt", methods={"GET"})
     *
     * @param ReceiptIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     * @throws DBALException
     */
    public function receipt(ReceiptIndexHandler $handler, Request $request): Response
    {
        $result = $handler->handle($request);

        return $this->render('Admin/receipt.html.twig', [
            'name_page' => 'Квитанция',
            'form_receipt' => $result['form_receipt'],
            'supposed_number_receipt' => $result['supposed_number_receipt'],
        ]);
    }

    /**
     * @Route("/admin/api/receipt/save", methods={"POST"})
     *
     * @param ReceiptSaveHandler $handler
     * @param Request $request
     * @param RowManager $manager
     *
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function receiptSave(ReceiptSaveHandler $handler, Request $request, RowManager $manager): JsonResponse
    {
        $manager->beginTransaction();

        try {
            $result = $handler->handle($request);

            $manager->commit();
        } catch (Throwable $exception) {
            $manager->rollBack();
            $this->logger->info('Request receipt save', $request->request->all());

            throw $exception;
        }

        return new JsonResponse([
            'receipt_order_id' => $result['orderId'],
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceOwnerNameList", methods={"GET"})
     *
     * @param DeviceOwnerNameListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptDeviceOwnerNameList(DeviceOwnerNameListHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('search_text');

        $deviceOwnerNameList = $handler->handle($searchText);

        return new JsonResponse([
            'status' => 'success',
            'device_owner_name_list' => $deviceOwnerNameList,
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceBrandList", methods={"GET"})
     *
     * @param ReceiptBrandListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptDeviceBrandList(ReceiptBrandListHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('search_text');
        $deviceType = $request->get('device_type');

        $brandList = $handler->handle($searchText, $deviceType);

        return new JsonResponse([
            'status' => 'success',
            'device_brand_list' => $brandList,
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceModelList", methods={"GET"})
     *
     * @param ReceiptModelListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptDeviceModelList(ReceiptModelListHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('search_text');
        $deviceType = $request->get('device_type');
        $brand = $request->get('brand');

        $modelList = $handler->handle($searchText, $deviceType, $brand);

        return new JsonResponse([
            'status' => 'success',
            'device_model_list' => $modelList,
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceImeiList", methods={"GET"})
     *
     * @param ReceiptDeviceImeiListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptDeviceImeiList(ReceiptDeviceImeiListHandler $handler, Request $request): JsonResponse
    {
        $searchText = $request->get('search_text');
        $deviceType = $request->get('device_type');
        $brand = $request->get('brand');
        $model = $request->get('model');

        $deviceImeiList = $handler->handle($searchText, $deviceType, $brand, $model);

        return new JsonResponse([
            'status' => 'success',
            'device_imei_list' => $deviceImeiList,
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceSerialNumberList", methods={"GET"})
     *
     * @param ReceiptDeviceSerialNumberListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptDeviceSerialNumberList(
        ReceiptDeviceSerialNumberListHandler $handler,
        Request $request
    ): JsonResponse {
        $searchText = $request->get('search_text');
        $deviceType = $request->get('device_type');
        $brand = $request->get('brand');
        $model = $request->get('model');
        $imei = $request->get('imei');

        $deviceSerialNumberList = $handler->handle($searchText, $deviceType, $brand, $model, $imei);

        return new JsonResponse([
            'status' => 'success',
            'device_serial_number_list' => $deviceSerialNumberList,
        ]);
    }

    /**
     * @Route("/admin/api/receipt/deviceOwnerAddressList", methods={"GET"})
     *
     * @param ReceiptDeviceOwnerAddressListHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DadataException
     */
    public function receiptDeviceOwnerAddressList(
        ReceiptDeviceOwnerAddressListHandler $handler,
        Request $request
    ): JsonResponse {
        $searchText = $request->get('search_text');

        $deviceOwnerAddressList = $handler->handle($searchText);

        return new JsonResponse([
            'status' => 'success',
            'device_owner_address_list' => $deviceOwnerAddressList,
        ]);
    }

    /**
     * @Route("/admin/receipt/print/{orderId}", methods={"GET"}, defaults={"orderId"=0})
     *
     * @param ReceiptPrintHandler $handler
     * @param int $orderId
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function receiptPrint(ReceiptPrintHandler $handler, int $orderId): Response
    {
        $result = $handler->handle($orderId);

        return $this->render('Admin/receipt_print.html.twig', [
            'receipt' => $result,
        ]);
    }

    /**
     * @Route("/admin/receipt/repairerWorkPrint/{orderId}", methods={"GET"})
     *
     * @param RepairerWorkPrintHandler $handler
     * @param int $orderId
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function repairerWorkPrint(RepairerWorkPrintHandler $handler, int $orderId): Response
    {
        $result = $handler->handle($orderId);

        return $this->render('Admin/repairer_work_print.html.twig', [
            'receipt' => $result,
        ]);
    }
}
