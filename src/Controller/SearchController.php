<?php

declare(strict_types=1);

namespace App\Controller;

use App\UseCase\GuaranteeReturn\GuaranteeReturnHandler;
use App\UseCase\SearchIndex\SearchIndexHandler;
use App\UseCase\SearchReceiptAgo\SearchReceiptAgoHandler;
use App\UseCase\SearchSmartTable\SearchSmartTableHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/admin/search", methods={"GET"})
     *
     * @param SearchIndexHandler $handler
     * @param Request $request
     *
     * @return Response
     *
     * @throws DBALException
     */
    public function search(SearchIndexHandler $handler, Request $request): Response
    {
        $result = $handler->handle($request);

        return $this->render('Admin/search.html.twig', [
            'name_page' => 'Поиск',
            'search_receipt_form' => $result['search_receipt_form'],
            'receipt' => $result['receipt'] ?? null,
            'receipt_status' => $result['receipt_status'] ?? null,
            'guarantee_return_form' => $result['guarantee_return_form'] ?? null,
            'order_id' => $result['order_id'] ?? null,
        ]);
    }

    /**
     * @Route("/admin/api/search/guaranteeReturn", methods={"POST"})
     *
     * @param GuaranteeReturnHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function guaranteeReturn(GuaranteeReturnHandler $handler, Request $request): JsonResponse
    {
        $handler->handle($request);

        return new JsonResponse();
    }

    /**
     * @Route("/admin/api/search/receiptAgo", methods={"GET"})
     *
     * @param SearchReceiptAgoHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function receiptAgo(SearchReceiptAgoHandler $handler, Request $request): JsonResponse
    {
        $orderId = $request->query->getInt('orderId');

        $result = $handler->handle($orderId);

        return new JsonResponse([
            'ago_text' => $result,
        ]);
    }

    /**
     * @Route("/admin/api/search/smartTable", methods={"GET"})
     *
     * @param SearchSmartTableHandler $handler
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function smartTable(SearchSmartTableHandler $handler, Request $request): JsonResponse
    {
        $result = $handler->handle($request);

        return new JsonResponse($result);
    }
}
