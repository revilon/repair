<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Exception\FormBadRequestHttpException;
use App\Guesser\AreaGuesser;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public const FORM_ERROR_DATA_KEY = 'error_list';
    public const FORM_ERROR_MESSAGE_KEY = 'error_message';

    private AreaGuesser $areaGuesser;
    private LoggerInterface $logger;
    private string $environment;

    /**
     * @param AreaGuesser $areaGuesser
     * @param LoggerInterface $logger
     * @param string $environment
     */
    public function __construct(
        AreaGuesser $areaGuesser,
        LoggerInterface $logger,
        string $environment
    ) {
        $this->areaGuesser = $areaGuesser;
        $this->logger = $logger;
        $this->environment = $environment;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['adminFormApiException', 10],
            ],
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function adminFormApiException(ExceptionEvent $event): void
    {
        $request = $event->getRequest();

        $isApiAdminRequest = $this->areaGuesser->isApiAdminRequest($request);

        if (!$isApiAdminRequest) {
            return;
        }

        $throwable = $event->getThrowable();

        switch (true) {
            case $throwable instanceof FormBadRequestHttpException:
                $errorList = $throwable->getFormErrorList();

                $response = new JsonResponse([
                    self::FORM_ERROR_DATA_KEY => $errorList,
                    self::FORM_ERROR_MESSAGE_KEY => $throwable->getMessage(),
                ], Response::HTTP_BAD_REQUEST);

                break;
            default:
                $data = [
                    self::FORM_ERROR_DATA_KEY => [],
                    self::FORM_ERROR_MESSAGE_KEY => 'Непредвиденная ошибка',
                ];

                if ($this->environment === 'dev') {
                    $data += [
                        'exception_code' => $throwable->getCode(),
                        'exception_file' => $throwable->getFile(),
                        'exception_line' => $throwable->getLine(),
                        'exception_message' => $throwable->getMessage(),
                        'exception_trace' => explode("\n", $throwable->getTraceAsString()),
                    ];
                }

                $response = new JsonResponse($data, Response::HTTP_INTERNAL_SERVER_ERROR);

                $this->logger->critical($throwable->getMessage(), explode("\n", $throwable->getTraceAsString()));
        }

        $event->setResponse($response);
    }
}
