<?php

declare(strict_types=1);

namespace App\Component\Dadata\Dto;

class FullNameDto
{
    private string $fullName;
    private ?string $surname;
    private ?string $name;
    private ?string $patronymic;
    private ?string $gender;

    /**
     * @param string $fullName
     * @param string|null $surname
     * @param string|null $name
     * @param string|null $patronymic
     * @param string|null $gender
     */
    public function __construct(
        string $fullName,
        ?string $surname,
        ?string $name,
        ?string $patronymic,
        ?string $gender
    ) {
        $this->fullName = $fullName;
        $this->surname = $surname;
        $this->name = $name;
        $this->patronymic = $patronymic;
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }
}
