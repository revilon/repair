<?php

declare(strict_types=1);

namespace App\Component\Dadata\Dto;

class AddressDto
{
    private string $address;

    /**
     * @param string $address
     */
    public function __construct(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }
}
