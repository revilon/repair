<?php

declare(strict_types=1);

namespace App\Component\Dadata\Exception;

use Exception;

class DadataException extends Exception
{
}
