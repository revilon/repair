<?php

namespace App\Component\IdGenerator\Traits;

use App\Component\IdGenerator\IdGenerator;
use Doctrine\ORM\Mapping as ORM;

trait IdEntityAwareTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=IdGenerator::class)
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}
