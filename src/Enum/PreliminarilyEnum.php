<?php

declare(strict_types=1);

namespace App\Enum;

class PreliminarilyEnum extends AbstractEnum
{
    public const IS_ENABLE = 'is_enable';
    public const IS_LIQUID = 'is_liquid';
    public const IS_STRIKE = 'is_strike';

    /**
     * @return string
     */
    public static function getTranslatePrefix(): string
    {
        return 'receipt.preliminarily.';
    }
}
