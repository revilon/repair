<?php

declare(strict_types=1);

namespace App\Enum;

class FlashTypeEnum extends AbstractEnum
{
    public const PRIMARY = 'primary';
    public const SECONDARY = 'secondary';
    public const SUCCESS = 'success';
    public const DANGER = 'danger';
    public const WARNING = 'warning';
    public const INFO = 'info';
    public const LIGHT = 'light';
    public const DARK = 'dark';
}
