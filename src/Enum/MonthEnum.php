<?php

declare(strict_types=1);

namespace App\Enum;

use ReflectionException;

class MonthEnum extends AbstractEnum
{
    public const JANUARY = 'январь';
    public const FEBRUARY = 'февраль';
    public const MARCH = 'март';
    public const APRIL = 'апрель';
    public const MAY = 'май';
    public const JUNE = 'июнь';
    public const JULY = 'июль';
    public const AUGUST = 'август';
    public const SEPTEMBER = 'сентябрь';
    public const OCTOBER = 'октябрь';
    public const NOVEMBER = 'ноябрь';
    public const DECEMBER = 'декабрь';

    /**
     * @return array
     *
     * @throws ReflectionException
     */
    public static function getNumberNameMonthList(): array
    {
        $monthList = static::getList();

        return array_combine(
            range(1, count($monthList)),
            array_values($monthList)
        );
    }

    /**
     * @param int $monthNumber
     *
     * @return string
     *
     * @throws ReflectionException
     */
    public static function getNameMonth(int $monthNumber): string
    {
        return self::getNumberNameMonthList()[$monthNumber];
    }
}
