<?php

declare(strict_types=1);

namespace App\Enum;

class MonthShortEnum extends MonthEnum
{
    public const JANUARY = 'янв';
    public const FEBRUARY = 'фев';
    public const MARCH = 'мар';
    public const APRIL = 'апр';
    public const MAY = 'май';
    public const JUNE = 'июн';
    public const JULY = 'июл';
    public const AUGUST = 'авг';
    public const SEPTEMBER = 'сен';
    public const OCTOBER = 'окт';
    public const NOVEMBER = 'ноя';
    public const DECEMBER = 'дек';
}
