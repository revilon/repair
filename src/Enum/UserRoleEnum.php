<?php

declare(strict_types=1);

namespace App\Enum;

use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

class UserRoleEnum extends AbstractEnum
{
    public const IS_AUTHENTICATED_ANONYMOUSLY = AuthenticatedVoter::IS_AUTHENTICATED_ANONYMOUSLY;
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SELLER = 'ROLE_SELLER';
    public const ROLE_REPAIRER = 'ROLE_REPAIRER';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @return string
     */
    public static function getTranslatePrefix(): string
    {
        return 'user.role.';
    }
}
