<?php

declare(strict_types=1);

namespace App\Enum;

class GenderEnum extends AbstractEnum
{
    public const MALE = 'male';
    public const FEMALE = 'female';
}
