<?php

declare(strict_types=1);

namespace App\Enum;

class ReceiptStatusEnum extends AbstractEnum
{
    public const NEW = 'new';
    public const WORK = 'work';
    public const REPAIR_SUCCESS_SHOP = 'repair_success_shop';
    public const REPAIR_FAIL_SHOP = 'repair_fail_shop';
    public const REPAIR_FAIL_OWNER = 'repair_fail_owner';
    public const REPAIR_SUCCESS_OWNER = 'repair_success_owner';
    public const GUARANTEE_RETURN = 'guarantee_return';

    /**
     * @return string
     */
    public static function getTranslatePrefix(): string
    {
        return 'receipt.repair_status.';
    }
}
