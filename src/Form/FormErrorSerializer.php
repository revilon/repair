<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;

class FormErrorSerializer
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function getFormErrorList(FormInterface $form): array
    {
        return $this->recursiveFormErrorList($form->getErrors(true, false), [$form->getName()]);
    }

    /**
     * @param FormErrorIterator $formErrorList
     * @param array $prefixList
     *
     * @return array
     */
    private function recursiveFormErrorList(FormErrorIterator $formErrorList, array $prefixList): array
    {
        $errorList = [];

        foreach ($formErrorList as $formError) {
            if ($formError instanceof FormErrorIterator) {
                $errorList = array_merge(
                    $errorList,
                    $this->recursiveFormErrorList(
                        $formError,
                        array_merge(
                            $prefixList,
                            [$formError->getForm()->getName()])
                    )
                );
            } elseif ($formError instanceof FormError) {
                $errorList[implode('_', $prefixList)][] = $formError->getMessage();
            }
        }

        return $errorList;
    }
}
