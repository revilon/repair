<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginForm extends AbstractType
{
    private AuthenticationUtils $authenticationUtils;
    private RouterInterface $router;

    /**
     * @required
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param RouterInterface $router
     */
    public function dependencyInjection(
        AuthenticationUtils $authenticationUtils,
        RouterInterface $router
    ): void {
        $this->authenticationUtils = $authenticationUtils;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $loginCheckUrl = $this->router->generate('app_auth_logincheck');

        $builder
            ->setMethod('POST')
            ->setAction($loginCheckUrl)
            ->add('username', null, [
                'label' => false,
                'row_attr' => [
                    'class' => 'form-group',
                ],
                'required' => true,
                'attr' => [
                    'autofocus' => 'autofocus',
                    'autocomplete' => 'off',
                    'class' => 'form-control form-control-lg',
                    'placeholder' => 'Логин',
                    'value' => $this->authenticationUtils->getLastUsername(),
                ]
            ])
            ->add('password', PasswordType::class, [
                'label' => false,
                'row_attr' => [
                    'class' => 'form-group',
                ],
                'required' => true,
                'attr' => [
                    'class' => 'form-control form-control-lg',
                    'placeholder' => 'Пароль',
                ]
            ])
            ->add('_remember_me', CheckboxType::class, [
                'label' => 'Запомнить',
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'checked' => 'checked',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Войти',
                'attr' => [
                    'class' => 'btn btn-primary btn-lg btn-block button-login',
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'login-form',
            ],
            'csrf_protection' => true,
            'csrf_token_id' => 'authenticate',
        ]);
    }
}
