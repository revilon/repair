<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\PreliminarilyEnum;
use App\Enum\ReceiptStatusEnum;
use App\UseCase\EditIndex\EditIndexHandler;
use DateTime;
use Doctrine\DBAL\DBALException;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class ReceiptEditForm extends AbstractType
{
    private RouterInterface $router;
    private EditIndexHandler $handler;

    /**
     * @param RouterInterface $router
     * @param EditIndexHandler $handler
     */
    public function __construct(RouterInterface $router, EditIndexHandler $handler)
    {
        $this->router = $router;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     *
     * @throws ReflectionException
     * @throws DBALException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $actionUrl = $this->router->generate('app_receiptedit_editreceiptsave');
        $callbackFullNameUrl = $this->router->generate('app_receipt_receiptdeviceownernamelist');
        $callbackAddressUrl = $this->router->generate('app_receipt_receiptdeviceowneraddresslist');

        $builder
            ->setAction($actionUrl)
            ->add(
                $builder
                    ->create('DeviceOwner', FormType::class, [
                        'label' => false,
                    ])
                    ->add('id', HiddenType::class)
                    ->add('name', TextType::class, [
                        'label' => 'ФИО владелеца',
                        'row_attr' => [
                            'class' => 'typeahead__container',
                        ],
                        'attr' => [
                            'class' => 'js-typeahead full-name',
                            'autocomplete' => 'off',
                            'placeholder' => 'ФИО',
                            'data-callback' => $callbackFullNameUrl,
                        ],
                        'constraints' => [
                            new NotBlank(),
                            new Length(['min' => 3, 'max' => 100]),
                        ],
                    ])
                    ->add('address', TextType::class, [
                        'label' => 'Адрес проживания владельца',
                        'required' => false,
                        'row_attr' => [
                            'class' => 'typeahead__container',
                        ],
                        'attr' => [
                            'class' => 'js-typeahead address',
                            'autocomplete' => 'off',
                            'placeholder' => 'Адрес',
                            'data-callback' => $callbackAddressUrl,
                        ],
                    ])
                    ->add('phone', TextType::class, [
                        'label' => 'Номер телефона владельца',
                        'required' => false,
                        'attr' => [
                            'autocomplete' => 'off',
                            'placeholder' => 'Телефон',
                        ],
                    ])
                    ->add('email', TextType::class, [
                        'label' => 'Email владельца',
                        'required' => false,
                        'attr' => [
                            'autocomplete' => 'off',
                            'placeholder' => 'Email',
                        ],
                        'constraints' => [
                            new Email(),
                        ],
                    ])
            )
            ->add('DeviceCollection', CollectionType::class, [
                'label' => false,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        'class' => 'tab-pane fade device-tab',
                        'role' => 'tabpanel',
                        'aria-labelledby' => 'tab-__name__',
                    ],
                ],
                'entry_type' => ReceiptEditDeviceForm::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
            ])
            ->add('defect', TextType::class, [
                'label' => 'Дефект',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Неисправность',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3]),
                ],

            ])
            ->add('preliminarilyList', ChoiceType::class, [
                'label' => 'Предварительно',
                'required' => false,
                'choices' => PreliminarilyEnum::getListCombine(),
                'attr' => [
                    'class' => 'preliminarilyList',
                    'autocomplete' => 'off',
                    'placeholder' => 'Предварительно',
                ],
                'multiple' => true,
                'choice_label' => fn($choice) => PreliminarilyEnum::getTranslatePrefix() . $choice,
            ])
            ->add('deviceCode', TextType::class, [
                'label' => 'Код доступа',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Код доступа',
                ],
            ])
            ->add('devicePreview', TextType::class, [
                'label' => 'Внешнее состояние',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Внешнее состояние',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 500]),
                ],
            ])
            ->add('prePrice', IntegerType::class, [
                'label' => 'Оценка стоимости',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость ремонта',
                ],
                'constraints' => [
                    new NotBlank(),
                    new PositiveOrZero(),
                ],
            ])
            ->add('orderId', HiddenType::class)
            ->add('addDevice', ButtonType::class, [
                'label' => 'Добавить устройство',
                'attr' => [
                    'class' => 'btn-primary btn-lg btn-block add-device-link',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn-success btn-lg btn-block button-save',
                ],
            ])
        ;

        $builder
            ->add('receiptStatus', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'attr' => [
                    'class' => 'repair-result',
                ],
                'placeholder' => false,
                'choices' => [
                    ReceiptStatusEnum::NEW => ReceiptStatusEnum::NEW,
                    ReceiptStatusEnum::WORK => ReceiptStatusEnum::WORK,
                    ReceiptStatusEnum::REPAIR_SUCCESS_SHOP => ReceiptStatusEnum::REPAIR_SUCCESS_SHOP,
                    ReceiptStatusEnum::REPAIR_FAIL_SHOP => ReceiptStatusEnum::REPAIR_FAIL_SHOP,
                    ReceiptStatusEnum::REPAIR_SUCCESS_OWNER => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                    ReceiptStatusEnum::REPAIR_FAIL_OWNER => ReceiptStatusEnum::REPAIR_FAIL_OWNER,
                    ReceiptStatusEnum::GUARANTEE_RETURN => ReceiptStatusEnum::GUARANTEE_RETURN,
                ],
                'choice_label' => fn($choice) => ReceiptStatusEnum::getTranslatePrefix() . $choice,
            ])
            ->add('workPerformed', TextareaType::class, [
                'label' => 'Проведенные работы',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Проведенные работы',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('workDescription', TextareaType::class, [
                'label' => 'Дополнительная информация',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Дополнительная информация',
                ],
            ])
            ->add('payRepairer', IntegerType::class, [
                'label' => 'Вознагрождение мастера',
                'required' => false,
                'attr' => [
                    'class' => 'pay-repairer-input',
                    'autocomplete' => 'off',
                    'placeholder' => 'Вознаграждение мастера',
                    'min' => 0,
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('priceRepair', IntegerType::class, [
                'label' => 'Стоимость ремонта',
                'required' => false,
                'attr' => [
                    'class' => 'price-repair-input',
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость ремонта',
                    'min' => 0,
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('priceSparePart', IntegerType::class, [
                'label' => 'Стоимость запчастей',
                'required' => false,
                'attr' => [
                    'class' => 'price-spare-part-input',
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость запчасти',
                    'min' => 0,
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('receiptCreatorId', ChoiceType::class, [
                'label' => 'Аппарат принял',
                'required' => true,
                'placeholder' => false,
                'choices' => array_flip($this->handler->getReceiptCreatorList()),
            ])
            ->add('deviceReturnerId', ChoiceType::class, [
                'label' => 'Аппарат вернул',
                'required' => false,
                'placeholder' => false,
                'choices' => array_flip($this->handler->getReceiptReturnerList()),
            ])
            ->add('repairerId', ChoiceType::class, [
                'label' => 'Аппарат отремонтировал',
                'required' => false,
                'placeholder' => false,
                'choices' => array_flip($this->handler->getRepairerList()),
            ])
            ->add('repairDate', TextType::class, [
                'label' => 'Дата ремонта',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker-input',
                ]
            ])
        ;

        $builder
            ->get('preliminarilyList')
            ->addModelTransformer(new CallbackTransformer(
                fn($preliminarilyList) => $preliminarilyList,
                fn($preliminarilyList) => $preliminarilyList ?: null
            ))
        ;

        $builder
            ->get('repairDate')
            ->addModelTransformer(new CallbackTransformer(
                fn($repairDate) => $repairDate ? (new DateTime($repairDate))->format('d.m.Y') : null,
                static function ($repairDate) {
                    return $repairDate
                        ? DateTime::createFromFormat('d.m.Y', $repairDate)->format('Y-m-d 00:00:00')
                        : null
                    ;
                }
            ))
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'receipt-form',
            ],
        ]);
    }
}
