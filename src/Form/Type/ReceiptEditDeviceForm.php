<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\UseCase\EditIndex\EditIndexHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReceiptEditDeviceForm extends AbstractType
{
    private RouterInterface $router;
    private EditIndexHandler $handler;

    /**
     * @param RouterInterface $router
     * @param EditIndexHandler $handler
     */
    public function __construct(RouterInterface $router, EditIndexHandler $handler)
    {
        $this->router = $router;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     *
     * @throws DBALException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                $builder
                    ->create('DeviceType', FormType::class, ['label' => false])
                    ->add('name', TextType::class, [
                        'label' => false,
                        'row_attr' => [
                            'class' => 'typeahead__container',
                        ],
                        'attr' => [
                            'class' => 'js-typeahead device-type',
                            'placeholder' => 'Тип устройства',
                            'data-choices' => json_encode($this->handler->getDeviceTypeList(), JSON_THROW_ON_ERROR),
                        ],
                    ])
            )
            ->add('id', HiddenType::class)
            ->add('brand', TextType::class, [
                'label' => false,
                'required' => false,
                'row_attr' => [
                    'class' => 'typeahead__container',
                ],
                'attr' => [
                    'class' => 'js-typeahead device-brand',
                    'autocomplete' => 'off',
                    'placeholder' => 'Бренд',
                    'data-callback' => $this->router->generate('app_receipt_receiptdevicebrandlist'),
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 100]),
                ],
            ])
            ->add('model', TextType::class, [
                'label' => false,
                'required' => false,
                'row_attr' => [
                    'class' => 'typeahead__container',
                ],
                'attr' => [
                    'class' => 'js-typeahead device-model',
                    'autocomplete' => 'off',
                    'placeholder' => 'Модель',
                    'data-callback' => $this->router->generate('app_receipt_receiptdevicemodellist'),
                ],
            ])
            ->add('imei', TextType::class, [
                'label' => false,
                'required' => false,
                'row_attr' => [
                    'class' => 'typeahead__container',
                ],
                'attr' => [
                    'class' => 'js-typeahead device-imei',
                    'autocomplete' => 'off',
                    'placeholder' => 'IMEI',
                    'data-callback' => $this->router->generate('app_receipt_receiptdeviceimeilist'),
                ],
            ])
            ->add('serialNumber', TextType::class, [
                'label' => false,
                'required' => false,
                'row_attr' => [
                    'class' => 'typeahead__container',
                ],
                'attr' => [
                    'class' => 'js-typeahead device-serial-number',
                    'autocomplete' => 'off',
                    'placeholder' => 'Серийный номер',
                    'data-callback' => $this->router->generate('app_receipt_receiptdeviceserialnumberlist'),
                ],
            ])
            ->add('parentDeviceId', HiddenType::class)
            ->add('deleteDevice', ButtonType::class, [
                'label' => 'Удалить устройство',
                'attr' => [
                    'class' => 'btn-danger remove-device',
                ],
            ])
        ;
    }
}
