<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;

class StatisticNetProfitForm extends AbstractType
{
    private RouterInterface $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $calculateNetProfitUrl = $this->router->generate('app_statistic_calculatenetprofit');

        $builder
            ->setAction($calculateNetProfitUrl)
            ->setMethod('GET')
            ->add('dateStart', TextType::class, [
                'required' => true,
                'label' => 'Начальная дата',
                'attr' => [
                    'class' => 'datepicker-input',
                ],
                'constraints' => [
                    new NotBlank(),
                    new DateTime(['format' => 'd.m.Y', 'message' => 'Значение даты недопустимо.']),
                ],
            ])
            ->add('dateEnd', TextType::class, [
                'required' => true,
                'label' => 'Конечная дата',
                'attr' => [
                    'class' => 'datepicker-input',
                ],
                'constraints' => [
                    new NotBlank(),
                    new DateTime(['format' => 'd.m.Y', 'message' => 'Значение даты недопустимо.']),
                ],
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
