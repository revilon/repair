<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\UserRoleEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class CreateUserForm extends AbstractType
{
    private RouterInterface $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $createUserUrl = $this->router->generate('app_administration_createuser');

        $builder
            ->setMethod('POST')
            ->setAction($createUserUrl)
            ->add('username', TextType::class, [
                'label' => 'Логин',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 16]),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9_-]{3,16}$/',
                        'message' => 'Логин может содержать только цифры, латинские заглавные и прописные буквы',
                    ])
                ]
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Пароль',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 6, 'max' => 16]),
                    new Regex([
                        'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/',
                        'message' => 'Пароль должен содержать цифры, заглавные и прописные буквы',
                    ])
                ]
            ])
            ->add('surname', TextType::class, [
                'label' => 'ФИО',
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Роль нового пользователя',
                'attr' => [
                    'class' => 'role-list',
                ],
                'choices' => [
                    UserRoleEnum::ROLE_ADMIN => UserRoleEnum::ROLE_ADMIN,
                    UserRoleEnum::ROLE_SELLER => UserRoleEnum::ROLE_SELLER,
                    UserRoleEnum::ROLE_REPAIRER => UserRoleEnum::ROLE_REPAIRER,
                ],
                'choice_label' => fn($choice) => UserRoleEnum::getTranslatePrefix() . $choice,
            ])
            ->add('percentRepairer', NumberType::class, [
                'required' => false,
                'label' => 'Процентная ставка с вознараждений по ремонту',
                'attr' => [
                    'placeholder' => 'Например 0.5',
                ],
            ])
        ;
    }
}
