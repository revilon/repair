<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\ReceiptStatusEnum;
use App\UseCase\RepairerSave\RepairerSaveHandler;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class RepairerWorkForm extends AbstractType
{
    private RouterInterface $router;
    private RepairerSaveHandler $handler;

    /**
     * @param RouterInterface $router
     * @param RepairerSaveHandler $handler
     */
    public function __construct(RouterInterface $router, RepairerSaveHandler $handler)
    {
        $this->router = $router;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     *
     * @throws DBALException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $actionUrl = $this->router->generate('app_repairer_repairersave');

        $builder
            ->setAction($actionUrl)
            ->add('repairResult', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'attr' => [
                    'class' => 'repair-result',
                ],
                'placeholder' => false,
                'choices' => [
                    ReceiptStatusEnum::REPAIR_SUCCESS_SHOP => ReceiptStatusEnum::REPAIR_SUCCESS_SHOP,
                    ReceiptStatusEnum::REPAIR_FAIL_SHOP => ReceiptStatusEnum::REPAIR_FAIL_SHOP,
                ],
                'choice_label' => fn($choice) => ReceiptStatusEnum::getTranslatePrefix() . $choice,
            ])
            ->add('workPerformed', TextareaType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Проведенные работы',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('workDescription', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Дополнительная информация',
                ],
            ])
            ->add('payRepairer', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'pay-repairer-input',
                    'readonly' => 'readonly',
                    'autocomplete' => 'off',
                    'placeholder' => 'Вознаграждение мастера',
                    'min' => 0,
                    'data-repairer-percent' => $this->handler->getRepairerPercent(),
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('priceRepair', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'price-repair-input',
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость ремонта',
                    'min' => 0,
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('priceSparePart', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'price-spare-part-input',
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость запчасти',
                    'min' => 0,
                ],
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('orderId', HiddenType::class)
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn-success btn-lg btn-block button-save',
                ]
            ])
        ;

        $builder
            ->get('orderId')
            ->addModelTransformer(new CallbackTransformer(
                fn($orderId) => $orderId,
                fn($orderId) => (int)$orderId
            ))
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'repairer-work-form',
            ],
        ]);
    }
}
