<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\PreliminarilyEnum;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class ReceiptForm extends AbstractType
{
    private RouterInterface $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     *
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $actionUrl = $this->router->generate('app_receipt_receiptsave');
        $callbackFullNameUrl = $this->router->generate('app_receipt_receiptdeviceownernamelist');
        $callbackAddressUrl = $this->router->generate('app_receipt_receiptdeviceowneraddresslist');

        $builder
            ->setAction($actionUrl)
            ->add(
                $builder
                    ->create('DeviceOwner', FormType::class)
                    ->add('name', TextType::class, [
                        'label' => false,
                        'row_attr' => [
                            'class' => 'typeahead__container',
                        ],
                        'attr' => [
                            'class' => 'js-typeahead full-name',
                            'autocomplete' => 'off',
                            'placeholder' => 'ФИО',
                            'data-callback' => $callbackFullNameUrl,
                        ],
                        'constraints' => [
                            new NotBlank(),
                            new Length(['min' => 3, 'max' => 100]),
                        ],
                    ])
                    ->add('address', TextType::class, [
                        'label' => false,
                        'required' => false,
                        'row_attr' => [
                            'class' => 'typeahead__container',
                        ],
                        'attr' => [
                            'class' => 'js-typeahead address',
                            'autocomplete' => 'off',
                            'placeholder' => 'Адрес',
                            'data-callback' => $callbackAddressUrl,
                        ],
                    ])
                    ->add('phone', TextType::class, [
                        'label' => false,
                        'required' => false,
                        'attr' => [
                            'autocomplete' => 'off',
                            'placeholder' => 'Телефон',
                        ],
                    ])
                    ->add('email', TextType::class, [
                        'label' => false,
                        'required' => false,
                        'attr' => [
                            'autocomplete' => 'off',
                            'placeholder' => 'Email',
                        ],
                        'constraints' => [
                            new Email(),
                        ],
                    ])
                    ->add('gender', HiddenType::class)
            )
            ->add('DeviceCollection', CollectionType::class, [
                'label' => false,
                'entry_options' => [
                    'label' => false,
                    'attr' => [
                        'class' => 'tab-pane fade',
                        'role' => 'tabpanel',
                        'aria-labelledby' => 'tab-__name__',
                    ],
                ],
                'entry_type' => ReceiptDeviceForm::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
            ])
            ->add('defect', TextType::class, [
                'label' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Неисправность',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3]),
                ],

            ])
            ->add('preliminarilyList', ChoiceType::class, [
                'required' => false,
                'label' => false,
                'choices' => PreliminarilyEnum::getListCombine(),
                'attr' => [
                    'class' => 'preliminarilyList',
                    'autocomplete' => 'off',
                    'placeholder' => 'Предварительно',
                ],
                'multiple' => true,
                'choice_label' => fn($choice) => PreliminarilyEnum::getTranslatePrefix() . $choice,
            ])
            ->add('deviceCode', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Код доступа',
                ],
            ])
            ->add('devicePreview', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Внешнее состояние',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 500]),
                ],
            ])
            ->add('prePrice', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Стоимость ремонта',
                ],
                'constraints' => [
                    new NotBlank(),
                    new PositiveOrZero(),
                ],
            ])
            ->add('addDevice', ButtonType::class, [
                'label' => 'Добавить устройство',
                'attr' => [
                    'class' => 'btn-primary btn-lg btn-block add-device-link',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn-success btn-lg btn-block button-save',
                ],
            ])
        ;

        $builder
            ->get('preliminarilyList')
            ->addModelTransformer(new CallbackTransformer(
                fn($preliminarilyList) => $preliminarilyList,
                fn($preliminarilyList) => $preliminarilyList ?: null
            ))
        ;
    }
}
