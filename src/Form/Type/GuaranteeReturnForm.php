<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class GuaranteeReturnForm extends AbstractType
{
    private RouterInterface $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $guaranteeReturnUrl = $this->router->generate('app_search_guaranteereturn');

        $builder
            ->setAction($guaranteeReturnUrl)
            ->add('guaranteeReturnDescription', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Комментарий по возврату',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('orderId', HiddenType::class)
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn-success btn-lg btn-block button-guarantee-return-save',
                ],
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'guarantee-return-form',
            ],
        ]);
    }
}
