<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Throwable;

class FormBadRequestHttpException extends BadRequestHttpException
{
    private array $formErrorList;

    /**
     * @param array $formErrorList
     * @param string $message
     * @param Throwable|null $previous
     * @param int $code
     * @param array $headers
     */
    public function __construct(
        array $formErrorList = [],
        string $message = '',
        Throwable $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        $this->formErrorList = $formErrorList;

        parent::__construct($message, $previous, $code, $headers);
    }

    /**
     * @return array
     */
    public function getFormErrorList(): array
    {
        return $this->formErrorList;
    }
}
