<?php

declare(strict_types=1);

namespace App\Guesser;

use Symfony\Component\HttpFoundation\Request;

class AreaGuesser
{
    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isApiAdminRequest(Request $request): bool
    {
        return strpos($request->getPathInfo(), '/admin/api') === 0;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isAdminRequest(Request $request): bool
    {
        return strpos($request->getPathInfo(), '/admin') === 0;
    }
}
