<?php

namespace App\Entity;

use App\Component\IdGenerator\Traits\IdEntityAwareTrait;
use App\Doctrine\Dbal\Type\PreliminarilySetType;
use App\Doctrine\Dbal\Type\ReceiptStatusEnumType;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uniqOrderId",
 *             columns={"orderId"}
 *         )
 *     })
 * )
 */
class Receipt
{
    use IdEntityAwareTrait;
    use TimestampableEntity;

    /**
     * @var DeviceOwner
     *
     * @ORM\ManyToOne(targetEntity=DeviceOwner::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $deviceOwner;

    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity=Device::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $defect;

    /**
     * @var array|null
     *
     * @ORM\Column(type=PreliminarilySetType::NAME, nullable=true)
     */
    private $preliminarilyList;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceCode;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $devicePreview;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prePrice;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $orderId;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date")
     */
    private $orderDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $receiptCreator;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $workPerformed;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $workDescription;

    /**
     * @var string
     *
     * @ORM\Column(type=ReceiptStatusEnumType::NAME)
     */
    private $receiptStatus;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $payRepairer;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn()
     */
    private $repairer;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn()
     */
    private $deviceReturner;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceRepair;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceSparePart;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $repairDate;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $returnDate;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $guaranteeReturnDescription;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $guaranteeReturnDate;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lastEditUser;

    /**
     * @return DeviceOwner
     */
    public function getDeviceOwner(): DeviceOwner
    {
        return $this->deviceOwner;
    }

    /**
     * @param DeviceOwner $deviceOwner
     */
    public function setDeviceOwner(DeviceOwner $deviceOwner): void
    {
        $this->deviceOwner = $deviceOwner;
    }

    /**
     * @return Device
     */
    public function getDevice(): Device
    {
        return $this->device;
    }

    /**
     * @param Device $device
     */
    public function setDevice(Device $device): void
    {
        $this->device = $device;
    }

    /**
     * @return string
     */
    public function getDefect(): string
    {
        return $this->defect;
    }

    /**
     * @param string $defect
     */
    public function setDefect(string $defect): void
    {
        $this->defect = $defect;
    }

    /**
     * @return array|null
     */
    public function getPreliminarilyList(): ?array
    {
        return $this->preliminarilyList;
    }

    /**
     * @param array|null $preliminarilyList
     */
    public function setPreliminarilyList(?array $preliminarilyList): void
    {
        $this->preliminarilyList = $preliminarilyList;
    }

    /**
     * @return string|null
     */
    public function getDeviceCode(): ?string
    {
        return $this->deviceCode;
    }

    /**
     * @param string|null $deviceCode
     */
    public function setDeviceCode(?string $deviceCode): void
    {
        $this->deviceCode = $deviceCode;
    }

    /**
     * @return string|null
     */
    public function getDevicePreview(): ?string
    {
        return $this->devicePreview;
    }

    /**
     * @param string|null $devicePreview
     */
    public function setDevicePreview(?string $devicePreview): void
    {
        $this->devicePreview = $devicePreview;
    }

    /**
     * @return int|null
     */
    public function getPrePrice(): ?int
    {
        return $this->prePrice;
    }

    /**
     * @param int|null $prePrice
     */
    public function setPrePrice(?int $prePrice): void
    {
        $this->prePrice = $prePrice;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return DateTime
     */
    public function getOrderDate(): DateTime
    {
        return $this->orderDate;
    }

    /**
     * @param DateTime $orderDate
     */
    public function setOrderDate(DateTime $orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return User
     */
    public function getReceiptCreator(): User
    {
        return $this->receiptCreator;
    }

    /**
     * @param User $receiptCreator
     */
    public function setReceiptCreator(User $receiptCreator): void
    {
        $this->receiptCreator = $receiptCreator;
    }

    /**
     * @return string|null
     */
    public function getWorkPerformed(): ?string
    {
        return $this->workPerformed;
    }

    /**
     * @param string|null $workPerformed
     */
    public function setWorkPerformed(?string $workPerformed): void
    {
        $this->workPerformed = $workPerformed;
    }

    /**
     * @return string|null
     */
    public function getWorkDescription(): ?string
    {
        return $this->workDescription;
    }

    /**
     * @param string|null $workDescription
     */
    public function setWorkDescription(?string $workDescription): void
    {
        $this->workDescription = $workDescription;
    }

    /**
     * @return string
     */
    public function getReceiptStatus(): string
    {
        return $this->receiptStatus;
    }

    /**
     * @param string $receiptStatus
     */
    public function setReceiptStatus(string $receiptStatus): void
    {
        $this->receiptStatus = $receiptStatus;
    }

    /**
     * @return int|null
     */
    public function getPayRepairer(): ?int
    {
        return $this->payRepairer;
    }

    /**
     * @param int|null $payRepairer
     */
    public function setPayRepairer(?int $payRepairer): void
    {
        $this->payRepairer = $payRepairer;
    }

    /**
     * @return User|null
     */
    public function getRepairer(): ?User
    {
        return $this->repairer;
    }

    /**
     * @param User|null $repairer
     */
    public function setRepairer(?User $repairer): void
    {
        $this->repairer = $repairer;
    }

    /**
     * @return User|null
     */
    public function getDeviceReturner(): ?User
    {
        return $this->deviceReturner;
    }

    /**
     * @param User|null $deviceReturner
     */
    public function setDeviceReturner(?User $deviceReturner): void
    {
        $this->deviceReturner = $deviceReturner;
    }

    /**
     * @return int|null
     */
    public function getPriceRepair(): ?int
    {
        return $this->priceRepair;
    }

    /**
     * @param int|null $priceRepair
     */
    public function setPriceRepair(?int $priceRepair): void
    {
        $this->priceRepair = $priceRepair;
    }

    /**
     * @return int|null
     */
    public function getPriceSparePart(): ?int
    {
        return $this->priceSparePart;
    }

    /**
     * @param int|null $priceSparePart
     */
    public function setPriceSparePart(?int $priceSparePart): void
    {
        $this->priceSparePart = $priceSparePart;
    }

    /**
     * @return DateTime|null
     */
    public function getRepairDate(): ?DateTime
    {
        return $this->repairDate;
    }

    /**
     * @param DateTime|null $repairDate
     */
    public function setRepairDate(?DateTime $repairDate): void
    {
        $this->repairDate = $repairDate;
    }

    /**
     * @return DateTime|null
     */
    public function getReturnDate(): ?DateTime
    {
        return $this->returnDate;
    }

    /**
     * @param DateTime|null $returnDate
     */
    public function setReturnDate(?DateTime $returnDate): void
    {
        $this->returnDate = $returnDate;
    }

    /**
     * @return string|null
     */
    public function getGuaranteeReturnDescription(): ?string
    {
        return $this->guaranteeReturnDescription;
    }

    /**
     * @param string|null $guaranteeReturnDescription
     */
    public function setGuaranteeReturnDescription(?string $guaranteeReturnDescription): void
    {
        $this->guaranteeReturnDescription = $guaranteeReturnDescription;
    }

    /**
     * @return DateTime|null
     */
    public function getGuaranteeReturnDate(): ?DateTime
    {
        return $this->guaranteeReturnDate;
    }

    /**
     * @param DateTime|null $guaranteeReturnDate
     */
    public function setGuaranteeReturnDate(?DateTime $guaranteeReturnDate): void
    {
        $this->guaranteeReturnDate = $guaranteeReturnDate;
    }

    /**
     * @return User
     */
    public function getLastEditUser(): User
    {
        return $this->lastEditUser;
    }

    /**
     * @param User $lastEditUser
     */
    public function setLastEditUser(User $lastEditUser): void
    {
        $this->lastEditUser = $lastEditUser;
    }
}
