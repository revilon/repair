<?php

namespace App\Entity;

use App\Component\IdGenerator\Traits\IdEntityAwareTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uniqName",
 *             columns={"name"}
 *         )
 *     })
 * )
 */
class DeviceType
{
    use IdEntityAwareTrait;
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
