<?php

declare(strict_types=1);

namespace App\Entity;

use App\Component\IdGenerator\Traits\IdEntityAwareTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class Device
{
    use IdEntityAwareTrait;
    use TimestampableEntity;

    /**
     * @var DeviceType
     *
     * @ORM\ManyToOne(targetEntity=DeviceType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $model;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $imei;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $serialNumber;

    /**
     * @var Device|null
     *
     * @ORM\ManyToOne(targetEntity=Device::class)
     * @ORM\JoinColumn()
     */
    private $parentDevice = null;

    /**
     * @return DeviceType
     */
    public function getDeviceType(): DeviceType
    {
        return $this->deviceType;
    }

    /**
     * @param DeviceType $deviceType
     */
    public function setDeviceType(DeviceType $deviceType): void
    {
        $this->deviceType = $deviceType;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string|null $model
     */
    public function setModel(?string $model): void
    {
        $this->model = $model;
    }

    /**
     * @return string|null
     */
    public function getImei(): ?string
    {
        return $this->imei;
    }

    /**
     * @param string|null $imei
     */
    public function setImei(?string $imei): void
    {
        $this->imei = $imei;
    }

    /**
     * @return string|null
     */
    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    /**
     * @param string|null $serialNumber
     */
    public function setSerialNumber(?string $serialNumber): void
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return Device|null
     */
    public function getParentDevice(): ?Device
    {
        return $this->parentDevice;
    }

    /**
     * @param Device|null $parentDevice
     */
    public function setParentDevice(?Device $parentDevice): void
    {
        $this->parentDevice = $parentDevice;
    }
}
