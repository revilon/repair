<?php

declare(strict_types=1);

namespace App\Command;

use App\Component\Manager\Executer\RowManager;
use App\Enum\PreliminarilyEnum;
use App\Enum\ReceiptStatusEnum;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NormalizationLegacyDatabaseCommand extends Command
{
    protected static $defaultName = 'database:normalization-legacy';
    private RowManager $manager;

    private const DEVICE_MODEL_MAP = [
        'видео камера' => 6,
        'Жесткий диск' => 5,
        'Мобильный телефон:' => 1,
        'Телефон' => 1,
        'Модель тех средства:' => 1,
        'Ноутбук' => 3,
        'Ноутбук:' => 3,
        'Планшет' => 2,
        'Планшетный компьютер:' => 2,
        'Электоронная книга' => 4,
        'Электронная книга:' => 4,
    ];

    private const RECEIPT_CREATOR_MAP = [
        '1' => 1,
        '5' => 5,
        'Александр Скотников' => 6,
        'Владимир Соболевский' => 11,
        'Евгений Кузнецов' => 1,
        'Татьяна Кузнецова' => 5,
    ];

    private const REPAIRER_MAP = [
        '3' => 3,
        '5' => 5,
        'Александр Скотников' => 6,
        'Владимир Климаков' => 3,
        'Евгений Кузнецов' => 1,
        'Татьяна Кузнецова' => 5,
    ];

    private const DEVICE_RETURNER_MAP = [
        '5' => 5,
        'Александр Скотников' => 6,
        'Владимир Соболевский' => 11,
        'Татьяна Кузнецова' => 5,
    ];

    private const LAST_EDIT_USER_MAP = [
        '1' => 1,
        '3' => 3,
        '5' => 5,
    ];

    /**
     * @required
     *
     * @param RowManager $manager
     */
    public function dependencyInjection(RowManager $manager): void
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->manager->beginTransaction();

        try {
            $sqlReceipts = <<<SQL
                select
                    r.vladelec name,
                    null email,
                    null address,
                    r.adres phone,
                    now() createdAt,
                    now() updatedAt,
                    r.device_brand brand,
                    r.model model,
                    r.imei imei,
                    r.modeltehsredstva modeltehsredstva,
                    r.snzu snzu,
                    r.snakb snakb,
                    r.ktosozdal ktosozdal,
                    r.ktosdelal ktosdelal,
                    r.ktootdal ktootdal,
                    r.name_last_open name_last_open,
                    r.neispravnost defect,
                    r.vkl vkl,
                    r.voda voda,
                    r.udar udar,
                    r.kod kod,
                    r.preliminarily preliminarily,
                    r.sostoyanie sostoyanie,
                    r.stoimost stoimost,
                    r.nomerdogovora nomerdogovora,
                    r.date date,
                    r.chtosdelanno chtosdelanno,
                    r.opisanie opisanie,
                    r.sbezremonta sbezremonta,
                    r.dateout dateout,
                    r.vozvrat vozvrat,
                    r.voznagrozhdenie voznagrozhdenie,
                    r.money money,
                    r.zapchast zapchast,
                    r.daterepair daterepair,
                    r.datelastedit datelastedit
                from receipts r order by nomerdogovora
SQL;

            $stmtReceipts = $this->manager->getConnection()->executeQuery($sqlReceipts);
            $receiptsList = $stmtReceipts->fetchAll();

            $deviceOwnerList = [];
            $deviceList = [];
            $deviceChildList = [];
            $receiptList = [];

            foreach ($receiptsList as $item) {
                $deviceOwnerId = $this->manager->generateUniqueId();
                $deviceOwnerList[] = [
                    'id' => $deviceOwnerId,
                    'name' => $item['name'],
                    'email' => null,
                    'address' => null,
                    'phone' => $item['phone'],
                    'gender' => null,
                    'createdAt' => $item['createdAt'],
                    'updatedAt' => $item['updatedAt'],
                ];

                $deviceId = $this->manager->generateUniqueId();
                $deviceList[] = [
                    'id' => $deviceId,
                    'brand' => $item['brand'],
                    'model' => $item['model'],
                    'imei' => $item['imei'],
                    'serialNumber' => null,
                    'createdAt' => $item['createdAt'],
                    'updatedAt' => $item['updatedAt'],
                    'deviceTypeId' => self::DEVICE_MODEL_MAP[$item['modeltehsredstva']],
                ];
                if (!empty($item['snzu'])) {
                    $deviceChildId = $this->manager->generateUniqueId();
                    $deviceChildList[] = [
                        'id' => $deviceChildId,
                        'brand' => $item['brand'],
                        'serialNumber' => $item['snzu'],
                        'parentDeviceId' => $deviceId,
                        'createdAt' => $item['createdAt'],
                        'updatedAt' => $item['updatedAt'],
                        'deviceTypeId' => 7,
                    ];
                }
                if (!empty($item['snakb'])) {
                    $deviceChildId = $this->manager->generateUniqueId();
                    $deviceChildList[] = [
                        'id' => $deviceChildId,
                        'brand' => $item['brand'],
                        'serialNumber' => $item['snakb'],
                        'parentDeviceId' => $deviceId,
                        'createdAt' => $item['createdAt'],
                        'updatedAt' => $item['updatedAt'],
                        'deviceTypeId' => 8,
                    ];
                }

                $preliminarilyList = [];
                if (!empty($item['vkl'])) {
                    $preliminarilyList[] = PreliminarilyEnum::IS_ENABLE;
                }
                if (!empty($item['voda'])) {
                    $preliminarilyList[] = PreliminarilyEnum::IS_LIQUID;
                }
                if (!empty($item['udar'])) {
                    $preliminarilyList[] = PreliminarilyEnum::IS_STRIKE;
                }
                if (!empty($item['preliminarily'])) {
                    $preliminarily = json_decode($item['preliminarily'], true, 512, JSON_THROW_ON_ERROR);
                    if (in_array('Включается', $preliminarily, true)) {
                        $preliminarilyList[] = PreliminarilyEnum::IS_ENABLE;
                    }
                    if (in_array('Подвергался влаге', $preliminarily, true)) {
                        $preliminarilyList[] = PreliminarilyEnum::IS_LIQUID;
                    }
                    if (in_array('Подвергался удару', $preliminarily, true)) {
                        $preliminarilyList[] = PreliminarilyEnum::IS_STRIKE;
                    }
                }
                $receiptStatus = null;
                if (!empty($item['sbezremonta'])) {
                    if ($item['sbezremonta'] === 'sremontom' || $item['sbezremonta'] === 'with-repair') {
                        if (!empty($item['dateout'])) {
                            $receiptStatus = ReceiptStatusEnum::REPAIR_SUCCESS_OWNER;
                        } else {
                            $receiptStatus = ReceiptStatusEnum::REPAIR_SUCCESS_SHOP;
                        }
                    }
                    if ($item['sbezremonta'] === 'bezremonta' || $item['sbezremonta'] === 'without-repair') {
                        if (!empty($item['dateout'])) {
                            $receiptStatus = ReceiptStatusEnum::REPAIR_FAIL_OWNER;
                        } else {
                            $receiptStatus = ReceiptStatusEnum::REPAIR_FAIL_SHOP;
                        }
                    }
                } else {
                    if (empty($item['ktootdal']) && !empty($item['chtosdelanno'])) {
                        $receiptStatus = ReceiptStatusEnum::WORK;
                    }
                    if (empty($item['ktootdal']) && empty($item['chtosdelanno'])) {
                        $receiptStatus = ReceiptStatusEnum::NEW;
                    }
                    if (!empty($item['ktootdal']) && empty($item['money'])) {
                        $receiptStatus = ReceiptStatusEnum::REPAIR_FAIL_OWNER;
                    }
                    if (!empty($item['ktootdal']) && !empty($item['money'])) {
                        $receiptStatus = ReceiptStatusEnum::REPAIR_SUCCESS_OWNER;
                    }
                }
                if (!empty($item['vozvrat'])) {
                    $receiptStatus = ReceiptStatusEnum::GUARANTEE_RETURN;
                }
                $returnDate = null;
                if ($item['dateout'] === '0000-00-00 00:00:00') {
                    $returnDate = $item['datelastedit'];
                } elseif (empty($item['dateout'])) {
                    $returnDate = null;
                } else {
                    $returnDate = $item['dateout'];
                }
                $repairDate = null;
                if ($item['daterepair'] === '0000-00-00 00:00:00') {
                    $repairDate = $item['datelastedit'];
                } elseif (empty($item['daterepair'])) {
                    $repairDate = null;
                } else {
                    $repairDate = $item['daterepair'];
                }
                $receiptList[] = [
                    'defect' => $item['defect'],
                    'preliminarilyList' => !empty($preliminarilyList)
                        ? implode(',', $preliminarilyList)
                        : null,
                    'deviceCode' => !empty($item['kod']) ? $item['kod'] : null,
                    'devicePreview' => !empty($item['sostoyanie']) ? $item['sostoyanie'] : null,
                    'prePrice' => !empty($item['stoimost']) ? (int)$item['stoimost'] : null,
                    'orderId' => $item['nomerdogovora'],
                    'orderDate' => $item['date'],
                    'workPerformed' => !empty($item['chtosdelanno']) ? $item['chtosdelanno'] : null,
                    'workDescription' => !empty($item['opisanie']) ? $item['opisanie'] : null,
                    'receiptStatus' => $receiptStatus,
                    'payRepairer' => !empty($item['voznagrozhdenie']) ? $item['voznagrozhdenie'] : null,
                    'priceRepair' => !empty($item['money']) ? $item['money'] : null,
                    'priceSparePart' => !empty($item['zapchast']) ? $item['zapchast'] : null,
                    'repairDate' => $repairDate,
                    'returnDate' => $returnDate,
                    'guaranteeReturnDescription' => !empty($item['vozvrat']) ? $item['vozvrat'] : null,
                    'guaranteeReturnDate' => !empty($item['vozvrat']) ? $item['datelastedit'] : null,
                    'createdAt' => $item['date'],
                    'updatedAt' => $item['datelastedit'],
                    'deviceOwnerId' => $deviceOwnerId,
                    'deviceId' => $deviceId,
                    'receiptCreatorId' => self::RECEIPT_CREATOR_MAP[$item['ktosozdal']],
                    'repairerId' => !empty($item['ktosdelal']) ? self::REPAIRER_MAP[$item['ktosdelal']] : null,
                    'deviceReturnerId' => !empty($item['ktootdal'])
                        ? self::DEVICE_RETURNER_MAP[$item['ktootdal']]
                        : null,
                    'lastEditUserId' => !empty($item['name_last_open'])
                        ? self::LAST_EDIT_USER_MAP[$item['name_last_open']]
                        : self::RECEIPT_CREATOR_MAP[$item['ktosozdal']],
                ];
            }

            foreach (array_chunk($deviceOwnerList, 500) as $deviceOwnerListChunk) {
                $this->manager->insertBulk('DeviceOwner', $deviceOwnerListChunk);
            }
            foreach (array_chunk($deviceList, 500) as $deviceListChunk) {
                $this->manager->insertBulk('Device', $deviceListChunk);
            }
            foreach (array_chunk($deviceChildList, 500) as $deviceChildListChunk) {
                $this->manager->insertBulk('Device', $deviceChildListChunk);
            }
            foreach (array_chunk($receiptList, 500) as $receiptListChunk) {
                $this->manager->insertBulk('Receipt', $receiptListChunk);
            }

            $this->manager->commit();
        } catch (Exception $exception) {
            $this->manager->rollBack();

            throw $exception;
        }

        return 0;
    }
}
