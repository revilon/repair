<?php

declare(strict_types=1);

namespace App\UseCase\StatisticTotalPrice;

use App\Enum\MonthEnum;
use App\Enum\MonthShortEnum;
use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;
use ReflectionException;

class StatisticTotalPriceHandler
{
    private StatisticTotalPriceManager $manager;

    /**
     * @param StatisticTotalPriceManager $manager
     */
    public function __construct(StatisticTotalPriceManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function handle(): array
    {
        $now = new DateTimeImmutable();
        $dateStart = $now->modify('-1 year');
        $dateEnd = $now->modify('now 23:59:59');

        $totalPricePairList = $this->manager->getTotalPriceList($dateStart, $dateEnd);

        $interval = new DateInterval('P1M');
        $dateRange = new DatePeriod($dateStart, $interval, $dateEnd);

        $monthList = [];
        $monthShortList = [];
        $totalPriceList = [];
        $totalPriceFormatList = [];
        $efficiencyList = [];

        /** @var DateTime[] $dateRange */
        foreach ($dateRange as $date) {
            $monthShortList[] = MonthShortEnum::getNameMonth((int)$date->format('n'));
            $monthList[] = MonthEnum::getNameMonth((int)$date->format('n'));

            if (isset($totalPricePairList[$date->format('Y-m')])) {
                $totalPrice = (int)$totalPricePairList[$date->format('Y-m')];
                $totalPriceList[] = $totalPrice;
                $totalPriceFormatList[] = number_format($totalPrice, 0, ',', ' ');
            } else {
                $totalPriceList[] = 0;
                $totalPriceFormatList[] = '0';
            }

            $currentPay = end($totalPriceList);
            $previousPay = prev($totalPriceList) ?: 0;

            $efficiencyList[] = $this->calculateEfficiency($currentPay, $previousPay);
        }

        return [
            'total_price_list' => $totalPriceList,
            'total_price_format_list' => $totalPriceFormatList,
            'month_list' => $monthList,
            'month_short_list' => $monthShortList,
            'efficiency_list' => $efficiencyList,
        ];
    }

    /**
     * @param int $firstPay
     * @param int $secondPay
     *
     * @return int
     */
    private function calculateEfficiency(int $firstPay, int $secondPay): int
    {
        if (!$firstPay || !$secondPay) {
            return 0;
        }

        $efficiency = round(($firstPay - $secondPay) / $secondPay, 2) * 100;

        return (int)$efficiency;
    }
}
