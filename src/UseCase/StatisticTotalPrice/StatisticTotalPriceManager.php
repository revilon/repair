<?php

declare(strict_types=1);

namespace App\UseCase\StatisticTotalPrice;

use App\Component\Manager\Executer\RowManager;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;

class StatisticTotalPriceManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param DateTimeImmutable $dateStart
     * @param DateTimeImmutable $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getTotalPriceList(DateTimeImmutable $dateStart, DateTimeImmutable $dateEnd): array
    {
        $sql = <<<SQL
            select
                sum(r.priceRepair) priceRepair,
                date_format(r.returnDate, '%Y-%m') returnYearMonth
            from Receipt r
            where r.returnDate between :dateStart and :dateEnd
            group by returnYearMonth
            order by returnYearMonth
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'dateStart' => $dateStart->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
        ]);

        return $this->manager->getResultPairList($stmt, 'returnYearMonth', 'priceRepair');
    }
}
