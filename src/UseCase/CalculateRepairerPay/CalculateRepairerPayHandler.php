<?php

declare(strict_types=1);

namespace App\UseCase\CalculateRepairerPay;

use App\Entity\User;
use DateTime;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CalculateRepairerPayHandler
{
    private CalculateRepairerPayManager $manager;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param CalculateRepairerPayManager $manager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CalculateRepairerPayManager $manager, TokenStorageInterface $tokenStorage)
    {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $startDatePayRepairer = $request->get('start');
        $endDatePayRepairer = $request->get('end');
        $dateStart = DateTime::createFromFormat('d.m.Y', $startDatePayRepairer);
        $dateEnd = DateTime::createFromFormat('d.m.Y', $endDatePayRepairer);

        $repairerId = $this->getUser()->getId();

        $repairerPayTotal = 0;
        $repairerPay = $this->manager->getRepairerPay($repairerId, $dateStart, $dateEnd);
        $repairerPay = array_map('array_values', $repairerPay);

        foreach ($repairerPay as $item) {
            $repairerPayTotal += $item[1];
        }

        return [
            'repairer_pay' => $repairerPay,
            'repairer_pay_total' => $repairerPayTotal,
        ];
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if ($token === null) {
            throw new AuthenticationException('Authentication exception');
        }

        /** @var User $user */
        $user = $token->getUser();

        return $user;
    }
}
