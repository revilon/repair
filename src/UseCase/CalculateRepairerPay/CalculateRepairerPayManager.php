<?php

declare(strict_types=1);

namespace App\UseCase\CalculateRepairerPay;

use App\Component\Manager\Executer\RowManager;
use DateTime;
use Doctrine\DBAL\DBALException;

class CalculateRepairerPayManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $repairerId
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getRepairerPay(string $repairerId, DateTime $dateStart, DateTime $dateEnd): array
    {
        $sql = '
            select
              dt.name,
              sum(r.payRepairer) pay_repairer
            from Receipt r
            join Device d on r.deviceId = d.id
            join DeviceType dt on d.deviceTypeId = dt.id
            where 1
              and r.repairDate between :date_start and :date_end
              and r.repairerId = :repairer_id
            group by d.deviceTypeId
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'date_start' => $dateStart->format('Y-m-d 00:00:00'),
            'date_end' => $dateEnd->format('Y-m-d 23:59:59'),
            'repairer_id' => $repairerId,
        ]);

        return $stmt->fetchAll();
    }
}
