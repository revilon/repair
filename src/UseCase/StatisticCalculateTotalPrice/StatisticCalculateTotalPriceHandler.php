<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateTotalPrice;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\StatisticTotalPriceForm;
use DateTime;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class StatisticCalculateTotalPriceHandler
{
    private StatisticCalculateTotalPriceManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;

    /**
     * @param StatisticCalculateTotalPriceManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        StatisticCalculateTotalPriceManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @param Request $request
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(Request $request): string
    {
        $statisticTotalPriceForm = $this->formFactory->create(StatisticTotalPriceForm::class);
        $statisticTotalPriceForm->handleRequest($request);

        if (!$statisticTotalPriceForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$statisticTotalPriceForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($statisticTotalPriceForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $statisticTotalPriceData = $statisticTotalPriceForm->getData();

        $dateStart = DateTime::createFromFormat('d.m.Y', $statisticTotalPriceData['dateStart']);
        $dateStart->setTime(00, 00, 00);
        $dateEnd = DateTime::createFromFormat('d.m.Y', $statisticTotalPriceData['dateEnd']);
        $dateEnd->setTime(23, 59, 00);

        $totalPrice = $this->manager->getTotalPrice($dateStart, $dateEnd);
        $totalPrice = number_format($totalPrice, 0, ',', ' ');

        return $totalPrice;
    }
}
