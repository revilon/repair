<?php

declare(strict_types=1);

namespace App\UseCase\SearchReceiptAgo;

use Doctrine\DBAL\DBALException;
use Knp\Bundle\TimeBundle\Templating\Helper\TimeHelper;

class SearchReceiptAgoHandler
{
    private SearchReceiptAgoManager $manager;
    private TimeHelper $agoTimeFormatter;

    /**
     * @param SearchReceiptAgoManager $manager
     * @param TimeHelper $agoTimeFormatter
     */
    public function __construct(SearchReceiptAgoManager $manager, TimeHelper $agoTimeFormatter)
    {
        $this->manager = $manager;
        $this->agoTimeFormatter = $agoTimeFormatter;
    }

    /**
     * @param int $orderId
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(int $orderId): string
    {
        $receiptUpdatedAt = $this->manager->getReceiptUpdatedAt($orderId);

        return $this->agoTimeFormatter->diff($receiptUpdatedAt);
    }
}
