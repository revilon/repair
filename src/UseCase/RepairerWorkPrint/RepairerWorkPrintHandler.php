<?php

declare(strict_types=1);

namespace App\UseCase\RepairerWorkPrint;

use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RepairerWorkPrintHandler
{
    private RepairerWorkPrintManager $manager;

    /**
     * @param RepairerWorkPrintManager $manager
     */
    public function __construct(RepairerWorkPrintManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(int $orderId): array
    {
        $receiptRepairerWork = $this->manager->getReceiptRepairerWork($orderId);

        if (!$receiptRepairerWork) {
            throw new NotFoundHttpException("Квитанция с номером $orderId не найдена");
        }

        return $receiptRepairerWork;
    }
}
