<?php

declare(strict_types=1);

namespace App\UseCase\RepairerWorkPrint;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class RepairerWorkPrintManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getReceiptRepairerWork(int $orderId): ?array
    {
        $sql = <<<SQL
            select
                r.orderId,
                r.workPerformed,
                r.priceRepair,
                r.orderDate
            from Receipt r
            where r.orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return $stmt->fetch() ?: null;
    }
}
