<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCostSparePart;

use App\Component\Manager\Executer\RowManager;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;

class StatisticCostSparePartManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param DateTimeImmutable $dateStart
     * @param DateTimeImmutable $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getCostSparePartList(DateTimeImmutable $dateStart, DateTimeImmutable $dateEnd): array
    {
        $sql = <<<SQL
            select
                sum(r.priceSparePart) priceSparePart,
                date_format(r.repairDate, '%Y-%m') repairDateYearMonth
            from Receipt r
            where r.repairDate between :dateStart and :dateEnd
            group by repairDateYearMonth
            order by repairDateYearMonth
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'dateStart' => $dateStart->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
        ]);

        return $this->manager->getResultPairList($stmt, 'repairDateYearMonth', 'priceSparePart');
    }
}
