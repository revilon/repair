<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCostSparePart;

use App\Enum\MonthEnum;
use App\Enum\MonthShortEnum;
use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;
use ReflectionException;

class StatisticCostSparePartHandler
{
    private StatisticCostSparePartManager $manager;

    /**
     * @param StatisticCostSparePartManager $manager
     */
    public function __construct(StatisticCostSparePartManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws ReflectionException
     * @throws DBALException
     */
    public function handle(): array
    {
        $now = new DateTimeImmutable();
        $dateStart = $now->modify('-1 year');
        $dateEnd = $now->modify('now 23:59:59');

        $costSparePartPairList = $this->manager->getCostSparePartList($dateStart, $dateEnd);

        $interval = new DateInterval('P1M');
        $dateRange = new DatePeriod($dateStart, $interval, $dateEnd);

        $monthList = [];
        $monthShortList = [];
        $costSparePartList = [];
        $costSparePartFormatList = [];
        $efficiencyList = [];

        /** @var DateTime[] $dateRange */
        foreach ($dateRange as $date) {
            $monthShortList[] = MonthShortEnum::getNameMonth((int)$date->format('n'));
            $monthList[] = MonthEnum::getNameMonth((int)$date->format('n'));

            if (isset($costSparePartPairList[$date->format('Y-m')])) {
                $costSparePart = (int)$costSparePartPairList[$date->format('Y-m')];
                $costSparePartList[] = $costSparePart;
                $costSparePartFormatList[] = number_format($costSparePart, 0, ',', ' ');
            } else {
                $costSparePartList[] = 0;
                $costSparePartFormatList[] = '0';
            }

            $currentPay = end($costSparePartList);
            $previousPay = prev($costSparePartList) ?: 0;

            $efficiencyList[] = $this->calculateEfficiency($currentPay, $previousPay);
        }

        return [
            'cost_spare_part_list' => $costSparePartList,
            'cost_spare_part_format_list' => $costSparePartFormatList,
            'month_list' => $monthList,
            'month_short_list' => $monthShortList,
            'efficiency_list' => $efficiencyList,
        ];
    }

    /**
     * @param int $firstPay
     * @param int $secondPay
     *
     * @return float
     */
    private function calculateEfficiency(int $firstPay, int $secondPay): float
    {
        if (!$firstPay || !$secondPay) {
            return 0;
        }

        $efficiency = round(($firstPay - $secondPay) / $secondPay, 2) * 100;

        return (int)$efficiency;
    }
}
