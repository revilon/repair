<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptDeviceImeiList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class ReceiptDeviceImeiListManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     * @param string|null $model
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getImeiList(string $searchText, string $deviceType, ?string $brand, ?string $model): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->select('d.imei')
            ->from('Device', 'd')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('d.imei like :search_text')
            ->andWhere('dt.name = :device_type')
            ->orderBy('d.createdAt', 'desc')
            ->setMaxResults(5)
            ->setParameter('search_text', "%$searchText%")
            ->setParameter('device_type', $deviceType)
        ;

        if ($brand) {
            $queryBuilder
                ->andWhere('d.brand = :brand')
                ->setParameter('brand', $brand)
            ;
        }

        if ($model) {
            $queryBuilder
                ->andWhere('d.model = :model')
                ->setParameter('model', $model)
            ;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll(FetchMode::COLUMN);
    }
}
