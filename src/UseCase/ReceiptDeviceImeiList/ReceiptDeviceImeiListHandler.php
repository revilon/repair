<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptDeviceImeiList;

use Doctrine\DBAL\DBALException;

class ReceiptDeviceImeiListHandler
{
    private ReceiptDeviceImeiListManager $manager;

    /**
     * @param ReceiptDeviceImeiListManager $manager
     */
    public function __construct(ReceiptDeviceImeiListManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     * @param string|null $model
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $searchText, string $deviceType, ?string $brand, ?string $model): array
    {
        $imeiList = $this->manager->getImeiList($searchText, $deviceType, $brand, $model);

        return array_values(array_unique($imeiList));
    }
}
