<?php

declare(strict_types=1);

namespace App\UseCase\EditReceiptAgo;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class EditReceiptAgoManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return string
     *
     * @throws DBALException
     */
    public function getReceiptUpdatedAt(int $orderId): string
    {
        $sql = <<<SQL
            select
                updatedAt
            from Receipt
            where orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return (string)$stmt->fetchColumn();
    }
}
