<?php

declare(strict_types=1);

namespace App\UseCase\EditReceiptAgo;

use Doctrine\DBAL\DBALException;
use Knp\Bundle\TimeBundle\Templating\Helper\TimeHelper;

class EditReceiptAgoHandler
{
    private EditReceiptAgoManager $manager;
    private TimeHelper $agoTimeFormatter;

    /**
     * @param EditReceiptAgoManager $manager
     * @param TimeHelper $agoTimeFormatter
     */
    public function __construct(EditReceiptAgoManager $manager, TimeHelper $agoTimeFormatter)
    {
        $this->manager = $manager;
        $this->agoTimeFormatter = $agoTimeFormatter;
    }

    /**
     * @param int $orderId
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(int $orderId): string
    {
        $receiptUpdatedAt = $this->manager->getReceiptUpdatedAt($orderId);

        return $this->agoTimeFormatter->diff($receiptUpdatedAt);
    }
}
