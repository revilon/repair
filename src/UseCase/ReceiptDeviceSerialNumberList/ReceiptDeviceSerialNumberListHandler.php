<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptDeviceSerialNumberList;

use Doctrine\DBAL\DBALException;

class ReceiptDeviceSerialNumberListHandler
{
    private ReceiptDeviceSerialNumberListManager $manager;

    /**
     * @param ReceiptDeviceSerialNumberListManager $manager
     */
    public function __construct(ReceiptDeviceSerialNumberListManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     * @param string|null $model
     * @param string|null $imei
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $searchText, string $deviceType, ?string $brand, ?string $model, ?string $imei): array
    {
        $serialNumberList = $this->manager->getSerialNumberList($searchText, $deviceType, $brand, $model, $imei);

        return array_values(array_unique($serialNumberList));
    }
}
