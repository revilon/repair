<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptDeviceSerialNumberList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class ReceiptDeviceSerialNumberListManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     * @param string|null $model
     * @param string|null $imei
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getSerialNumberList(
        string $searchText,
        string $deviceType,
        ?string $brand,
        ?string $model,
        ?string $imei
    ): array {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->select('d.serialNumber')
            ->from('Device', 'd')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('d.serialNumber like :search_text')
            ->andWhere('dt.name = :device_type')
            ->orderBy('d.createdAt', 'desc')
            ->setMaxResults(5)
            ->setParameter('search_text', "%$searchText%")
            ->setParameter('device_type', $deviceType)
        ;

        if ($brand) {
            $queryBuilder
                ->andWhere('d.brand = :brand')
                ->setParameter('brand', $brand)
            ;
        }

        if ($model) {
            $queryBuilder
                ->andWhere('d.model = :model')
                ->setParameter('model', $model)
            ;
        }

        if ($imei) {
            $queryBuilder
                ->andWhere('d.imei = :imei')
                ->setParameter('imei', $imei)
            ;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll(FetchMode::COLUMN);
    }
}
