<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationEditUser;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;

class AdministrationEditUserManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param array $userData
     *
     * @throws DBALException
     */
    public function saveUser(array $userData): void
    {
        $userData['roles'] = Type::getType(Types::JSON)->convertToDatabaseValue(
            [$userData['roles']],
            $this->manager->getConnection()->getDatabasePlatform()
        );

        $this->manager->update('User', $userData, [
            'username' => $userData['username'],
        ]);
    }
}
