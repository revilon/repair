<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationEditUser;

use App\Entity\User;
use App\Enum\UserRoleEnum;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\EditUserForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AdministrationEditUserHandler
{
    private AdministrationEditUserManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private EncoderFactoryInterface $encoderFactory;
    private RouterInterface $router;

    /**
     * @param AdministrationEditUserManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param EncoderFactoryInterface $encoderFactory
     * @param RouterInterface $router
     */
    public function __construct(
        AdministrationEditUserManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        EncoderFactoryInterface $encoderFactory,
        RouterInterface $router
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->encoderFactory = $encoderFactory;
        $this->router = $router;
    }

    /**
     * @param string $userId
     * @param Request $request
     *
     * @throws DBALException
     */
    public function handle(string $userId, Request $request): void
    {
        $editUserForm = $this->formFactory->create(EditUserForm::class, null, [
            'action' => $this->router->generate('app_administration_edituser', ['userId' => $userId])
        ]);
        $editUserForm->handleRequest($request);

        if (!$editUserForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$editUserForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($editUserForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $editUserData = $editUserForm->getData();

        $isRoleWithPercentRepair = in_array(
            $editUserData['roles'],
            [UserRoleEnum::ROLE_REPAIRER, UserRoleEnum::ROLE_ADMIN],
            true
        );

        if (empty($editUserData['percentRepairer']) && $isRoleWithPercentRepair) {
            throw new FormBadRequestHttpException([], 'Указанная роль должна быть с установленной процентной ставкой.');
        }

        if (!empty($editUserData['password'])) {
            $userEncoder = $this->encoderFactory->getEncoder(User::class);
            $userPasswordHash = $userEncoder->encodePassword($editUserData['password'], null);
            $editUserData['password'] = $userPasswordHash;
        } else {
            unset($editUserData['password']);
        }

        $this->manager->saveUser($editUserData);
    }
}
