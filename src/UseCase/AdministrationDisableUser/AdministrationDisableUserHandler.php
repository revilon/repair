<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationDisableUser;

use Doctrine\DBAL\DBALException;

class AdministrationDisableUserHandler
{
    private AdministrationDisableUserManager $manager;

    /**
     * @param AdministrationDisableUserManager $manager
     */
    public function __construct(
        AdministrationDisableUserManager $manager
    ) {
        $this->manager = $manager;
    }

    /**
     * @param string $userId
     *
     * @throws DBALException
     */
    public function handle(string $userId): void
    {
        $this->manager->disableUser($userId);
    }
}
