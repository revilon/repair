<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationDisableUser;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class AdministrationDisableUserManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $userId
     *
     * @throws DBALException
     */
    public function disableUser(string $userId): void
    {
        $this->manager->update(
            'User',
            [
                'active' => 0,
            ],
            [
                'id' => $userId,
            ]
        );
    }
}
