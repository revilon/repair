<?php

declare(strict_types=1);

namespace App\UseCase\EditIndex;

use App\Doctrine\Dbal\Type\PreliminarilySetType;
use App\Enum\FlashTypeEnum;
use App\Form\Type\EditReceiptSearchForm;
use App\Form\Type\ReceiptEditForm;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class EditIndexHandler
{
    private EditIndexManager $manager;
    private FormFactoryInterface $formFactory;
    private FlashBagInterface $flashBag;

    /**
     * @param EditIndexManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FlashBagInterface $flashBag
     */
    public function __construct(
        EditIndexManager $manager,
        FormFactoryInterface $formFactory,
        FlashBagInterface $flashBag
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->flashBag = $flashBag;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $receiptEditSearchForm = $this->formFactory->create(EditReceiptSearchForm::class);
        $receiptEditSearchForm->handleRequest($request);

        if (!$receiptEditSearchForm->isSubmitted() || !$receiptEditSearchForm->isValid()) {
            return [
                'receipt_edit_search_form' => $receiptEditSearchForm->createView(),
            ];
        }

        $orderId = $receiptEditSearchForm->getData()['orderId'] ?? 0;
        $receipt = $this->manager->getReceipt($orderId);

        if (!$receipt) {
            $this->flashBag->add(FlashTypeEnum::DANGER, "Квитанция с номером $orderId не найдена");

            return [
                'receipt_edit_search_form' => $receiptEditSearchForm->createView(),
            ];
        }

        $receipt['preliminarilyList'] = $this->getPreliminarilyList($receipt['preliminarilyList']);
        $receipt['DeviceOwner']['id'] = $receipt['deviceOwnerId'];
        $receipt['DeviceOwner']['name'] = $receipt['deviceOwner'];
        $receipt['DeviceOwner']['address'] = $receipt['deviceOwnerAddress'];
        $receipt['DeviceOwner']['phone'] = $receipt['deviceOwnerPhone'];
        $receipt['DeviceOwner']['email'] = $receipt['deviceOwnerEmail'];
        $receipt['DeviceOwner']['gender'] = $receipt['deviceOwner'];

        $deviceList = $this->manager->getDeviceList($receipt['deviceId']);
        foreach ($deviceList as $device) {
            $device['DeviceType']['name'] = $device['typeDevice'];

            $receipt['DeviceCollection'][] = $device;
        }

        $receiptEditForm = $this->formFactory->create(ReceiptEditForm::class, $receipt);
        $receiptEditForm->handleRequest($request);

        return [
            'receipt_edit_form' => $receiptEditForm->createView(),
            'receipt_edit_search_form' => $receiptEditSearchForm->createView(),
            'receipt_order_id' => $receipt['orderId'],
            'receipt' => $receipt,
        ];
    }

    /**
     * @param string|null $preliminarilyList
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getPreliminarilyList(?string $preliminarilyList): array
    {
        $preliminarilySetType = Type::getType(PreliminarilySetType::NAME);

        return $preliminarilySetType->convertToPHPValue(
            $preliminarilyList,
            $this->manager->getDatabasePlatform()
        );
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceTypeList(): array
    {
        $deviceTypeList = $this->manager->getDeviceTypeList();

        return array_values($deviceTypeList);
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptCreatorList(): array
    {
        return $this->manager->getReceiptCreatorList();
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptReturnerList(): array
    {
        return $this->manager->getReceiptReturnerList();
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getRepairerList(): array
    {
        return $this->manager->getRepairerList();
    }
}
