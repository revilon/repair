<?php

declare(strict_types=1);

namespace App\UseCase\EditIndex;

use App\Component\Manager\Executer\RowManager;
use App\Enum\UserRoleEnum;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class EditIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getReceipt(int $orderId): ?array
    {
        $sql = <<<SQL
            select
                r.orderId,
                r.preliminarilyList,
                r.defect,
                r.devicePreview,
                dow.id deviceOwnerId,
                dow.name deviceOwner,
                r.deviceCode,
                r.prePrice,
                dow.phone deviceOwnerPhone,
                dow.address deviceOwnerAddress,
                dow.email deviceOwnerEmail,
                r.deviceId,
                r.receiptStatus,
                r.repairDate,
                r.workPerformed,
                r.workDescription,
                r.payRepairer,
                r.priceRepair,
                r.priceSparePart,
                u_repairer.surname repairerName,
                u_creator.surname creatorName,
                u_returner.surname returnerName,
                u_last_edit.surname lastEditName,
                r.orderDate,
                r.returnDate,
                r.updatedAt,
                r.guaranteeReturnDescription,
                r.guaranteeReturnDate,
                r.repairerId,
                r.deviceReturnerId,
                r.receiptCreatorId
            from Receipt r
            inner join DeviceOwner dow on r.deviceOwnerId = dow.id
            left join User u_repairer on r.repairerId = u_repairer.id
            left join User u_creator on r.receiptCreatorId = u_creator.id
            left join User u_returner on r.deviceReturnerId = u_returner.id
            left join User u_last_edit on r.lastEditUserId = u_last_edit.id
            where orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param string $parentDevice
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceList(string $parentDevice): array
    {
        $sql = <<<SQL
            select
                d.id,
                dt.name typeDevice,
                d.brand,
                d.model,
                d.serialNumber,
                d.imei,
                d.parentDeviceId
            from Device d
            inner join DeviceType dt on d.deviceTypeId = dt.id
            where d.id = :parent_device or d.parentDeviceId = :parent_device
            order by d.parentDeviceId
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'parent_device' => $parentDevice,
        ]);

        return $stmt->fetchAll();
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceTypeList(): array
    {
        $sql = <<<SQL
            select
                dt.name
            from DeviceType dt
            order by dt.createdAt
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return $stmt->fetchAll(FetchMode::COLUMN);
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptCreatorList(): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();
        $queryBuilder
            ->addSelect('id')
            ->addSelect('surname')
            ->from('User')
        ;

        foreach ([UserRoleEnum::ROLE_ADMIN, UserRoleEnum::ROLE_SELLER] as $key => $role) {
            $queryBuilder->orWhere("json_contains(roles, :receipt_creator_role_$key)");
            $queryBuilder->setParameter("receipt_creator_role_$key", "[\"$role\"]");
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $this->manager->getResultPairList($stmt, 'id', 'surname');
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptReturnerList(): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();
        $queryBuilder
            ->addSelect('id')
            ->addSelect('surname')
            ->from('User')
        ;

        foreach ([UserRoleEnum::ROLE_ADMIN, UserRoleEnum::ROLE_SELLER] as $key => $role) {
            $queryBuilder->orWhere("json_contains(roles, :receipt_returner_role_$key)");
            $queryBuilder->setParameter("receipt_returner_role_$key", "[\"$role\"]");
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $this->manager->getResultPairList($stmt, 'id', 'surname');
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getRepairerList(): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();
        $queryBuilder
            ->addSelect('id')
            ->addSelect('surname')
            ->from('User')
        ;

        foreach ([UserRoleEnum::ROLE_ADMIN, UserRoleEnum::ROLE_REPAIRER] as $key => $role) {
            $queryBuilder->orWhere("json_contains(roles, :repairer_role_$key)");
            $queryBuilder->setParameter("repairer_role_$key", "[\"$role\"]");
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $this->manager->getResultPairList($stmt, 'id', 'surname');
    }

    /**
     * @return AbstractPlatform
     *
     * @throws DBALException
     */
    public function getDatabasePlatform(): AbstractPlatform
    {
        return $this->manager->getConnection()->getDatabasePlatform();
    }
}
