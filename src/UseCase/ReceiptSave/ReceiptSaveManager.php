<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptSave;

use App\Component\Manager\Executer\RowManager;
use App\Doctrine\Dbal\Type\PreliminarilySetType;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\Type;

class ReceiptSaveManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $name
     *
     * @throws DBALException
     */
    public function saveDeviceType(string $name): void
    {
        $this->manager->upsert(
            'DeviceType',
            [
                'name' => $name,
            ],
            []
        );
    }

    /**
     * @param string $name
     *
     * @return string
     *
     * @throws DBALException
     */
    public function getDeviceTypeId(string $name): ?string
    {
        $sql = <<<SQL
            select id
            from DeviceType
            where name = :name
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'name' => $name,
        ]);

        return $stmt->fetch(FetchMode::COLUMN);
    }

    /**
     * @return string
     */
    public function generateUniqueId(): string
    {
        return $this->manager->generateUniqueId();
    }

    /**
     * @return int
     *
     * @throws DBALException
     */
    public function getSupposedId(): int
    {
        $sql = '
            select max(r.orderId) + 1
            from Receipt r
            limit 1
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return (int)$stmt->fetch(FetchMode::COLUMN) ?: 1;
    }

    /**
     * @param array $deviceOwner
     *
     * @throws DBALException
     */
    public function addDeviceOwner(array $deviceOwner): void
    {
        $this->manager->insert('DeviceOwner', $deviceOwner);
    }

    /**
     * @param array $deviceList
     *
     * @throws DBALException
     */
    public function addDeviceList(array $deviceList): void
    {
        $this->manager->insertBulk('Device', $deviceList);
    }

    /**
     * @param array $receipt
     *
     * @throws DBALException
     */
    public function addReceipt(array $receipt): void
    {
        $receipt['preliminarilyList'] = Type::getType(PreliminarilySetType::NAME)->convertToDatabaseValue(
            $receipt['preliminarilyList'],
            $this->manager->getConnection()->getDatabasePlatform()
        );

        $this->manager->insert('Receipt', $receipt);
    }
}
