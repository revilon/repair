<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptSave;

use App\Entity\User;
use App\Enum\ReceiptStatusEnum;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\ReceiptForm;
use Doctrine\DBAL\DBALException;
use Exception;
use LogicException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use function in_array;

class ReceiptSaveHandler
{
    private ReceiptSaveManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private TokenStorageInterface $tokenStorage;
    private LoggerInterface $logger;

    /**
     * @param ReceiptSaveManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param TokenStorageInterface $tokenStorage
     * @param LoggerInterface $logger
     */
    public function __construct(
        ReceiptSaveManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        TokenStorageInterface $tokenStorage,
        LoggerInterface $logger
    ) {
        $this->formFactory = $formFactory;
        $this->manager = $manager;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     * @throws Exception
     */
    public function handle(Request $request): array
    {
        $form = $this->formFactory->create(ReceiptForm::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted()) {
            throw new LogicException(sprintf('Form "%s" is not submit', ReceiptForm::class));
        }

        if (!$form->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($form);

            throw new FormBadRequestHttpException($errorList, 'Ошибка при сохранении квитанции');
        }

        $formData = $form->getData();

        $deviceOwner = $this->getDeviceOwnerByFormData($formData['DeviceOwner']);
        $deviceList = $this->getDeviceListByFormData($formData['DeviceCollection']);
        $receipt = $this->getReceiptByFormData($formData, $deviceOwner, $deviceList);

        $this->manager->addDeviceOwner($deviceOwner);
        $this->manager->addDeviceList($deviceList);
        $this->manager->addReceipt($receipt);

        return [
            'orderId' => $receipt['orderId'],
        ];
    }

    /**
     * @param array $formData
     *
     * @return array
     */
    private function getDeviceOwnerByFormData(array $formData): array
    {
        $deviceOwnerId = $this->manager->generateUniqueId();
        $formData['id'] = $deviceOwnerId;

        return $formData;
    }

    /**
     * @param array $formData
     *
     * @return array
     *
     * @throws DBALException
     */
    private function getDeviceListByFormData(array $formData): array
    {
        $deviceList = [];

        if (!$formData) {
            throw new BadRequestHttpException('Required minimum one device for registration receipt');
        }

        $formData = array_values($formData);

        foreach ($formData as $deviceIndex => $device) {
            foreach ($device as $fieldName => $value) {
                if ($fieldName === 'DeviceType') {
                    $deviceTypeName = ucfirst($value['name']);

                    $this->manager->saveDeviceType($deviceTypeName);
                    $deviceTypeId = $this->manager->getDeviceTypeId($deviceTypeName);

                    $deviceList[$deviceIndex]['deviceTypeId'] = $deviceTypeId;
                    $deviceList[$deviceIndex]['parentDeviceId'] = null;

                    continue;
                }

                if ($deviceIndex === 0) {
                    $parentDeviceId = $this->manager->generateUniqueId();

                    $deviceList[$deviceIndex]['id'] = $parentDeviceId;
                } else {
                    $childDeviceId = $this->manager->generateUniqueId();

                    $deviceList[$deviceIndex]['id'] = $childDeviceId;
                    $deviceList[$deviceIndex]['parentDeviceId'] = $deviceList[0]['id'];
                }

                $deviceList[$deviceIndex][$fieldName] = $value;
            }
        }

        return $deviceList;
    }

    /**
     * @param array $formData
     * @param array $deviceOwner
     * @param array $deviceList
     *
     * @return array
     *
     * @throws DBALException
     */
    private function getReceiptByFormData(array $formData, array $deviceOwner, array $deviceList): array
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            throw new AuthenticationException('Authentication not found');
        }

        /** @var User $user */
        $user = $token->getUser();

        foreach ($formData as $fieldName => $value) {
            if (in_array($fieldName, ['DeviceOwner', 'DeviceCollection'], true)) {
                continue;
            }

            $receipt[$fieldName] = $value;
        }

        $receipt['orderId'] = $this->manager->getSupposedId();
        $receipt['deviceOwnerId'] = $deviceOwner['id'];
        $receipt['deviceId'] = $deviceList[0]['id'];
        $receipt['receiptCreatorId'] = $user->getId();
        $receipt['lastEditUserId'] = $user->getId();
        $receipt['orderDate'] = date('Y-m-d');
        $receipt['receiptStatus'] = ReceiptStatusEnum::NEW;

        return $receipt;
    }
}
