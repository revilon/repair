<?php

declare(strict_types=1);

namespace App\UseCase\ProfileIndex;

use App\Entity\User;
use App\Enum\MonthEnum;
use DateTime;
use Doctrine\DBAL\DBALException;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class ProfileIndexHandler
{
    private ProfileIndexManager $manager;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param ProfileIndexManager $manager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ProfileIndexManager $manager, TokenStorageInterface $tokenStorage)
    {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function handle(): array
    {
        $user = $this->getUser();

        $efficiencyRepairer = $this->getEfficiencyRepairer($user->getId());
        $repairerPay = $this->getRepairerPay($user->getId());
        $repairerPayTotal = 0;
        $currentMonth = MonthEnum::getNumberNameMonthList()[date('n')];

        foreach ($repairerPay as $item) {
            $repairerPayTotal += $item[1];
        }

        return [
            'repairer_name' => $user->getSurname(),
            'efficiency_repairer' => $efficiencyRepairer,
            'repairer_pay' => $repairerPay,
            'repairer_pay_total' => $repairerPayTotal,
            'current_month' => $currentMonth,
        ];
    }

    /**
     * @param string $repairerId
     *
     * @return array
     *
     * @throws DBALException
     * @throws Exception
     */
    private function getRepairerPay(string $repairerId): array
    {
        $now = new DateTime();

        $dateStart = (clone $now)->modify('first day of this month today');
        $dateEnd = (clone $now)->modify('last day of this month 23:59:59');

        $repairerPay = $this->manager->getRepairerPay($repairerId, $dateStart, $dateEnd);
        $repairerPay = array_map('array_values', $repairerPay);

        return $repairerPay;
    }

    /**
     * @param string $repairerId
     *
     * @return array
     *
     * @throws DBALException
     * @throws Exception
     */
    private function getEfficiencyRepairer(string $repairerId): array
    {
        $now = new DateTime('first day of this month');

        $dateStart = (clone $now)->modify('first day of -3 month today');
        $dateEnd = (clone $now)->modify('last day of this month 23:59:59');

        $payRepairerList = $this->manager->getEfficiencyRepairerMonthList($repairerId, $dateStart, $dateEnd);

        $prev3Month = (clone $now)->modify('-3 month');
        $prev2Month = (clone $now)->modify('-2 month');
        $prev1Month = (clone $now)->modify('-1 month');
        $nowMonth = (clone $now)->modify('this month');

        $prev3MonthNumber = $prev3Month->format('m');
        $prev2MonthNumber = $prev2Month->format('m');
        $prev1MonthNumber = $prev1Month->format('m');
        $nowMonthNumber = $nowMonth->format('m');

        $nowMonthPay = $payRepairerList[$nowMonthNumber] ?? 0;
        $prev1MonthPay = $payRepairerList[$prev1MonthNumber] ?? 0;
        $prev2MonthPay = $payRepairerList[$prev2MonthNumber] ?? 0;
        $prev3MonthPay = $payRepairerList[$prev3MonthNumber] ?? 0;

        return [
            [
                'pay' => $prev2MonthPay,
                'month' => MonthEnum::getNumberNameMonthList()[$prev2Month->format('n')],
                'percent' => $prev2MonthPay && $prev3MonthPay
                    ? round(($prev2MonthPay - $prev3MonthPay) / $prev3MonthPay, 2) * 100
                    : 0,
            ],
            [
                'pay' => $prev1MonthPay,
                'month' => MonthEnum::getNumberNameMonthList()[$prev1Month->format('n')],
                'percent' => $prev1MonthPay && $prev2MonthPay
                    ? round(($prev1MonthPay - $prev2MonthPay) / $prev2MonthPay, 2) * 100
                    : 0,
            ],
            [
                'pay' => $nowMonthPay,
                'month' => MonthEnum::getNumberNameMonthList()[$nowMonth->format('n')],
                'percent' => $nowMonthPay && $prev1MonthPay
                    ? round(($nowMonthPay - $prev1MonthPay) / $prev1MonthPay, 2) * 100
                    : 0,
            ],
        ];
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if ($token === null) {
            throw new AuthenticationException('Authentication exception');
        }

        /** @var User $user */
        $user = $token->getUser();

        return $user;
    }
}
