<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptModelList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class ReceiptModelListManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getModelList(string $searchText, string $deviceType, ?string $brand): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->select('d.model')
            ->from('Device', 'd')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('d.model like :search_text')
            ->andWhere('dt.name = :device_type')
            ->orderBy('d.createdAt', 'desc')
            ->setMaxResults(5)
            ->setParameter('search_text', "%$searchText%")
            ->setParameter('device_type', $deviceType)
        ;

        if ($brand) {
            $queryBuilder
                ->andWhere('d.brand = :brand')
                ->setParameter('brand', $brand)
            ;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll(FetchMode::COLUMN);
    }
}
