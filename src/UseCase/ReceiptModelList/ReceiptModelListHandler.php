<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptModelList;

use Doctrine\DBAL\DBALException;

class ReceiptModelListHandler
{
    private ReceiptModelListManager $manager;

    /**
     * @param ReceiptModelListManager $manager
     */
    public function __construct(ReceiptModelListManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     * @param string|null $brand
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $searchText, string $deviceType, ?string $brand): array
    {
        $modelList = $this->manager->getModelList($searchText, $deviceType, $brand);

        return array_values(array_unique($modelList));
    }
}
