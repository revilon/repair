<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationCreateUser;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;

class AdministrationCreateUserManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $username
     * @param string $passwordHash
     * @param string $surname
     * @param float|null $percentRepairer
     * @param string $role
     *
     * @throws DBALException
     */
    public function saveUser(
        string $username,
        string $passwordHash,
        string $surname,
        ?float $percentRepairer,
        string $role
    ): void {
        $jsonType = Type::getType(Types::JSON);

        $roles = $jsonType->convertToDatabaseValue([$role], $this->manager->getConnection()->getDatabasePlatform());

        $this->manager->insert('User', [
            'id' => $this->manager->generateUniqueId(),
            'username' => $username,
            'password' => $passwordHash,
            'surname' => $surname,
            'percentRepairer' => $percentRepairer,
            'roles' => $roles,
        ]);
    }

    /**
     * @param string $username
     *
     * @return string|null
     *
     * @throws DBALException
     */
    public function getUserId(string $username): ?string
    {
        $sql = <<<SQL
            select id
            from User
            where username = :username
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'username' => $username,
        ]);

        return $stmt->fetch(FetchMode::COLUMN) ?: null;
    }
}
