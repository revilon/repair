<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationCreateUser;

use App\Entity\User;
use App\Enum\UserRoleEnum;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\CreateUserForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AdministrationCreateUserHandler
{
    private AdministrationCreateUserManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private EncoderFactoryInterface $encoderFactory;

    /**
     * @param AdministrationCreateUserManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(
        AdministrationCreateUserManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        EncoderFactoryInterface $encoderFactory
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param Request $request
     *
     * @return void
     *
     * @throws DBALException
     */
    public function handle(Request $request): void
    {
        $createUserForm = $this->formFactory->create(CreateUserForm::class);
        $createUserForm->handleRequest($request);

        if (!$createUserForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$createUserForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($createUserForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $createUserData = $createUserForm->getData();

        $user = $this->manager->getUserId($createUserData['username']);

        if ($user) {
            $message = "Пользователь с логином $createUserData[username] уже существует.";

            throw new FormBadRequestHttpException([], $message);
        }

        $isRoleWithPercentRepair = in_array(
            $createUserData['roles'],
            [UserRoleEnum::ROLE_REPAIRER, UserRoleEnum::ROLE_ADMIN],
            true
        );

        if (empty($createUserData['percentRepairer']) && $isRoleWithPercentRepair) {
            throw new FormBadRequestHttpException([], 'Указанная роль должна быть с установленной процентной ставкой.');
        }

        $userEncoder = $this->encoderFactory->getEncoder(User::class);
        $userPasswordHash = $userEncoder->encodePassword($createUserData['password'], null);

        $this->manager->saveUser(
            $createUserData['username'],
            $userPasswordHash,
            $createUserData['surname'],
            $createUserData['percentRepairer'],
            $createUserData['roles'],
        );
    }
}
