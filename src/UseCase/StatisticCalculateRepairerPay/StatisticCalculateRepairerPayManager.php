<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateRepairerPay;

use App\Component\Manager\Executer\RowManager;
use DateTime;
use Doctrine\DBAL\DBALException;

class StatisticCalculateRepairerPayManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getRepairerPayList(DateTime $dateStart, DateTime $dateEnd): array
    {
        $sql = <<<SQL
            select
                sum(r.payRepairer) repairerPay,
                u.id repairerId,
                u.surname repairerName
            from Receipt r
            inner join User u on r.repairerId = u.id
            where 1
                and r.repairDate between :date_start and :date_end
            group by r.repairerId, u.surname
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'date_start' => $dateStart->format('Y-m-d H:i:s'),
            'date_end' => $dateEnd->format('Y-m-d H:i:s'),
        ]);

        return $this->manager->getResultList($stmt, 'repairerId');
    }
}
