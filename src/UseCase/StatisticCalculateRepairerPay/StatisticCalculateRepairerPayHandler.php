<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateRepairerPay;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\StatisticRepairerPayForm;
use DateTime;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class StatisticCalculateRepairerPayHandler
{
    private StatisticCalculateRepairerPayManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;

    /**
     * @param StatisticCalculateRepairerPayManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        StatisticCalculateRepairerPayManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $statisticRepairerPayForm = $this->formFactory->create(StatisticRepairerPayForm::class);
        $statisticRepairerPayForm->handleRequest($request);

        if (!$statisticRepairerPayForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$statisticRepairerPayForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($statisticRepairerPayForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $statisticRepairerPayData = $statisticRepairerPayForm->getData();

        $dateStart = DateTime::createFromFormat('d.m.Y', $statisticRepairerPayData['dateStart']);
        $dateStart->setTime(00, 00, 00);
        $dateEnd = DateTime::createFromFormat('d.m.Y', $statisticRepairerPayData['dateEnd']);
        $dateEnd->setTime(23, 59, 00);

        $repairerPayList = $this->manager->getRepairerPayList($dateStart, $dateEnd);

        foreach ($repairerPayList as &$repairerPay) {
            $repairerPay['repairerPay'] = number_format((float)$repairerPay['repairerPay'], 0, ',', ' ');
        }

        return $repairerPayList;
    }
}
