<?php

declare(strict_types=1);

namespace App\UseCase\EditReceiptSave;

use App\Component\Manager\Executer\RowManager;
use App\Doctrine\Dbal\Type\PreliminarilySetType;
use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\Type;

class EditReceiptSaveManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getMainDevice(int $orderId): array
    {
        $sql = <<<SQL
            select
                d.id,
                dt.name typeDevice,
                d.brand,
                d.model,
                d.serialNumber,
                d.imei
            from Receipt r
            inner join Device d on r.deviceId = d.id
            left join DeviceType dt on d.deviceTypeId = dt.id
            where r.orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return $stmt->fetch();
    }

    /**
     * @param string $mainDeviceId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getOtherDeviceList(string $mainDeviceId): array
    {
        $sql = <<<SQL
            select
                d.id,
                dt.name typeDevice,
                d.brand,
                d.model,
                d.serialNumber,
                d.imei
            from Device d
            left join DeviceType dt on d.deviceTypeId = dt.id
            where d.parentDeviceId = :main_device_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'main_device_id' => $mainDeviceId,
        ]);

        $result = [];

        foreach ($stmt->fetchAll() as $item) {
            $result[$item['id']] = $item;
        }

        return $result;
    }

    /**
     * @param array $idList
     *
     * @throws DBALException
     */
    public function removeDeviceList(array $idList): void
    {
        $this->manager->deleteBulk('Device', $idList);
    }

    /**
     * @param array $deviceList
     *
     * @throws DBALException
     */
    public function saveDeviceList(array $deviceList): void
    {
        $this->manager->upsertBulk('Device', $deviceList, [
            'brand',
            'model',
            'imei',
            'serialNumber',
            'deviceTypeId',
        ]);
    }

    /**
     * @param string $name
     *
     * @return string
     *
     * @throws DBALException
     */
    public function getDeviceTypeId(string $name): ?string
    {
        $sql = <<<SQL
            select id
            from DeviceType
            where name = :name
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'name' => $name,
        ]);

        return $stmt->fetch(FetchMode::COLUMN);
    }

    /**
     * @return string
     */
    public function generateUniqueId(): string
    {
        return $this->manager->generateUniqueId();
    }

    /**
     * @param string $name
     *
     * @throws DBALException
     */
    public function saveDeviceType(string $name): void
    {
        $this->manager->upsert(
            'DeviceType',
            [
                'name' => $name,
            ],
            []
        );
    }

    /**
     * @param array $deviceOwner
     *
     * @throws DBALException
     */
    public function saveDeviceOwner(array $deviceOwner): void
    {
        $this->manager->upsert('DeviceOwner', $deviceOwner, [
            'name',
            'email',
            'address',
            'phone',
        ]);
    }

    /**
     * @param array $receipt
     *
     * @throws DBALException
     */
    public function saveReceipt(array $receipt): void
    {
        $receipt += [
            'orderDate' => '1970-01-01',
            'deviceOwnerId' => 0,
            'deviceId' => 0,
        ];

        $preliminarilySetType = Type::getType(PreliminarilySetType::NAME);

        $receipt['preliminarilyList'] =  $preliminarilySetType->convertToDatabaseValue(
            $receipt['preliminarilyList'],
            $this->manager->getConnection()->getDatabasePlatform()
        );

        $this->manager->upsert('Receipt', $receipt, [
            'defect',
            'preliminarilyList',
            'deviceCode',
            'devicePreview',
            'prePrice',
            'workPerformed',
            'workDescription',
            'receiptStatus',
            'payRepairer',
            'priceRepair',
            'priceSparePart',
            'repairDate',
            'returnDate',
            'guaranteeReturnDescription',
            'guaranteeReturnDate',
            'receiptCreatorId',
            'repairerId',
            'deviceReturnerId',
        ]);
    }
}
