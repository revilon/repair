<?php

declare(strict_types=1);

namespace App\UseCase\EditReceiptSave;

use App\Entity\User;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\ReceiptEditForm;
use App\Form\Type\ReceiptForm;
use Doctrine\DBAL\DBALException;
use LogicException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use function in_array;

class EditReceiptSaveHandler
{
    private EditReceiptSaveManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param EditReceiptSaveManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EditReceiptSaveManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        TokenStorageInterface $tokenStorage
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     *
     * @throws DBALException
     */
    public function handle(Request $request): void
    {
        $receiptEditForm = $this->formFactory->create(ReceiptEditForm::class);
        $receiptEditForm->handleRequest($request);

        if (!$receiptEditForm->isSubmitted()) {
            throw new LogicException(sprintf('Form "%s" is not submit', ReceiptForm::class));
        }

        if (!$receiptEditForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($receiptEditForm);

            throw new FormBadRequestHttpException($errorList, 'Ошибка при сохранении квитанции');
        }

        $formData = $receiptEditForm->getData();

        $deviceOwner = $formData['DeviceOwner'];
        $deviceList = $this->getDeviceListByFormData((int)$formData['orderId'], $formData['DeviceCollection']);
        $receipt = $this->getReceiptByFormData($formData);

        $this->manager->saveDeviceOwner($deviceOwner);
        $this->manager->saveDeviceList($deviceList);
        $this->manager->saveReceipt($receipt);
    }

    /**
     * @param int $orderId
     * @param array $formData
     *
     * @return array
     *
     * @throws DBALException
     */
    private function getDeviceListByFormData(int $orderId, array $formData): array
    {
        if (!$formData) {
            throw new BadRequestHttpException('Должно остаться хотя бы одно устройство');
        }

        $mainDevice = $this->manager->getMainDevice($orderId);

        $existDeviceList = array_merge(
            [$mainDevice['id'] => $mainDevice],
            $this->manager->getOtherDeviceList($mainDevice['id']),
        );

        $freshDeviceList = array_values($formData);

        $freshDeviceIdList = array_column($freshDeviceList, 'id');
        $existDeviceIdList = array_column($existDeviceList, 'id');
        $removeDeviceIdList = array_diff($existDeviceIdList, $freshDeviceIdList);

        if (in_array($mainDevice['id'], $removeDeviceIdList, true)) {
            throw new FormBadRequestHttpException([], 'Удалять основное устройство запрещено. Перезагрузите страницу');
        }

        $this->manager->removeDeviceList($removeDeviceIdList);

        foreach ($freshDeviceList as $deviceIndex => $device) {
            if ($deviceIndex > 0) {
                if (!$device['id']) {
                    $deviceId = $this->manager->generateUniqueId();
                    $freshDeviceList[$deviceIndex]['id'] = $deviceId;
                }

                if (!$device['parentDeviceId']) {
                    $freshDeviceList[$deviceIndex]['parentDeviceId'] = $mainDevice['id'];
                }
            }

            $deviceTypeName = ucfirst($device['DeviceType']['name']);
            $this->manager->saveDeviceType($deviceTypeName);
            $deviceTypeId = $this->manager->getDeviceTypeId($deviceTypeName);

            $freshDeviceList[$deviceIndex]['deviceTypeId'] = $deviceTypeId;

            unset($freshDeviceList[$deviceIndex]['DeviceType']);
        }

        return $freshDeviceList;
    }

    /**
     * @param array $formData
     *
     * @return array
     */
    private function getReceiptByFormData(array $formData): array
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            throw new AuthenticationException('Authentication not found');
        }

        /** @var User $user */
        $user = $token->getUser();

        foreach ($formData as $fieldName => $value) {
            if (in_array($fieldName, ['DeviceOwner', 'DeviceCollection'], true)) {
                continue;
            }

            $receipt[$fieldName] = $value;
        }

        $receipt['lastEditUserId'] = $user->getId();

        return $receipt;
    }
}
