<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationActiveUser;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class AdministrationActiveUserManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $userId
     *
     * @throws DBALException
     */
    public function activeUser(string $userId): void
    {
        $this->manager->update(
            'User',
            [
                'active' => 1,
            ],
            [
                'id' => $userId,
            ]
        );
    }
}
