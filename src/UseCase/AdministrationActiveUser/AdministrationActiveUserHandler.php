<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationActiveUser;

use Doctrine\DBAL\DBALException;

class AdministrationActiveUserHandler
{
    private AdministrationActiveUserManager $manager;

    /**
     * @param AdministrationActiveUserManager $manager
     */
    public function __construct(AdministrationActiveUserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $userId
     *
     * @throws DBALException
     */
    public function handle(string $userId): void
    {
        $this->manager->activeUser($userId);
    }
}
