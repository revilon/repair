<?php

declare(strict_types=1);

namespace App\UseCase\RepairerIndex;

use App\Enum\FlashTypeEnum;
use App\Enum\PreliminarilyEnum;
use App\Enum\ReceiptStatusEnum;
use App\Form\Type\RepairerReceiptSearchForm;
use App\Form\Type\RepairerWorkForm;
use App\src\ReceiptStatus;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use function in_array;

class RepairerIndexHandler
{
    private RepairerIndexManager $manager;
    private FormFactoryInterface $formFactory;
    private FlashBagInterface $flashBag;

    /**
     * @param RepairerIndexManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FlashBagInterface $flashBag
     */
    public function __construct(
        RepairerIndexManager $manager,
        FormFactoryInterface $formFactory,
        FlashBagInterface $flashBag
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->flashBag = $flashBag;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $searchReceiptForm = $this->formFactory->create(RepairerReceiptSearchForm::class);
        $searchReceiptForm->handleRequest($request);

        if (!$searchReceiptForm->isSubmitted() || !$searchReceiptForm->isValid()) {
            return [
                'search_receipt_form' => $searchReceiptForm->createView(),
            ];
        }

        $orderId = $searchReceiptForm->getData()['orderId'] ?? 0;
        $receipt = $this->manager->getReceipt($orderId);

        if (!$receipt) {
            $this->flashBag->add(FlashTypeEnum::DANGER, "Квитанция с номером $orderId не найдена");

            return [
                'search_receipt_form' => $searchReceiptForm->createView(),
            ];
        }

        $receipt['preliminarilyList'] = $this->getPreliminarilyList($receipt);
        $receipt['deviceList'] = $this->manager->getDeviceList($receipt['deviceId']);

        $receiptStatusForWork = [ReceiptStatusEnum::NEW, ReceiptStatusEnum::WORK, ReceiptStatusEnum::GUARANTEE_RETURN];
        if (!in_array($receipt['receiptStatus'], $receiptStatusForWork, true)) {
            return [
                'receipt' => $receipt,
                'search_receipt_form' => $searchReceiptForm->createView(),
                'receipt_status' => ReceiptStatusEnum::getTranslatePrefix() . $receipt['receiptStatus']
            ];
        }

        $repairerWorkForm = $this->formFactory->create(RepairerWorkForm::class, [
            'orderId' => $orderId,
            'receiptStatus' => null,
            'workPerformed' => $receipt['workPerformed'],
            'workDescription' => $receipt['workDescription'],
            'payRepairer' => $receipt['payRepairer'],
            'priceRepair' => $receipt['priceRepair'],
            'priceSparePart' => $receipt['priceSparePart'],
        ]);

        return [
            'receipt' => $receipt,
            'repairer_work_form' => $repairerWorkForm->createView(),
            'search_receipt_form' => $searchReceiptForm->createView(),
        ];
    }

    /**
     * @param array $receipt
     *
     * @return array
     */
    private function getPreliminarilyList(array $receipt): array
    {
        if (!isset($receipt['preliminarilyList'])) {
            return [];
        }

        $preliminarilyList = explode(',', $receipt['preliminarilyList']) ?? [];

        return array_map(
            fn($preliminarily) => PreliminarilyEnum::getTranslatePrefix() . $preliminarily,
            $preliminarilyList,
        );
    }
}
