<?php

declare(strict_types=1);

namespace App\UseCase\RepairerReceiptAgo;

use Doctrine\DBAL\DBALException;
use Knp\Bundle\TimeBundle\Templating\Helper\TimeHelper;

class RepairerReceiptAgoHandler
{
    private RepairerReceiptAgoManager $manager;
    private TimeHelper $agoTimeFormatter;

    /**
     * @param RepairerReceiptAgoManager $manager
     * @param TimeHelper $agoTimeFormatter
     */
    public function __construct(RepairerReceiptAgoManager $manager, TimeHelper $agoTimeFormatter)
    {
        $this->manager = $manager;
        $this->agoTimeFormatter = $agoTimeFormatter;
    }

    /**
     * @param int $orderId
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(int $orderId): string
    {
        $receiptUpdatedAt = $this->manager->getReceiptUpdatedAt($orderId);

        return $this->agoTimeFormatter->diff($receiptUpdatedAt);
    }
}
