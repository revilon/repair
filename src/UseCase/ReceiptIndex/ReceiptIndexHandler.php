<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptIndex;

use App\Form\Type\ReceiptForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class ReceiptIndexHandler
{
    private FormFactoryInterface $formFactory;
    private ReceiptIndexManager $manager;

    /**
     * @param ReceiptIndexManager $manager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        ReceiptIndexManager $manager,
        FormFactoryInterface $formFactory
    ) {
        $this->formFactory = $formFactory;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $form = $this->formFactory->create(ReceiptForm::class);

        $form->handleRequest($request);

        return [
            'form_receipt' => $form->createView(),
            'supposed_number_receipt' => $this->manager->getSupposedId(),
        ];
    }

    /**
     * @param string $name
     *
     * @throws DBALException
     */
    public function saveDeviceType(string $name): void
    {
        $this->manager->saveDeviceType($name);
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceTypeList(): array
    {
        $deviceTypeList = $this->manager->getDeviceTypeList();

        return array_values($deviceTypeList);
    }
}
