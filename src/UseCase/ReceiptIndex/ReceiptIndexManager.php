<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptIndex;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class ReceiptIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return int
     *
     * @throws DBALException
     */
    public function getSupposedId(): int
    {
        $sql = '
            select max(r.orderId) + 1
            from Receipt r
            limit 1
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return (int)$stmt->fetch(FetchMode::COLUMN) ?: 1;
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceTypeList(): array
    {
        $sql = '
            select
              dt.name
            from DeviceType dt
            order by dt.createdAt
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return $stmt->fetchAll(FetchMode::COLUMN);
    }

    /**
     * @param string $name
     *
     * @throws DBALException
     */
    public function saveDeviceType(string $name): void
    {
        $this->manager->upsert(
            'DeviceType',
            [
                'name' => $name,
            ],
            []
        );
    }
}
