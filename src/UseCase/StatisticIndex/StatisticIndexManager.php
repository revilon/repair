<?php

declare(strict_types=1);

namespace App\UseCase\StatisticIndex;

use App\Component\Manager\Executer\RowManager;

class StatisticIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }
}
