<?php

declare(strict_types=1);

namespace App\UseCase\StatisticIndex;

use App\Form\Type\StatisticCostSparePartForm;
use App\Form\Type\StatisticNetProfitForm;
use App\Form\Type\StatisticRepairerPayForm;
use App\Form\Type\StatisticTotalPriceForm;
use DateTimeImmutable;
use Exception;
use Symfony\Component\Form\FormFactoryInterface;

class StatisticIndexHandler
{
    private StatisticIndexManager $manager;
    private FormFactoryInterface $formFactory;

    /**
     * @param StatisticIndexManager $manager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        StatisticIndexManager $manager,
        FormFactoryInterface $formFactory
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function handle(): array
    {
        $now = new DateTimeImmutable();

        $statisticRepairerPayForm = $this->formFactory->create(StatisticRepairerPayForm::class, [
            'dateStart' => $now->modify('first day of this month')->format('d.m.Y'),
            'dateEnd' => $now->modify('last day of this month')->format('d.m.Y'),
        ]);

        $statisticNetProfitForm = $this->formFactory->create(StatisticNetProfitForm::class, [
            'dateStart' => $now->modify('first day of this month')->format('d.m.Y'),
            'dateEnd' => $now->modify('last day of this month')->format('d.m.Y'),
        ]);

        $statisticCostSparePartForm = $this->formFactory->create(StatisticCostSparePartForm::class, [
            'dateStart' => $now->modify('first day of this month')->format('d.m.Y'),
            'dateEnd' => $now->modify('last day of this month')->format('d.m.Y'),
        ]);

        $statisticTotalPriceForm = $this->formFactory->create(StatisticTotalPriceForm::class, [
            'dateStart' => $now->modify('first day of this month')->format('d.m.Y'),
            'dateEnd' => $now->modify('last day of this month')->format('d.m.Y'),
        ]);

        return [
            'statistic_repairer_pay_form' => $statisticRepairerPayForm->createView(),
            'statistic_net_profit_form' => $statisticNetProfitForm->createView(),
            'statistic_cost_spare_part_form' => $statisticCostSparePartForm->createView(),
            'statistic_total_price_form' => $statisticTotalPriceForm->createView(),
        ];
    }
}
