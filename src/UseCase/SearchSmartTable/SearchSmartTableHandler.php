<?php

declare(strict_types=1);

namespace App\UseCase\SearchSmartTable;

use App\Entity\User;
use App\Enum\UserRoleEnum;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SearchSmartTableHandler
{
    private const MAX_LIMIT_SELECT_RECEIPT = 100;

    private SearchSmartTableManager $manager;
    private RouterInterface $router;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param SearchSmartTableManager $manager
     * @param RouterInterface $router
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        SearchSmartTableManager $manager,
        RouterInterface $router,
        TokenStorageInterface $tokenStorage
    ) {
        $this->manager = $manager;
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $tableDrawCount = $request->query->getInt('draw');
        $search = $request->query->get('search');
        $limit = $request->query->getInt('length');
        $offset = $request->query->getInt('start');

        if ($limit > self::MAX_LIMIT_SELECT_RECEIPT) {
            $limit = self::MAX_LIMIT_SELECT_RECEIPT;
        }

        $searchText = !empty($search['value']) ? $search['value'] : null;

        $receiptTotalCount = $this->manager->getReceiptTotalCount();
        $receiptFilteredCount = $this->manager->getReceiptFilteredCount($searchText);
        $receiptList = $this->manager->getReceiptList($searchText, $limit, $offset);

        $result = [
            'draw' => $tableDrawCount,
            'recordsFiltered' => $receiptFilteredCount,
            'recordsTotal' => $receiptTotalCount,
            'data' => [],
        ];

        foreach ($receiptList as $receipt) {
            if (in_array(UserRoleEnum::ROLE_REPAIRER, $this->getUser()->getRoles(), true)) {
                $orderLinkDetails = $this->router->generate(
                    'app_repairer_repairer',
                    ['orderId' => $receipt['orderId']]
                );
            } else {
                $orderLinkDetails = $this->router->generate(
                    'app_search_search',
                    ['orderId' => $receipt['orderId']]
                );
            }

            $row = [
                $receipt['deviceOwnerName'],
                $receipt['orderId'],
                $receipt['deviceInfo'],
                $receipt['defect'],
                $receipt['workPerformed'],
                'DT_RowId' => "receipt_$receipt[orderId]",
                'DT_RowData' => [
                    'order-link-details' => $orderLinkDetails,
                ],
            ];

            $result['data'][] = $row;
        }

        return $result;
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if ($token === null) {
            throw new AuthenticationException('Authentication exception');
        }

        /** @var User $user */
        $user = $token->getUser();

        return $user;
    }
}
