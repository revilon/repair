<?php

declare(strict_types=1);

namespace App\UseCase\SearchSmartTable;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class SearchSmartTableManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptTotalCount(): int
    {
        $sql = <<<SQL
            select count(*)
            from Receipt
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string|null $searchText
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptFilteredCount(?string $searchText): int
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder->addSelect('r.id');
        $queryBuilder->addSelect(
            'concat(dt.name, " ", d.brand, " ", d.model, " ", r.defect, " ", r.workPerformed) combineFields'
        );

        $queryBuilder->from('Receipt', 'r');
        $queryBuilder->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id');
        $queryBuilder->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id');
        $queryBuilder->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id');
        $queryBuilder->where('d.parentDeviceId is null');

        $queryBuilder->orWhere('d.model like :search_text');
        $queryBuilder->orWhere('d.brand like :search_text');
        $queryBuilder->orWhere('r.defect like :search_text');
        $queryBuilder->orWhere('r.workPerformed like :search_text');
        $queryBuilder->having('combineFields like :search_text');
        $queryBuilder->setParameter('search_text', "%$searchText%");

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return count($stmt->fetchAll());
    }

    /**
     * @param string $searchText
     * @param int $limit
     * @param int $offset
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptList(?string $searchText, int $limit, int $offset): array
    {
        if (empty($searchText)) {
            return [];
        }

        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->addSelect('do.name deviceOwnerName')
            ->addSelect('r.orderId')
            ->addSelect("concat(dt.name, ' ', d.brand, ' ', d.model) deviceInfo")
            ->addSelect('r.defect')
            ->addSelect('r.workPerformed')
        ;

        $queryBuilder->addSelect(
            'concat(dt.name, " ", d.brand, " ", d.model, " ", r.defect, " ", r.workPerformed) combineFields'
        );

        $queryBuilder
            ->from('Receipt', 'r')
            ->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id')
            ->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('d.parentDeviceId is null')
        ;

        $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('r.orderId', 'desc')
        ;

        $queryBuilder
            ->orWhere('d.model like :search_text')
            ->orWhere('d.brand like :search_text')
            ->orWhere('r.defect like :search_text')
            ->orWhere('r.workPerformed like :search_text')
            ->having('combineFields like :search_text')
            ->setParameter('search_text', "%$searchText%")
        ;

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll();
    }
}
