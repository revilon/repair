<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptBrandList;

use Doctrine\DBAL\DBALException;

class ReceiptBrandListHandler
{
    private ReceiptBrandListManager $manager;

    /**
     * @param ReceiptBrandListManager $manager
     */
    public function __construct(ReceiptBrandListManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $searchText, string $deviceType): array
    {
        $brandList = $this->manager->getBrandList($searchText, $deviceType);

        return array_values(array_unique($brandList));
    }
}
