<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptBrandList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class ReceiptBrandListManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     * @param string $deviceType
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getBrandList(string $searchText, string $deviceType): array
    {
        $sql = '
            select d.brand
            from Device d
            inner join DeviceType dt on d.deviceTypeId = dt.id
            where 1
                and d.brand like :search_text
                and dt.name = :device_type
            order by d.createdAt DESC
            limit 5
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'search_text' => "%$searchText%",
            'device_type' => $deviceType,
        ]);

        return $stmt->fetchAll(FetchMode::COLUMN);
    }
}
