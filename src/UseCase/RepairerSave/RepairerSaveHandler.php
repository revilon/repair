<?php

declare(strict_types=1);

namespace App\UseCase\RepairerSave;

use App\Entity\User;
use App\Enum\ReceiptStatusEnum;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\RepairerWorkForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use function in_array;

class RepairerSaveHandler
{
    private RepairerSaveManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param RepairerSaveManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        RepairerSaveManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        TokenStorageInterface $tokenStorage
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     *
     * @throws DBALException
     */
    public function handle(Request $request): void
    {
        $repairerWorkForm = $this->formFactory->create(RepairerWorkForm::class);
        $repairerWorkForm->handleRequest($request);

        if (!$repairerWorkForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$repairerWorkForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($repairerWorkForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $repairerWorkFormData = $repairerWorkForm->getData();
        $orderId = $repairerWorkFormData['orderId'];
        $receipt = $this->manager->getReceipt($orderId);
        $receiptStatus = $receipt['receiptStatus'];
        $repairResult = $repairerWorkFormData['repairResult'] ?? ReceiptStatusEnum::WORK;

        $receiptStatusForWork = [ReceiptStatusEnum::NEW, ReceiptStatusEnum::WORK, ReceiptStatusEnum::GUARANTEE_RETURN];
        if (!in_array($receiptStatus, $receiptStatusForWork, true)) {
            throw new FormBadRequestHttpException([], 'Результат ремонта уже был сохранен');
        }

        if ($receipt['repairerId'] !== null && $receipt['repairerId'] !== $this->getUser()->getId()) {
            throw new FormBadRequestHttpException([], 'Ремонтом уже занимается другой мастер');
        }

        $repairerWorkData['receiptStatus'] = $repairResult;
        $repairerWorkData['workPerformed'] = $repairerWorkFormData['workPerformed'];
        $repairerWorkData['workDescription'] = $repairerWorkFormData['workDescription'];
        $repairerWorkData['payRepairer'] = $this->getPayRepairer($repairerWorkFormData) ?: null;
        $repairerWorkData['priceRepair'] = $repairerWorkFormData['priceRepair'];
        $repairerWorkData['priceSparePart'] = $repairerWorkFormData['priceSparePart'];
        $repairerWorkData['orderId'] = $orderId;
        $repairerWorkData['repairerId'] = $this->getUser()->getId();
        $repairerWorkData['lastEditUserId'] = $this->getUser()->getId();

        $completeWorkStatusList = [ReceiptStatusEnum::REPAIR_SUCCESS_SHOP, ReceiptStatusEnum::REPAIR_SUCCESS_SHOP];
        $isRepairerCompleteWork = in_array($repairResult, $completeWorkStatusList, true);

        if ($isRepairerCompleteWork) {
            $repairerWorkData['repairDate'] = date('Y-m-d H:i:s');
        }

        $this->manager->saveReceipt($repairerWorkData);
    }

    /**
     * @return float
     *
     * @throws DBALException
     */
    public function getRepairerPercent(): float
    {
        $userId = $this->getUser()->getId();

        return $this->manager->getRepairerPercent($userId);
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if ($token === null) {
            throw new AuthenticationException('Authentication exception');
        }

        /** @var User $user */
        $user = $token->getUser();

        return $user;
    }

    /**
     * @param array $repairerWorkFormData
     *
     * @return int
     *
     * @throws DBALException
     */
    private function getPayRepairer(array $repairerWorkFormData): int
    {
        $priceRepair = $repairerWorkFormData['priceRepair'];
        $priceSparePart = $repairerWorkFormData['priceSparePart'];

        $payRepairer = ($priceRepair - $priceSparePart) * $this->getRepairerPercent();

        if ($payRepairer < 0) {
            $payRepairer = 0;
        }

        return (int)round($payRepairer, 0, PHP_ROUND_HALF_UP);
    }
}
