<?php

declare(strict_types=1);

namespace App\UseCase\RepairerSave;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class RepairerSaveManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param array $repairerWorkData
     *
     * @throws DBALException
     */
    public function saveReceipt(array $repairerWorkData): void
    {
        $repairerWorkData += [
            'defect' => '',
            'orderDate' => '1970-01-01',
            'deviceOwnerId' => 0,
            'deviceId' => 0,
            'receiptCreatorId' => 0,
            'lastEditUserId' => 0,
        ];

        $this->manager->upsert('Receipt', $repairerWorkData, [
            'receiptStatus',
            'workPerformed',
            'workDescription',
            'payRepairer',
            'priceRepair',
            'priceSparePart',
            'repairerId',
            'repairDate',
            'lastEditUserId',
        ]);
    }

    /**
     * @param int $orderId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getReceipt(int $orderId): array
    {
        $sql = <<<SQL
            select
                receiptStatus,
                repairerId
            from Receipt
            where orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return $stmt->fetch();
    }

    /**
     * @param string $repairerId
     *
     * @return float
     *
     * @throws DBALException
     */
    public function getRepairerPercent(string $repairerId): float
    {
        $sql = <<<SQL
            select percentRepairer
            from User
            where id = :repairer_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'repairer_id' => $repairerId,
        ]);

        return (float)$stmt->fetchColumn() ?: .0;
    }
}
