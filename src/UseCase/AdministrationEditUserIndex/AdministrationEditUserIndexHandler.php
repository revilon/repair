<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationEditUserIndex;

use App\Form\Type\EditUserForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class AdministrationEditUserIndexHandler
{
    private AdministrationEditUserIndexManager $manager;
    private FormFactoryInterface $formFactory;
    private RouterInterface $router;

    /**
     * @param AdministrationEditUserIndexManager $manager
     * @param FormFactoryInterface $formFactory
     * @param RouterInterface $router
     */
    public function __construct(
        AdministrationEditUserIndexManager $manager,
        FormFactoryInterface $formFactory,
        RouterInterface $router
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    /**
     * @param string $userId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $userId): array
    {
        $user = $this->manager->getUser($userId);

        if (!$user) {
            throw new NotFoundHttpException("Пользователя с id $userId не существует.");
        }

        $editUserForm = $this->formFactory->create(EditUserForm::class, $user, [
            'action' => $this->router->generate('app_administration_edituser', ['userId' => $userId])
        ]);

        return [
            'edit_user_form' => $editUserForm->createView(),
        ];
    }
}
