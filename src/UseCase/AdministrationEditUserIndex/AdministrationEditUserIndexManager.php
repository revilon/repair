<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationEditUserIndex;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;

class AdministrationEditUserIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $userId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getUser(string $userId): ?array
    {
        $sql = <<<SQL
            select
                username,
                surname,
                percentRepairer,
                roles
            from User
            where id = :user_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'user_id' => $userId,
        ]);

        $result = $stmt->fetch() ?: null;

        if (!$result) {
            return null;
        }

        $result['roles'] = Type::getType(Types::JSON)->convertToPHPValue(
            $result['roles'],
            $this->manager->getConnection()->getDatabasePlatform()
        )[0];

        return $result;
    }
}
