<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateCostSparePart;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\StatisticCostSparePartForm;
use DateTime;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class StatisticCalculateCostSparePartHandler
{
    private StatisticCalculateCostSparePartManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;

    /**
     * @param StatisticCalculateCostSparePartManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        StatisticCalculateCostSparePartManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @param Request $request
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(Request $request): string
    {
        $statisticCostSparePartForm = $this->formFactory->create(StatisticCostSparePartForm::class);
        $statisticCostSparePartForm->handleRequest($request);

        if (!$statisticCostSparePartForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$statisticCostSparePartForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($statisticCostSparePartForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $statisticCostSparePartData = $statisticCostSparePartForm->getData();

        $dateStart = DateTime::createFromFormat('d.m.Y', $statisticCostSparePartData['dateStart']);
        $dateStart->setTime(00, 00, 00);
        $dateEnd = DateTime::createFromFormat('d.m.Y', $statisticCostSparePartData['dateEnd']);
        $dateEnd->setTime(23, 59, 00);

        $costSparePart = $this->manager->getCostSparePart($dateStart, $dateEnd);
        $costSparePart = number_format($costSparePart, 0, ',', ' ');

        return $costSparePart;
    }
}
