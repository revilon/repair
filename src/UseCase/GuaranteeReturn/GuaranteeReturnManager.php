<?php

declare(strict_types=1);

namespace App\UseCase\GuaranteeReturn;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class GuaranteeReturnManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param array $guaranteeReturnData
     *
     * @throws DBALException
     */
    public function saveGuaranteeReturn(array $guaranteeReturnData): void
    {
        $guaranteeReturnData += [
            'defect' => '',
            'orderDate' => '1970-01-01',
            'deviceOwnerId' => 0,
            'deviceId' => 0,
            'receiptCreatorId' => 0,
        ];

        $this->manager->upsert('Receipt', $guaranteeReturnData, [
            'receiptStatus',
            'guaranteeReturnDate',
            'guaranteeReturnDescription',
            'lastEditUserId',
        ]);
    }
}
