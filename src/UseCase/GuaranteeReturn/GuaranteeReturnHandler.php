<?php

declare(strict_types=1);

namespace App\UseCase\GuaranteeReturn;

use App\Entity\User;
use App\Enum\ReceiptStatusEnum;
use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\GuaranteeReturnForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class GuaranteeReturnHandler
{
    private GuaranteeReturnManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer $formErrorSerializer;
    private TokenStorageInterface $tokenStorage;

    /**
     * @param GuaranteeReturnManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        GuaranteeReturnManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer,
        TokenStorageInterface $tokenStorage
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     *
     * @throws DBALException
     */
    public function handle(Request $request): void
    {
        $guaranteeReturnForm = $this->formFactory->create(GuaranteeReturnForm::class);
        $guaranteeReturnForm->handleRequest($request);

        if (!$guaranteeReturnForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$guaranteeReturnForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($guaranteeReturnForm);

            throw new FormBadRequestHttpException($errorList, 'Ошибка в форме');
        }

        $guaranteeReturnData = $guaranteeReturnForm->getData();
        $guaranteeReturnData['guaranteeReturnDate'] = date('Y-m-d H:i:s');
        $guaranteeReturnData['receiptStatus'] = ReceiptStatusEnum::GUARANTEE_RETURN;
        $guaranteeReturnData['lastEditUserId'] = $this->getUser()->getId();

        $this->manager->saveGuaranteeReturn($guaranteeReturnData);
    }

    /**
     * @return User
     */
    private function getUser(): User
    {
        $token = $this->tokenStorage->getToken();

        if ($token === null) {
            throw new AuthenticationException('Authentication exception');
        }

        /** @var User $user */
        $user = $token->getUser();

        return $user;
    }
}
