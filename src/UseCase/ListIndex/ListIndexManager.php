<?php

declare(strict_types=1);

namespace App\UseCase\ListIndex;

use App\Component\Manager\Executer\RowManager;

class ListIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }
}
