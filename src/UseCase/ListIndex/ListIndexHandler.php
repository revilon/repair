<?php

declare(strict_types=1);

namespace App\UseCase\ListIndex;

use Symfony\Component\HttpFoundation\Request;

class ListIndexHandler
{
    private ListIndexManager $manager;

    /**
     * @param ListIndexManager $manager
     */
    public function __construct(ListIndexManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function handle(Request $request): array
    {
        return [];
    }
}
