<?php

declare(strict_types=1);

namespace App\UseCase\StatisticNetProfitChart;

use App\Enum\MonthEnum;
use App\Enum\MonthShortEnum;
use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;
use ReflectionException;

class StatisticNetProfitChartHandler
{
    private StatisticNetProfitChartManager $manager;

    /**
     * @param StatisticNetProfitChartManager $manager
     */
    public function __construct(StatisticNetProfitChartManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     * @throws ReflectionException
     */
    public function handle(): array
    {
        $now = new DateTimeImmutable();
        $dateStart = $now->modify('-1 year');
        $dateEnd = $now->modify('now 23:59:59');

        $netProfitPairList = $this->manager->getNetProfitList($dateStart, $dateEnd);

        $interval = new DateInterval('P1M');
        $dateRange = new DatePeriod($dateStart, $interval, $dateEnd);

        $monthList = [];
        $monthShortList = [];
        $netProfitList = [];
        $netProfitFormatList = [];
        $efficiencyList = [];

        /** @var DateTime[] $dateRange */
        foreach ($dateRange as $date) {
            $monthShortList[] = MonthShortEnum::getNameMonth((int)$date->format('n'));
            $monthList[] = MonthEnum::getNameMonth((int)$date->format('n'));

            if (isset($netProfitPairList[$date->format('Y-m')])) {
                $netProfit = (int)$netProfitPairList[$date->format('Y-m')];
                $netProfitList[] = $netProfit;
                $netProfitFormatList[] = number_format($netProfit, 0, ',', ' ');
            } else {
                $netProfitList[] = 0;
                $netProfitFormatList[] = '0';
            }

            $currentPay = end($netProfitList);
            $previousPay = prev($netProfitList) ?: 0;

            $efficiencyList[] = $this->calculateEfficiency($currentPay, $previousPay);
        }

        return [
            'net_profit_list' => $netProfitList,
            'net_profit_format_list' => $netProfitFormatList,
            'month_list' => $monthList,
            'month_short_list' => $monthShortList,
            'efficiency_list' => $efficiencyList,
        ];
    }

    /**
     * @param int $firstPay
     * @param int $secondPay
     *
     * @return float
     */
    private function calculateEfficiency(int $firstPay, int $secondPay): float
    {
        if (!$firstPay || !$secondPay) {
            return 0;
        }

        $efficiency = round(($firstPay - $secondPay) / $secondPay, 2) * 100;

        return (int)$efficiency;
    }
}
