<?php

declare(strict_types=1);

namespace App\UseCase\StatisticNetProfitChart;

use App\Component\Manager\Executer\RowManager;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;

class StatisticNetProfitChartManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param DateTimeImmutable $dateStart
     * @param DateTimeImmutable $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getNetProfitList(DateTimeImmutable $dateStart, DateTimeImmutable $dateEnd): array
    {
        $sql = <<<SQL
            select
                sum(r.priceRepair - r.priceSparePart - r.payRepairer) netProfit,
                date_format(r.returnDate, '%Y-%m') returnYearMonth
            from Receipt r
            where r.returnDate between :dateStart and :dateEnd
            group by returnYearMonth
            order by returnYearMonth
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'dateStart' => $dateStart->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
        ]);

        return $this->manager->getResultPairList($stmt, 'returnYearMonth', 'netProfit');
    }
}
