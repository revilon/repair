<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateNetProfit;

use App\Exception\FormBadRequestHttpException;
use App\Form\FormErrorSerializer;
use App\Form\Type\StatisticNetProfitForm;
use DateTime;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class StatisticCalculateNetProfitHandler
{
    private StatisticCalculateNetProfitManager $manager;
    private FormFactoryInterface $formFactory;
    private FormErrorSerializer$formErrorSerializer;

    /**
     * @param StatisticCalculateNetProfitManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        StatisticCalculateNetProfitManager $manager,
        FormFactoryInterface $formFactory,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * @param Request $request
     *
     * @return string
     *
     * @throws DBALException
     */
    public function handle(Request $request): string
    {
        $statisticNetProfitForm = $this->formFactory->create(StatisticNetProfitForm::class);
        $statisticNetProfitForm->handleRequest($request);

        if (!$statisticNetProfitForm->isSubmitted()) {
            throw new FormBadRequestHttpException([], 'Форма не была отправлена');
        }

        if (!$statisticNetProfitForm->isValid()) {
            $errorList = $this->formErrorSerializer->getFormErrorList($statisticNetProfitForm);

            throw new FormBadRequestHttpException($errorList, 'Не корректные данные');
        }

        $statisticNetProfitData = $statisticNetProfitForm->getData();

        $dateStart = DateTime::createFromFormat('d.m.Y', $statisticNetProfitData['dateStart']);
        $dateStart->setTime(00, 00, 00);
        $dateEnd = DateTime::createFromFormat('d.m.Y', $statisticNetProfitData['dateEnd']);
        $dateEnd->setTime(23, 59, 00);

        $netProfit = $this->manager->getNetProfit($dateStart, $dateEnd);
        $netProfit = number_format($netProfit, 0, ',', ' ');

        return $netProfit;
    }
}
