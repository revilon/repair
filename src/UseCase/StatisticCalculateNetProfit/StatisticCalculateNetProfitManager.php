<?php

declare(strict_types=1);

namespace App\UseCase\StatisticCalculateNetProfit;

use App\Component\Manager\Executer\RowManager;
use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class StatisticCalculateNetProfitManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getNetProfit(DateTime $dateStart, DateTime $dateEnd): int
    {
        $sql = <<<SQL
            select
                sum(r.priceRepair - r.priceSparePart - r.payRepairer) netProfit
            from Receipt r
            where r.returnDate between :dateStart and :dateEnd
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'dateStart' => $dateStart->format('Y-m-d H:i:s'),
            'dateEnd' => $dateEnd->format('Y-m-d H:i:s'),
        ]);

        return (int)$stmt->fetch(FetchMode::COLUMN) ?: 0;
    }
}
