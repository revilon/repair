<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptDeviceOwnerAddressList;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;

class ReceiptDeviceOwnerAddressListHandler
{
    private DadataProvider $dadataProvider;

    /**
     * @param DadataProvider $dadataProvider
     */
    public function __construct(DadataProvider $dadataProvider)
    {
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DadataException
     */
    public function handle(string $searchText): array
    {
        $addressList = $this->dadataProvider->getAddressDtoList($searchText);

        $resultList = [];

        foreach ($addressList as $addressDto) {
            $resultList[] = $addressDto->getAddress();
        }

        return array_unique($resultList);
    }
}
