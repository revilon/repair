<?php

declare(strict_types=1);

namespace App\UseCase\StatisticEfficiencyBar;

use App\Component\Manager\Executer\RowManager;
use App\Enum\UserRoleEnum;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;

class StatisticEfficiencyBarManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $repairerId
     * @param DateTimeImmutable $dateStart
     * @param DateTimeImmutable $dateEnd
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getEfficiencyRepairerMonthList(
        string $repairerId,
        DateTimeImmutable $dateStart,
        DateTimeImmutable $dateEnd
    ): array {
        $sql = <<<SQL
            select
                sum(r.payRepairer) pay_repairer,
                date_format(r.repairDate, '%m') as month_repair
            from Receipt r
            where 1
                and r.repairDate between :date_start and :date_end
                and r.repairerId = :repairer_id
            group by month_repair
            order by month_repair
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'date_start' => $dateStart->format('Y-m-d H:i:s'),
            'date_end' => $dateEnd->format('Y-m-d H:i:s'),
            'repairer_id' => $repairerId,
        ]);

        return $this->manager->getResultPairList($stmt, 'month_repair', 'pay_repairer');
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getRepairerIdList(): array
    {
        $sql = <<<SQL
            select
                id,
                surname
            from User
            where json_contains(roles, :repairer_role)
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'repairer_role' => sprintf('["%s"]', UserRoleEnum::ROLE_REPAIRER)
        ]);

        return $stmt->fetchAll();
    }
}
