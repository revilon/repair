<?php

declare(strict_types=1);

namespace App\UseCase\StatisticEfficiencyBar;

use App\Enum\MonthEnum;
use DateTimeImmutable;
use Doctrine\DBAL\DBALException;
use Exception;
use ReflectionException;

class StatisticEfficiencyBarHandler
{
    private StatisticEfficiencyBarManager $manager;

    /**
     * @param StatisticEfficiencyBarManager $manager
     */
    public function __construct(StatisticEfficiencyBarManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     * @throws ReflectionException
     * @throws Exception
     */
    public function handle(): array
    {
        $now = new DateTimeImmutable('first day of this month');
        $dateStart = $now->modify('first day of -3 month today');
        $dateEnd = $now->modify('last day of this month 23:59:59');

        $efficiencyRepairerList = [];
        $repairerPayList = [];
        $repairerNameList = [];

        foreach ($this->manager->getRepairerIdList() as $repairer) {
            $repairerId = $repairer['id'];

            $repairerMonthPayList = $this->manager->getEfficiencyRepairerMonthList($repairerId, $dateStart, $dateEnd);

            $efficiencyRepairerList[$repairerId] = $this->getEfficiencyRepairer($repairerMonthPayList, $repairerId);
            $repairerPayList[$repairerId] = $this->getRepairerPayList($repairerMonthPayList);
            $repairerNameList[$repairerId] = $repairer['surname'];
        }

        $monthList[] = $now->modify('-2 month')->format('n');
        $monthList[] = $now->modify('-1 month')->format('n');
        $monthList[] = $now->modify('this month')->format('n');
        $monthList = array_map([MonthEnum::class, 'getNameMonth'], $monthList);

        return [
            'efficiency_repairer_list' => array_values($efficiencyRepairerList),
            'month_list' => $monthList,
            'repairer_pay_list' => $repairerPayList,
            'repairer_name_list' => $repairerNameList,
        ];
    }

    /**
     * @param array $repairerPayList
     *
     * @return array
     *
     * @throws Exception
     */
    private function getRepairerPayList(array $repairerPayList): array
    {
        $now = new DateTimeImmutable('first day of this month');

        $prev2Month = $now->modify('-2 month');
        $prev1Month = $now->modify('-1 month');
        $nowMonth = $now->modify('this month');

        $prev2MonthNumber = $prev2Month->format('m');
        $prev1MonthNumber = $prev1Month->format('m');
        $nowMonthNumber = $nowMonth->format('m');

        $nowMonthPay = $repairerPayList[$nowMonthNumber] ?? 0;
        $prev1MonthPay = $repairerPayList[$prev1MonthNumber] ?? 0;
        $prev2MonthPay = $repairerPayList[$prev2MonthNumber] ?? 0;

        return [
            number_format((float)$prev2MonthPay, 0, ',', ' '),
            number_format((float)$prev1MonthPay, 0, ',', ' '),
            number_format((float)$nowMonthPay, 0, ',', ' '),
        ];
    }

    /**
     * @param array $repairerPayList
     * @param string $repairerId
     *
     * @return array
     *
     * @throws Exception
     */
    private function getEfficiencyRepairer(array $repairerPayList, string $repairerId): array
    {
        $now = new DateTimeImmutable('first day of this month');

        $prev3Month = $now->modify('-3 month');
        $prev2Month = $now->modify('-2 month');
        $prev1Month = $now->modify('-1 month');
        $nowMonth = $now->modify('this month');

        $prev3MonthNumber = $prev3Month->format('m');
        $prev2MonthNumber = $prev2Month->format('m');
        $prev1MonthNumber = $prev1Month->format('m');
        $nowMonthNumber = $nowMonth->format('m');

        $nowMonthPay = $repairerPayList[$nowMonthNumber] ?? 0;
        $prev1MonthPay = $repairerPayList[$prev1MonthNumber] ?? 0;
        $prev2MonthPay = $repairerPayList[$prev2MonthNumber] ?? 0;
        $prev3MonthPay = $repairerPayList[$prev3MonthNumber] ?? 0;

        return [
            $repairerId,
            $this->calculateEfficiency((int)$prev2MonthPay, (int)$prev3MonthPay),
            $this->calculateEfficiency((int)$prev1MonthPay, (int)$prev2MonthPay),
            $this->calculateEfficiency((int)$nowMonthPay, (int)$prev1MonthPay),
        ];
    }

    /**
     * @param int $firstPay
     * @param int $secondPay
     *
     * @return float
     */
    private function calculateEfficiency(int $firstPay, int $secondPay): float
    {
        if (!$firstPay || !$secondPay) {
            return 0;
        }

        $efficiency = round(($firstPay - $secondPay) / $secondPay, 2) * 100;

        return (int)$efficiency;
    }
}
