<?php

declare(strict_types=1);

namespace App\UseCase\ReceiptPrint;

use App\Doctrine\Dbal\Type\PreliminarilySetType;
use App\Enum\PreliminarilyEnum;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReceiptPrintHandler
{
    private ReceiptPrintManager $manager;

    /**
     * @param ReceiptPrintManager $manager
     */
    public function __construct(ReceiptPrintManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(int $orderId): array
    {
        $receipt = $this->manager->getReceipt($orderId);

        if (!$receipt) {
            throw new NotFoundHttpException("Receipt $orderId not exist");
        }

        $receipt['preliminarilyList'] = Type::getType(PreliminarilySetType::NAME)->convertToPHPValue(
            $receipt['preliminarilyList'],
            $this->manager->getDatabasePlatform()
        );

        $receipt['preliminarilyList'] = array_map(
            fn($preliminarily) => PreliminarilyEnum::getTranslatePrefix() . $preliminarily,
            $receipt['preliminarilyList']
        );

        unset($preliminarily);

        $deviceList = $this->manager->getDeviceList($receipt['deviceId']);

        $receipt['deviceList'] = $deviceList;

        return $receipt;
    }
}
