<?php

declare(strict_types=1);

namespace App\UseCase\DeviceOwnerNameList;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;

class DeviceOwnerNameListManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceOwnerNameList(string $searchText): array
    {
        $sql = '
            select do.name
            from DeviceOwner do
            where do.name like :search_text
            order by do.createdAt DESC
            limit 5
        ';

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'search_text' => "%$searchText%"
        ]);

        return $stmt->fetchAll(FetchMode::COLUMN);
    }
}
