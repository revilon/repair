<?php

declare(strict_types=1);

namespace App\UseCase\DeviceOwnerNameList;

use App\Component\Dadata\Exception\DadataException;
use App\Component\Dadata\Provider\DadataProvider;
use Doctrine\DBAL\DBALException;

class DeviceOwnerNameListHandler
{
    private DeviceOwnerNameListManager $manager;
    private DadataProvider $dadataProvider;

    /**
     * @param DeviceOwnerNameListManager $manager
     * @param DadataProvider $dadataProvider
     */
    public function __construct(DeviceOwnerNameListManager $manager, DadataProvider $dadataProvider)
    {
        $this->manager = $manager;
        $this->dadataProvider = $dadataProvider;
    }

    /**
     * @param string $searchText
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(string $searchText): array
    {
        $deviceOwnerNameList = [];

        try {
            $fullNameDtoList = $this->dadataProvider->getFullNameDtoList($searchText);

            foreach ($fullNameDtoList as $fullNameDto) {
                $deviceOwnerNameList[] = $fullNameDto->getFullName();
            }
        } catch (DadataException $exception) {
            $deviceOwnerNameList = $this->manager->getDeviceOwnerNameList($searchText);
        }

        return array_values(array_unique($deviceOwnerNameList));
    }
}
