<?php

declare(strict_types=1);

namespace App\UseCase\SearchIndex;

use App\Enum\FlashTypeEnum;
use App\Enum\PreliminarilyEnum;
use App\Enum\ReceiptStatusEnum;
use App\Form\Type\GuaranteeReturnForm;
use App\Form\Type\RepairerReceiptSearchForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class SearchIndexHandler
{
    private SearchIndexManager $manager;
    private FormFactoryInterface $formFactory;
    private FlashBagInterface $flashBag;

    /**
     * @param SearchIndexManager $manager
     * @param FormFactoryInterface $formFactory
     * @param FlashBagInterface $flashBag
     */
    public function __construct(
        SearchIndexManager $manager,
        FormFactoryInterface $formFactory,
        FlashBagInterface $flashBag
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->flashBag = $flashBag;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws DBALException
     */
    public function handle(Request $request): array
    {
        $searchReceiptForm = $this->formFactory->create(RepairerReceiptSearchForm::class);
        $searchReceiptForm->handleRequest($request);

        if (!$searchReceiptForm->isSubmitted() || !$searchReceiptForm->isValid()) {
            return [
                'search_receipt_form' => $searchReceiptForm->createView(),
            ];
        }

        $orderId = $searchReceiptForm->getData()['orderId'] ?? 0;
        $receipt = $this->manager->getReceipt($orderId);

        if (!$receipt) {
            $this->flashBag->add(FlashTypeEnum::DANGER, "Квитанция с номером $orderId не найдена");

            return [
                'search_receipt_form' => $searchReceiptForm->createView(),
            ];
        }

        $receipt['preliminarilyList'] = $this->getPreliminarilyList($receipt);
        $receipt['deviceList'] = $this->manager->getDeviceList($receipt['deviceId']);

        if ($receipt['receiptStatus'] === ReceiptStatusEnum::REPAIR_SUCCESS_OWNER) {
            $guaranteeReturnForm = $this
                ->formFactory
                ->create(GuaranteeReturnForm::class, ['orderId' => $orderId])
                ->createView()
            ;
        } else {
            $guaranteeReturnForm = null;
        }

        return [
            'search_receipt_form' => $searchReceiptForm->createView(),
            'receipt' => $receipt,
            'receipt_status' => ReceiptStatusEnum::getTranslatePrefix() . $receipt['receiptStatus'],
            'guarantee_return_form' => $guaranteeReturnForm,
            'order_id' => $orderId,
        ];
    }

    /**
     * @param array $receipt
     *
     * @return array
     */
    private function getPreliminarilyList(array $receipt): array
    {
        if (!isset($receipt['preliminarilyList'])) {
            return [];
        }

        $preliminarilyList = explode(',', $receipt['preliminarilyList']) ?? [];

        return array_map(
            fn($preliminarily) => PreliminarilyEnum::getTranslatePrefix() . $preliminarily,
            $preliminarilyList,
        );
    }
}
