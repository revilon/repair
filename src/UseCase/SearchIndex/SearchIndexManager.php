<?php

declare(strict_types=1);

namespace App\UseCase\SearchIndex;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class SearchIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $orderId
     *
     * @return array|null
     *
     * @throws DBALException
     */
    public function getReceipt(int $orderId): ?array
    {
        $sql = <<<SQL
            select
                r.preliminarilyList,
                r.defect,
                r.devicePreview,
                dow.name deviceOwner,
                r.deviceCode,
                r.prePrice,
                dow.phone deviceOwnerPhone,
                dow.address deviceOwnerAddress,
                dow.email deviceOwnerEmail,
                r.deviceId,
                r.receiptStatus,
                r.repairDate,
                r.workPerformed,
                r.workDescription,
                r.payRepairer,
                r.priceRepair,
                r.priceSparePart,
                u_repairer.surname repairerName,
                u_creator.surname creatorName,
                u_returner.surname returnerName,
                u_last_edit.surname lastEditName,
                r.orderDate,
                r.returnDate,
                r.updatedAt,
                r.guaranteeReturnDescription,
                r.guaranteeReturnDate
            from Receipt r
            inner join DeviceOwner dow on r.deviceOwnerId = dow.id
            left join User u_repairer on r.repairerId = u_repairer.id
            left join User u_creator on r.receiptCreatorId = u_creator.id
            left join User u_returner on r.deviceReturnerId = u_returner.id
            left join User u_last_edit on r.lastEditUserId = u_last_edit.id
            where orderId = :order_id
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'order_id' => $orderId,
        ]);

        return $stmt->fetch() ?: null;
    }

    /**
     * @param string $parentDevice
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getDeviceList(string $parentDevice): array
    {
        $sql = <<<SQL
            select
                dt.name typeDevice,
                d.brand,
                d.model,
                d.serialNumber,
                d.imei
            from Device d
            inner join DeviceType dt on d.deviceTypeId = dt.id
            where d.id = :parent_device or d.parentDeviceId = :parent_device
            order by d.parentDeviceId
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'parent_device' => $parentDevice,
        ]);

        return $stmt->fetchAll();
    }
}
