<?php

declare(strict_types=1);

namespace App\UseCase\ListTableWaitFilter;

use App\Component\Manager\Executer\RowManager;
use App\Enum\ReceiptStatusEnum;
use Doctrine\DBAL\DBALException;

class ListTableWaitManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptWaitTotalCount(): int
    {
        $sql = <<<SQL
            select count(*)
            from Receipt
            where receiptStatus = :receipt_wait_status
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql, [
            'receipt_wait_status' => ReceiptStatusEnum::WORK,
        ]);

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string|null $searchText
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptWaitFilteredCount(?string $searchText): int
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->addSelect('count(*)')
            ->from('Receipt', 'r')
            ->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id')
            ->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('r.receiptStatus = :receipt_wait_status')
            ->setParameter('receipt_wait_status', ReceiptStatusEnum::WORK)
        ;

        switch (true) {
            case is_numeric($searchText):
                $queryBuilder->andWhere('r.orderId like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;

            case $searchText !== null:
                $queryBuilder->andWhere('do.name like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string $searchText
     * @param int $limit
     * @param int $offset
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptWaitList(?string $searchText, int $limit, int $offset): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->addSelect('r.orderId')
            ->addSelect('unix_timestamp(r.orderDate) orderDate')
            ->addSelect('do.name deviceOwnerName')
            ->addSelect("concat(dt.name, ' ', d.brand, ' ', d.model) deviceInfo")
            ->addSelect('r.defect')
            ->addSelect('r.prePrice')
            ->addSelect("concat(do.phone, ' ', do.address) deviceOwnerContact")
            ->addSelect('r.workPerformed')
            ->addSelect('r.receiptStatus')
            ->from('Receipt', 'r')
            ->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id')
            ->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
            ->where('r.receiptStatus = :receipt_wait_status')
            ->setParameter('receipt_wait_status', ReceiptStatusEnum::WORK)
        ;

        $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('r.updatedAt', 'desc')
        ;

        switch (true) {
            case is_numeric($searchText):
                $queryBuilder->andWhere('r.orderId like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;

            case $searchText !== null:
                $queryBuilder->andWhere('do.name like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll();
    }
}
