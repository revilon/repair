<?php

declare(strict_types=1);

namespace App\UseCase\ListTableFilter;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class ListTableManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptTotalCount(): int
    {
        $sql = <<<SQL
            select count(*)
            from Receipt
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string|null $searchText
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getReceiptFilteredCount(?string $searchText): int
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->addSelect('count(*)')
            ->from('Receipt', 'r')
            ->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id')
            ->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
        ;

        switch (true) {
            case is_numeric($searchText):
                $queryBuilder->andWhere('r.orderId like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;

            case $searchText !== null:
                $queryBuilder->andWhere('do.name like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string $searchText
     * @param int $limit
     * @param int $offset
     *
     * @return array
     *
     * @throws DBALException
     */
    public function getReceiptList(?string $searchText, int $limit, int $offset): array
    {
        $queryBuilder = $this->manager->getConnection()->createQueryBuilder();

        $queryBuilder
            ->addSelect('r.orderId')
            ->addSelect('unix_timestamp(r.orderDate) orderDate')
            ->addSelect('do.name deviceOwnerName')
            ->addSelect("concat(dt.name, ' ', d.brand, ' ', d.model) deviceInfo")
            ->addSelect('r.defect')
            ->addSelect('r.prePrice')
            ->addSelect("concat(do.phone, ' ', do.address) deviceOwnerContact")
            ->addSelect('r.workPerformed')
            ->addSelect('r.receiptStatus')
            ->from('Receipt', 'r')
            ->innerJoin('r', 'DeviceOwner', 'do', 'r.deviceOwnerId = do.id')
            ->innerJoin('r', 'Device', 'd', 'r.deviceId = d.id')
            ->innerJoin('d', 'DeviceType', 'dt', 'd.deviceTypeId = dt.id')
        ;

        $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('r.orderId', 'desc')
        ;

        switch (true) {
            case is_numeric($searchText):
                $queryBuilder->andWhere('r.orderId like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;

            case $searchText !== null:
                $queryBuilder->andWhere('do.name like :search_text');
                $queryBuilder->setParameter('search_text', "%$searchText%");
                break;
        }

        $stmt = $this->manager->getConnection()->executeQuery(
            $queryBuilder->getSQL(),
            $queryBuilder->getParameters()
        );

        return $stmt->fetchAll();
    }
}
