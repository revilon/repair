<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationIndex;

use App\Enum\UserRoleEnum;
use App\Form\Type\CreateUserForm;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdministrationIndexHandler
{
    private AdministrationIndexManager $manager;
    private FormFactoryInterface $formFactory;
    private TranslatorInterface $translator;

    /**
     * @param AdministrationIndexManager $manager
     * @param FormFactoryInterface $formFactory
     * @param TranslatorInterface $translator
     */
    public function __construct(
        AdministrationIndexManager $manager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->translator = $translator;
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function handle(): array
    {
        $createUserForm = $this->formFactory->create(CreateUserForm::class);

        $userList = $this->manager->getUserList();

        foreach ($userList as &$user) {
            $userRoleList = json_decode($user['roles'], true, 512, JSON_THROW_ON_ERROR);

            $userRoleTranslateList = [];
            
            foreach ($userRoleList as $userRole) {
                $userRoleTranslateList[] = $this->translator->trans(UserRoleEnum::getTranslatePrefix() . $userRole);
            }

            $user['roles'] = $userRoleTranslateList;
        }

        return [
            'create_user_form' => $createUserForm->createView(),
            'user_list' => $userList,
        ];
    }
}
