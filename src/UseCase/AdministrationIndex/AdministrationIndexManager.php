<?php

declare(strict_types=1);

namespace App\UseCase\AdministrationIndex;

use App\Component\Manager\Executer\RowManager;
use Doctrine\DBAL\DBALException;

class AdministrationIndexManager
{
    private RowManager $manager;

    /**
     * @param RowManager $manager
     */
    public function __construct(RowManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    public function getUserList(): array
    {
        $sql = <<<SQL
            select
                id,
                username,
                surname,
                percentRepairer,
                roles,
                active
            from User
SQL;

        $stmt = $this->manager->getConnection()->executeQuery($sql);

        return $stmt->fetchAll();
    }
}
