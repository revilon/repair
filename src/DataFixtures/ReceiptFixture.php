<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Receipt;
use App\Enum\PreliminarilyEnum;
use App\Enum\ReceiptStatusEnum;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

class ReceiptFixture extends Fixture implements DependentFixtureInterface
{
    private int $orderIdIncrement = 5000;

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $receiptParamsList = [
            [
                'defect' => 'Не включается',
                'preliminarilyList' => [PreliminarilyEnum::IS_LIQUID],
                'deviceCode' => '1234',
                'devicePreview' => 'Царапины, потертости',
                'prePrice' => 1000,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('today'),
                'receiptStatus' => ReceiptStatusEnum::NEW,
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_FIRST),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_FIRST),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
            ],
            [
                'defect' => 'Нет изображения',
                'preliminarilyList' => [PreliminarilyEnum::IS_STRIKE, PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Удовлетворительное',
                'prePrice' => 2000,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-10 day today'),
                'workPerformed' => 'Замена дисплея',
                'workDescription' => 'Дополнительно восстановление кнопок громкости',
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_SHOP,
                'payRepairer' => 350,
                'priceRepair' => 1500,
                'priceSparePart' => 800,
                'repairDate' => new DateTime('-5 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_SECOND),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_SECOND),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
            [
                'defect' => 'Зависает',
                'preliminarilyList' => [PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Разъем hdmi отсутствует',
                'prePrice' => 1500,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-45 day today'),
                'workPerformed' => 'Чистка, замена термопасты',
                'workDescription' => 'Попадание влаги',
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                'payRepairer' => 600,
                'priceRepair' => 1200,
                'priceSparePart' => 0,
                'repairDate' => new DateTime('-31 day'),
                'returnDate' => new DateTime('-5 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_THIRD),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_THIRD),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
            [
                'defect' => 'Сам отключается',
                'preliminarilyList' => [],
                'devicePreview' => 'Отсутсвует задняя крышка',
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-20 day today'),
                'workPerformed' => 'Прошивка',
                'workDescription' => 'Вскрывали',
                'receiptStatus' => ReceiptStatusEnum::GUARANTEE_RETURN,
                'payRepairer' => 800,
                'priceRepair' => 1600,
                'priceSparePart' => 0,
                'repairDate' => new DateTime('-15 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_FOURTH),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_THIRD),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'deviceReturner' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'guaranteeReturnDescription' => 'Не работает слуховой динамик',
                'guaranteeReturnDate' => new DateTime('-1 day'),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
            [
                'defect' => 'Разбит экран',
                'preliminarilyList' => [PreliminarilyEnum::IS_STRIKE, PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Отвратительное',
                'prePrice' => 3500,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-9 day today'),
                'workPerformed' => 'Замена экрана',
                'workDescription' => 'Экран б/у',
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_SHOP,
                'payRepairer' => 1800,
                'priceRepair' => 3500,
                'priceSparePart' => 500,
                'repairDate' => new DateTime('-4 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_FIVE),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_FIVE),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_ADMIN),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_ADMIN),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_SECOND),
            ],
            [
                'defect' => 'Не заряжается. Заряжали напрямую от 220 вольт',
                'preliminarilyList' => [],
                'devicePreview' => 'Обугленный',
                'prePrice' => 2500,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-45 day today'),
                'workPerformed' => 'Замена материнской платы',
                'workDescription' => 'Экран б/у',
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                'payRepairer' => 480,
                'priceRepair' => 800,
                'priceSparePart' => 0,
                'repairDate' => new DateTime('-35 day'),
                'returnDate' => new DateTime('-5 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_SIX),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_SIX),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_ADMIN),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_ADMIN),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_SECOND),
            ],
            [
                'defect' => 'Отсутствует задняя крышка',
                'preliminarilyList' => [PreliminarilyEnum::IS_STRIKE, PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Отвратительное',
                'prePrice' => 1000,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-50 day today'),
                'workPerformed' => 'Установка задней крышки',
                'workDescription' => null,
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                'payRepairer' => 450,
                'priceRepair' => 1000,
                'priceSparePart' => 100,
                'repairDate' => new DateTime('-41 day'),
                'returnDate' => new DateTime('-40 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_SEVEN),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_SEVEN),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
            [
                'defect' => 'Не ловит сеть',
                'preliminarilyList' => [PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Сносно',
                'prePrice' => 3000,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-54 day today'),
                'workPerformed' => 'Установка задней крышки',
                'workDescription' => null,
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                'payRepairer' => 1400,
                'priceRepair' => 3000,
                'priceSparePart' => 200,
                'repairDate' => new DateTime('-45 day'),
                'returnDate' => new DateTime('-44 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_EIGHT),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_EIGHT),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
            [
                'defect' => 'Распух',
                'preliminarilyList' => [PreliminarilyEnum::IS_ENABLE],
                'devicePreview' => 'Торчит задняя панель',
                'prePrice' => 1200,
                'orderId' => $this->orderIdIncrement++,
                'orderDate' => new DateTime('-66 day today'),
                'workPerformed' => 'Установка задней крышки',
                'workDescription' => null,
                'receiptStatus' => ReceiptStatusEnum::REPAIR_SUCCESS_OWNER,
                'payRepairer' => 500,
                'priceRepair' => 1250,
                'priceSparePart' => 250,
                'repairDate' => new DateTime('-65 day'),
                'returnDate' => new DateTime('-65 day'),
                'deviceOwner' => $this->getReference(DeviceOwnerFixture::REFERENCE_DEVICE_OWNER_NINE),
                'device' => $this->getReference(DeviceFixture::REFERENCE_DEVICE_NINE),
                'receiptCreator' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'lastEditUser' => $this->getReference(UserFixture::REFERENCE_USER_SELLER),
                'repairer' => $this->getReference(UserFixture::REFERENCE_USER_REPAIRER_FIRST),
            ],
        ];

        foreach ($receiptParamsList as $receiptParams) {
            $receipt = new Receipt();

            foreach ($receiptParams as $fieldName => $receiptParam) {
                $setter = 'set' . ucfirst($fieldName);

                $receipt->$setter($receiptParam);
            }

            $manager->persist($receipt);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(): array
    {
        return [
            UserFixture::class,
            DeviceOwnerFixture::class,
            DeviceFixture::class,
        ];
    }
}
