<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Device;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

class DeviceFixture extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE_DEVICE_FIRST = 'reference_device_first';
    public const REFERENCE_DEVICE_SECOND = 'reference_device_second';
    public const REFERENCE_DEVICE_THIRD = 'reference_device_third';
    public const REFERENCE_DEVICE_FOUR = 'reference_device_four';
    public const REFERENCE_DEVICE_FIVE = 'reference_device_five';
    public const REFERENCE_DEVICE_SIX = 'reference_device_six';
    public const REFERENCE_DEVICE_SEVEN = 'reference_device_seven';
    public const REFERENCE_DEVICE_EIGHT = 'reference_device_eight';
    public const REFERENCE_DEVICE_NINE = 'reference_device_nine';

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $deviceParamsList = [
            self::REFERENCE_DEVICE_FIRST => [
                'brand' => 'iPhone',
                'model' => 'XR',
                'imei' => (string)random_int(111111111111111, 999999999999999),
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [
                    [
                        'brand' => 'iPhone',
                        'model' => 'A1357',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ]
                ]
            ],
            self::REFERENCE_DEVICE_SECOND => [
                'brand' => 'Huawei',
                'model' => 'MediaPad T3',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_PAD),
                'childDeviceList' => [
                    [
                        'brand' => 'Huawei',
                        'model' => 'AP32',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                ]
            ],
            self::REFERENCE_DEVICE_THIRD => [
                'brand' => 'Lenovo',
                'model' => 'IdeaPad S145-15AST',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_NOTEBOOK),
                'childDeviceList' => [
                    [
                        'brand' => 'Lenovo',
                        'model' => 'IdeaPad U330p',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                    [
                        'brand' => 'Lenovo',
                        'model' => 'L09M6Y21',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_BATTERY),
                    ],
                ]
            ],
            self::REFERENCE_DEVICE_FOUR => [
                'brand' => 'Samsung',
                'model' => 'Galaxy A70',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [
                    [
                        'brand' => 'Samsung',
                        'model' => 'EP-TA12',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                    [
                        'brand' => 'Samsung',
                        'model' => 'EB-B500AEBECRU',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_BATTERY),
                    ],
                ]
            ],
            self::REFERENCE_DEVICE_FIVE => [
                'brand' => 'Motorola',
                'model' => 'C200',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [],
            ],
            self::REFERENCE_DEVICE_SIX => [
                'brand' => 'Alcatel',
                'model' => '5033D',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [],
            ],
            self::REFERENCE_DEVICE_SEVEN => [
                'brand' => 'Motorola',
                'model' => 'E365',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [
                    [
                        'brand' => 'Motorola',
                        'model' => 'EP-133',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                ],
            ],
            self::REFERENCE_DEVICE_EIGHT => [
                'brand' => 'Samsung',
                'model' => 'C100',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [
                    [
                        'brand' => 'Samsung',
                        'model' => 'UU-13',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                ],
            ],
            self::REFERENCE_DEVICE_NINE => [
                'brand' => 'Sony',
                'model' => 'X1',
                'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_MOBILE_PHONE),
                'childDeviceList' => [
                    [
                        'brand' => 'Sony',
                        'model' => 'xx-11',
                        'serialNumber' => strtoupper(substr(sha1(random_bytes(10)), 0, 20)),
                        'deviceType' => $this->getReference(DeviceTypeFixture::REFERENCE_ENERGY_CHARGING),
                    ],
                ],
            ],
        ];

        foreach ($deviceParamsList as $reference => $deviceParams) {
            $parentDevice = new Device();

            foreach ($deviceParams['childDeviceList'] as $childDeviceParams) {
                $childDevice = new Device();

                foreach ($childDeviceParams as $fieldName => $childDeviceParam) {
                    $setter = 'set' . ucfirst($fieldName);

                    $childDevice->$setter($childDeviceParam);
                    $childDevice->setParentDevice($parentDevice);

                    $manager->persist($childDevice);
                }
            }

            unset($deviceParams['childDeviceList']);

            foreach ($deviceParams as $fieldName => $deviceParam) {
                $setter = 'set' . ucfirst($fieldName);

                $parentDevice->$setter($deviceParam);
            }

            $manager->persist($parentDevice);

            $this->setReference($reference, $parentDevice);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(): array
    {
        return [
            DeviceTypeFixture::class,
        ];
    }
}
