<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\DeviceType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DeviceTypeFixture extends Fixture
{
    public const REFERENCE_MOBILE_PHONE = 'reference_device_type_mobile_phone';
    public const REFERENCE_NOTEBOOK = 'reference_device_type_notebook';
    public const REFERENCE_PAD = 'reference_device_type_pad';
    public const REFERENCE_ENERGY_CHARGING = 'reference_device_type_energy_charging';
    public const REFERENCE_BATTERY = 'reference_device_type_battery';

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach ([
            self::REFERENCE_MOBILE_PHONE => 'Мобильный телефон',
            self::REFERENCE_NOTEBOOK => 'Ноутбук',
            self::REFERENCE_PAD => 'Планшет',
            self::REFERENCE_ENERGY_CHARGING => 'Зарядное устройство',
            self::REFERENCE_BATTERY => 'Аккумуляторная батарея',
        ] as $reference => $name) {
            $deviceType = new DeviceType();
            $deviceType->setName($name);

            $manager->persist($deviceType);

            $this->setReference($reference, $deviceType);
        }

        $manager->flush();
    }
}
