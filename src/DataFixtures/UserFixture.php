<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    public const REFERENCE_USER_ADMIN = 'reference_user_admin';
    public const REFERENCE_USER_REPAIRER_FIRST = 'reference_user_repairer_first';
    public const REFERENCE_USER_REPAIRER_SECOND = 'reference_user_repairer_second';
    public const REFERENCE_USER_SELLER = 'reference_user_seller';

    private UserPasswordEncoderInterface $encoder;

    /**
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach ([
            self::REFERENCE_USER_ADMIN => ['admin', 'Мистер Админ', ['ROLE_ADMIN'], 0.5],
            self::REFERENCE_USER_SELLER => ['seller', 'Гражданин Продавец', ['ROLE_SELLER']],
            self::REFERENCE_USER_REPAIRER_FIRST => ['repairer', 'Товарищ Ремонтник', ['ROLE_REPAIRER'], 0.5],
            self::REFERENCE_USER_REPAIRER_SECOND => ['repairer_second', 'Мистр ремонтер', ['ROLE_REPAIRER'], 0.6],
        ] as $reference => $params) {
            $user = new User();

            $password = $this->encoder->encodePassword($user, $params[0]);

            $user->setUsername($params[0]);
            $user->setSurname($params[1]);
            $user->setRoles($params[2]);
            $user->setPassword($password);
            $user->setActive(true);
            isset($params[3]) ? $user->setPercentRepairer($params[3]) : null;

            $manager->persist($user);

            $this->setReference($reference, $user);
        }

        $manager->flush();
    }
}
