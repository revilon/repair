<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\DeviceOwner;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DeviceOwnerFixture extends Fixture
{
    public const REFERENCE_DEVICE_OWNER_FIRST = 'reference_device_owner_first';
    public const REFERENCE_DEVICE_OWNER_SECOND = 'reference_device_owner_second';
    public const REFERENCE_DEVICE_OWNER_THIRD = 'reference_device_owner_third';
    public const REFERENCE_DEVICE_OWNER_FOURTH = 'reference_device_owner_fourth';
    public const REFERENCE_DEVICE_OWNER_FIVE = 'reference_device_owner_five';
    public const REFERENCE_DEVICE_OWNER_SIX = 'reference_device_owner_six';
    public const REFERENCE_DEVICE_OWNER_SEVEN = 'reference_device_owner_seven';
    public const REFERENCE_DEVICE_OWNER_EIGHT = 'reference_device_owner_eight';
    public const REFERENCE_DEVICE_OWNER_NINE = 'reference_device_owner_nine';

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach ([
            self::REFERENCE_DEVICE_OWNER_FIRST => [
                'Жора Невключаев',
                'address' => 'Пр. Пелевина 110',
                'email' => 'lol@kek.ru',
                'phone' => '89019043090',
            ],
            self::REFERENCE_DEVICE_OWNER_SECOND => [
                'Афоня Безремонтов',
                'phone' => '8 800 555 3535',
            ],
            self::REFERENCE_DEVICE_OWNER_THIRD => [
                'Игорь Микросхемов',
                'email' => 'micro@schema.com',
            ],
            self::REFERENCE_DEVICE_OWNER_FOURTH => [
                'Августин Термопастов',
            ],
            self::REFERENCE_DEVICE_OWNER_FIVE => [
                'Очередняров Талгат',
                'phone' => '+79878979988',
            ],
            self::REFERENCE_DEVICE_OWNER_SIX => [
                'Пастулатов Эскалоп',
                'phone' => '50 50 2',
            ],
            self::REFERENCE_DEVICE_OWNER_SEVEN => [
                'Александр Пистолетов',
                'phone' => '20 51 2',
            ],
            self::REFERENCE_DEVICE_OWNER_EIGHT => [
                'Владимир Епифанцев',
                'phone' => '188887777',
            ],
            self::REFERENCE_DEVICE_OWNER_NINE => [
                'Сергей Пахомов',
            ],
        ] as $reference => $params) {
            $deviceOwner = new DeviceOwner();

            $deviceOwner->setName($params[0]);
            $deviceOwner->setPhone($params['phone'] ?? null);
            $deviceOwner->setAddress($params['address'] ?? null);
            $deviceOwner->setEmail($params['email'] ?? null);

            $manager->persist($deviceOwner);

            $this->setReference($reference, $deviceOwner);
        }

        $manager->flush();
    }
}
