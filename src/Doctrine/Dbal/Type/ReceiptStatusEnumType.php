<?php

declare(strict_types=1);

namespace App\Doctrine\Dbal\Type;

use App\Enum\ReceiptStatusEnum;

class ReceiptStatusEnumType extends AbstractEnumType
{
    public const NAME = 'receipt_status_enum_type';

    /**
     * {@inheritdoc}
     */
    protected function getEnumClass(): string
    {
        return ReceiptStatusEnum::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
