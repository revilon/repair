<?php

declare(strict_types=1);

namespace App\Doctrine\Dbal\Type;

use App\Enum\PreliminarilyEnum;

class PreliminarilySetType extends AbstractSetType
{
    public const NAME = 'preliminarily_set_type';

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    protected function getEnumClass(): string
    {
        return PreliminarilyEnum::class;
    }
}
