<?php

declare(strict_types=1);

namespace App\Doctrine\Dbal\Type;

use App\Enum\GenderEnum;

class GenderEnumType extends AbstractEnumType
{
    public const NAME = 'gender_enum_type';

    /**
     * {@inheritdoc}
     */
    protected function getEnumClass(): string
    {
        return GenderEnum::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
