require('noty/lib/noty.css');
require('noty/lib/themes/bootstrap-v4.css');
require('jquery-typeahead/dist/jquery.typeahead.min.css');
require('chosen-js/chosen.min.css');

require('chosen-js');
require('jquery-typeahead');

jQuery(function ($) {
    let form_receipt = $('.page-receipt form');

    form_receipt.find('.preliminarilyList').chosen({
        no_results_text: 'Выберите из трех возможных вариантов',
        placeholder_text_multiple: form_receipt.find('.preliminarilyList').attr('placeholder')
    });

    form_receipt.find('.js-typeahead.full-name').typeahead({
        minLength: 1,
        maxItem: 10,
        dynamic: true,
        filter: false,
        cancelButton: false,
        source: {
            full_name_list: {
                ajax: {
                    url: form_receipt.find('.js-typeahead.full-name').data('callback'),
                    data: {
                        search_text: '{{query}}'
                    },
                    path: 'device_owner_name_list'
                }
            },
        }
    });

    form_receipt.find('.js-typeahead.address').typeahead({
        minLength: 1,
        maxItem: 10,
        dynamic: true,
        filter: false,
        cancelButton: false,
        source: {
            full_name_list: {
                ajax: {
                    url: form_receipt.find('.js-typeahead.address').data('callback'),
                    data: {
                        search_text: '{{query}}'
                    },
                    path: 'device_owner_address_list'
                }
            },
        }
    });

    form_receipt.on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    });
    form_receipt.on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    });

    let add_device_link = form_receipt.find('.add-device-link');
    let tab_device = form_receipt.find('.tab-device');
    let collection_holder = form_receipt.find('.add-device-collection-widget');
    add_device_link.on('click', function (e) {
        e.preventDefault();
        addDeviceForm(collection_holder);
    });

    if (collection_holder.data('widget-counter') === 0) {
        addDeviceForm(collection_holder);
    }

    tab_device.on('click', '.remove-device', function (e) {
        e.preventDefault();
        if (form_receipt.find('.tab-content').children().length === 1) {
            notyError('Должно остаться хотя бы одно устройство');
            return false;
        }

        let nav_link_id = $(this).closest('.tab-pane').attr('aria-labelledby');

        $(this).closest('.tab-pane').remove();
        form_receipt.find('#' + nav_link_id).closest('.nav-item').remove();
        form_receipt.find('.tab-device a.nav-link').first().tab('show');

        return false;
    });

    form_receipt.on('keyup', '.device-type', function (e) {
        let tab_link_id = $(e.target).closest('.tab-pane').attr('aria-labelledby');
        form_receipt.find('#' + tab_link_id).text($(e.target).val());
    });

    function addDeviceForm(collection_holder)
    {
        let prototype = collection_holder.data('prototype');
        let prototype_tab = collection_holder.data('prototype-tab');
        let index = collection_holder.data('widget-counter');
        let new_form = prototype.replace(/__name__/g, index);
        let new_form_widget = $(new_form);
        let new_form_tab = $(prototype_tab.replace(/__name__/g, index));
        let tabs = collection_holder.closest('.tab-device').find('.nav-tabs');
        collection_holder.data('widget-counter', index + 1);

        tabs.append(new_form_tab);
        collection_holder.append(new_form_widget);
        new_form_tab.find('a').tab('show');

        let device_card = form_receipt.find('#receipt_form_DeviceCollection_' + index);

        $.typeahead({
            input: device_card.find('.js-typeahead.device-type'),
            minLength: 0,
            searchOnFocus: true,
            maxItem: 10,
            filter: false,
            cancelButton: false,
            source: {
                data: device_card.find('.js-typeahead.device-type').data('choices')
            },
            callback: {
                onClickAfter: function (node, a, item, event) {
                    let tab_link_id = node.closest('.tab-pane').attr('aria-labelledby');

                    form_receipt.find('#' + tab_link_id).text(node.val());
                }
            }
        });

        $.typeahead({
            input: device_card.find('.js-typeahead.device-brand'),
            minLength: 1,
            maxItem: 10,
            dynamic: true,
            filter: false,
            cancelButton: false,
            source: {
                brand_list: {
                    ajax: {
                        url: device_card.find('.js-typeahead.device-brand').data('callback'),
                        data: {
                            search_text: '{{query}}',
                            device_type: function () {
                                return device_card.find('.device-type').val()
                            },
                        },
                        path: 'device_brand_list'
                    }
                },
            }
        });

        $.typeahead({
            input: device_card.find('.js-typeahead.device-model'),
            minLength: 1,
            maxItem: 10,
            dynamic: true,
            filter: false,
            cancelButton: false,
            source: {
                model_list: {
                    ajax: {
                        url: device_card.find('.js-typeahead.device-model').data('callback'),
                        data: {
                            search_text: '{{query}}',
                            device_type: function () {
                                return device_card.find('.device-type').val()
                            },
                            brand: function () {
                                return device_card.find('.js-typeahead.device-brand').val()
                            }
                        },
                        path: 'device_model_list'
                    }
                },
            }
        });

        $.typeahead({
            input: device_card.find('.js-typeahead.device-imei'),
            minLength: 1,
            maxItem: 10,
            dynamic: true,
            filter: false,
            cancelButton: false,
            source: {
                model_list: {
                    ajax: {
                        url: device_card.find('.js-typeahead.device-imei').data('callback'),
                        data: {
                            search_text: '{{query}}',
                            device_type: function () {
                                return device_card.find('.device-type').val()
                            },
                            brand: function () {
                                return device_card.find('.device-brand').val()
                            },
                            model: function () {
                                return device_card.find('.device-model').val()
                            }
                        },
                        path: 'device_imei_list'
                    }
                },
            }
        });

        $.typeahead({
            input: device_card.find('.js-typeahead.device-serial-number'),
            minLength: 1,
            maxItem: 10,
            dynamic: true,
            filter: false,
            cancelButton: false,
            source: {
                model_list: {
                    ajax: {
                        url: device_card.find('.js-typeahead.device-serial-number').data('callback'),
                        data: {
                            search_text: '{{query}}',
                            device_type: function () {
                                return device_card.find('.device-type').val()
                            },
                            brand: function () {
                                return device_card.find('.device-brand').val()
                            },
                            model: function () {
                                return device_card.find('.device-model').val()
                            },
                            imei: function () {
                                return device_card.find('.device-imei').val()
                            }
                        },
                        path: 'device_serial_number_list'
                    }
                },
            }
        });
    }

    form_receipt.submit(function (e) {
        e.preventDefault();
        let form = $(e.target);
        let button_save_loader = form.find('.button-save-loader');
        let button_save = form.find('.button-save');
        let button_add_device_link = form.find('.add-device-link');
        let button_remove_device = form.find('.remove-device');

        button_save.hide(0, function () {
            button_save_loader.show();
        });
        button_add_device_link.attr('disabled', 'disabled');
        button_remove_device.attr('disabled', 'disabled');

        $.ajax({
            method: form.attr('method'),
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            form_receipt.find('.receipt-number').text(data['receipt_order_id']);
            form_receipt.find('.button-receipt-print').data('order-id', data['receipt_order_id']);

            button_save_loader.hide(0, function () {
                form_receipt.find('.button-receipt-print').show();
            });

            notySuccess('Успех!');
        }).fail(function (jqXHR) {
            formErrorShow(jqXHR['responseJSON']['error_list']);

            if (collection_holder.find('.is-invalid')) {
                let device_nav_link_id = collection_holder.find('.is-invalid').closest('.tab-pane').attr('aria-labelledby');

                form_receipt.find('#' + device_nav_link_id).tab('show');
            }

            button_add_device_link.removeAttr('disabled');
            button_remove_device.removeAttr('disabled');
            button_save_loader.hide(0, function () {
                button_save.show();
            });

            notyError(jqXHR['responseJSON']['error_message']);
        });
    });

    form_receipt.find('.button-receipt-print').click(function (e) {
        let button_print = $(e.target);
        let href = button_print.data('href');
        let order_id = button_print.data('order-id');
        let url = href + '/' + order_id;

        window.open(url, '_blank');
    });
});