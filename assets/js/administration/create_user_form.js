require('chosen-js/chosen.min.css');

require('chosen-js');

jQuery(function ($) {
    let page_administration = $('.page-administration');
    let card_create_user = page_administration.find('.card-create-user');
    let form_create_user = card_create_user.find('form');
    let submit_button_create_user = card_create_user.find('.button-create-user');
    let loader_button_create_user = card_create_user.find('.button-loader');

    form_create_user.find('.role-list').chosen({
        no_results_text: 'Выберите роль нового пользователя',
        placeholder_text_multiple: form_create_user.find('.role-list').attr('placeholder')
    });

    submit_button_create_user.click(function () {
        form_create_user.submit();
    });

    form_create_user.submit(function (e) {
        e.preventDefault();

        submit_button_create_user.hide(0, function () {
            loader_button_create_user.show();
        });

        $.ajax({
            method: form_create_user.attr('method'),
            url: form_create_user.attr('action'),
            data: form_create_user.serialize(),
            dataType: 'json',
        }).done(function (data) {
            notySuccess('Успех!');

            window.location.reload();
        }).fail(function (jqXHR) {
            loader_button_create_user.hide(0, function () {
                submit_button_create_user.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);

            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
