require('chosen-js/chosen.min.css');

require('chosen-js');

jQuery(function ($) {
    let page_administration = $('.page-administration-edit-user');
    let card_edit_user = page_administration.find('.card-edit-user');
    let form_edit_user = card_edit_user.find('form');
    let submit_button_edit_user = card_edit_user.find('.button-edit-user');
    let loader_button_edit_user = card_edit_user.find('.button-loader');

    form_edit_user.find('.role-list').chosen({
        no_results_text: 'Выберите роль пользователя',
        placeholder_text_multiple: form_edit_user.find('.role-list').attr('placeholder')
    });

    submit_button_edit_user.click(function () {
        form_edit_user.submit();
    });

    form_edit_user.submit(function (e) {
        e.preventDefault();

        submit_button_edit_user.hide(0, function () {
            loader_button_edit_user.show();
        });

        $.ajax({
            method: form_edit_user.attr('method'),
            url: form_edit_user.attr('action'),
            data: form_edit_user.serialize(),
            dataType: 'json',
        }).done(function (data) {
            notySuccess('Успех!');

            window.location.href = form_edit_user.data('redirect-url');
        }).fail(function (jqXHR) {
            loader_button_edit_user.hide(0, function () {
                submit_button_edit_user.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);
            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
