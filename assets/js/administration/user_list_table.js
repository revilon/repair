require('datatables.net-bs4/css/dataTables.bootstrap4.min.css');

require('datatables.net-bs4');

jQuery(function ($) {
    let page_administration = $('.page-administration');
    let user_list_table = page_administration.find('#user_list_table');

    user_list_table.DataTable({
        responsive: true,
        scrollX: true,
        ordering: false,
        bAutoWidth: false,
        paging: false,
        searching: false,
        info: false,
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json',
        },
    });

    user_list_table.find('.button-switch-active-user').click(function () {
        let button_switch_active = $(this);

        button_switch_active.hide(0, function () {
            button_switch_active.closest('th').find('.button-loader').show();
        });

        $.ajax({
            method: 'POST',
            url: button_switch_active.data('callback'),
            dataType: 'json',
        }).done(function (data) {
            notySuccess('Успех!');

            window.location.reload();
        }).fail(function (jqXHR) {
            button_switch_active.closest('th').find('.button-loader').hide(0, function () {
                button_switch_active.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);
            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
