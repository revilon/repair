jQuery(function ($) {
    let form = $('.login-form');

    form.submit(function (e) {
        e.preventDefault();

        let form = $(e.target);
        let button_login = form.find('.button-login');
        let button_login_loader = form.find('.button-login-loader');

        button_login.hide(0, function () {
            button_login_loader.show();
        });

        $.ajax({
            method: form.attr('method'),
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            notySuccess('Успех! Сейчас вы будете перенаправлены в кабинет.');

            window.location = data['redirect_url'];
        }).fail(function (jqXHR) {
            formErrorShow(jqXHR['responseJSON']['error_list']);

            button_login_loader.hide(0, function () {
                button_login.show();
            });

            notyError(jqXHR['responseJSON']['error_message']);
        })
    })
});