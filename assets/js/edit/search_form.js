jQuery(function ($) {
    let page_edit = $('.page-edit');
    let form_search = page_edit.find('form.search-form');
    let receipt_ago = page_edit.find('.receipt-ago');

    let search_action = function (e) {
        e.preventDefault();

        let button = $(e.target);

        button.closest('form').find('.button-search').hide(0, function () {
            button.closest('form').find('.button-search-loader').show();
        });
        button.closest('form').submit();
    };

    form_search.find('.input-search').on('keydown', function (e) {
        if (e.keyCode === 13) {
            search_action(e);
        }
    });

    form_search.find('.button-search').on('click', search_action);

    let input_search = form_search.find('.input-search');
    let input_search_val = parseInt(input_search.val());
    let ago_interval = receipt_ago.data('interval') * 1000;

    if (input_search_val) {
        setInterval(function () {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: receipt_ago.data('callback'),
                data: {orderId: input_search_val}
            }).done(function (data) {
                page_edit.find('.text-ago').text(data['ago_text']);
            });
        }, ago_interval)
    }
});