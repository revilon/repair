require('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css');
require('noty/lib/noty.css');
require('noty/lib/themes/bootstrap-v4.css');

const c3 = require('c3');
const d3 = require('d3');
const Noty = require('noty');
require('bootstrap-datepicker');
require('bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js');

jQuery(function ($) {
    let profile_donut = $('.page-profile #c3chart_donut');
    let c3chart_donut_loader = $('.page-profile .c3chart_donut-loader');

    let c3chart_donut_form = $('.page-profile .form-c3chart_donut.needs-validation');
    let repairer_pay = profile_donut.data('repairer-pay');

    let title = profile_donut.data('repairer-pay-total') + ' руб';
    let sub_title = profile_donut.data('month');

    if (profile_donut.length) {
        let chart = c3.generate({
            bindto: '.page-profile #c3chart_donut',
            data: {
                columns: repairer_pay,
                type: 'donut',
            },
            donut: {
                title: title,
            },
            legend: {
                show: false
            },
            tooltip:{
                format: {
                    title: function (x, index, lol, kek, haha) {
                        return repairer_pay[index][1] + ' руб';
                    }
                }
            }
        });

        let profile_donut_title = d3.select('.page-profile #c3chart_donut text.c3-chart-arcs-title');

        profile_donut_title.html('');

        if (repairer_pay.length > 0) {
            profile_donut_title.insert('tspan').text(title).attr('dy', 0).attr('x', 0).attr('class', 'font-weight-bold h3');
            profile_donut_title.insert('tspan').text(sub_title).attr('dy', 20).attr('x', 0);
        } else {
            profile_donut_title.insert('tspan').text('Нет данных').attr('dy', 0).attr('x', 0).attr('class', 'font-weight-bold h3');
        }


        (function () {
            'use strict';
            window.addEventListener('load', function () {
                let forms = document.querySelectorAll('.page-profile .form-c3chart_donut.needs-validation');
                let validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        form.classList.add('was-validated');

                        if (form.checkValidity() === false) {
                            return;
                        }

                        c3chart_donut_form.find('button[type=submit]').hide(0, function () {
                            c3chart_donut_loader.show();
                        });


                        $.ajax({
                            method: c3chart_donut_form.attr('method'),
                            dataType: 'text',
                            url: c3chart_donut_form.attr('action'),
                            data: c3chart_donut_form.serialize()
                        }).done(function (response) {
                            let data = JSON.parse(response);
                            let columns = data['repairer_pay'];
                            let repairer_pay_total = data['repairer_pay_total'];

                            if (columns.length === 0) {
                                profile_donut.find('.c3-chart-arcs-title').hide(100, function () {
                                    profile_donut_title.html('');
                                    profile_donut_title.insert('tspan').text('Нет данных').attr('dy', 0).attr('x', 0).attr('class', 'font-weight-bold h3');
                                    $(this).show(100);
                                });

                                chart.load({
                                    unload: true,
                                    columns: []
                                });

                                c3chart_donut_loader.hide(0, function () {
                                    c3chart_donut_form.find('button[type=submit]').show();
                                });

                                return;
                            }

                            profile_donut.find('.c3-chart-arcs-title').hide(100, function () {
                                let title = repairer_pay_total + ' руб';

                                profile_donut_title.html('');
                                profile_donut_title.insert('tspan').text(title).attr('dy', 0).attr('x', 0).attr('class', 'font-weight-bold h3');
                                $(this).show(100);
                            });
                            chart.load({
                                unload: true,
                                columns: columns
                            });

                            repairer_pay = columns;

                            c3chart_donut_loader.hide(0, function () {
                                c3chart_donut_form.find('button[type=submit]').show();
                            });
                        }).fail(function (data) {
                            notyError('Ошибка!');

                            c3chart_donut_loader.hide(0, function () {
                                c3chart_donut_form.find('button[type=submit]').show();
                            });
                        });
                    }, false);
                });
            }, false);
        })();
    }
});
