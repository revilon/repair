const c3 = require('c3');

jQuery(function () {
    let profile_c3chart = $('.page-profile #c3chart_stacked');

    let pay_data = profile_c3chart.data('repairer-efficiency');
    let user = profile_c3chart.data('repairer-name');

    if (profile_c3chart.length) {
        let chart = c3.generate({
            bindto: '.page-profile #c3chart_stacked',
            data: {
                columns: [
                    [user, 0, 0, 0]
                ],
                type: 'bar',
                groups: [
                    [user]
                ],
                order: null,
                colors: {
                    [user]: '#5969ff'
                }
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                        return value + '%';
                    }
                },
            },
            axis: {
                y: {
                    max: 100,
                    min: -100,
                    padding: 0,
                    label: {
                        text: '%',
                        position: 'outer-middle'
                    },
                    show: true
                },
                x: {
                    type: 'category',
                    categories: [
                        pay_data[0]['month'] + ' (' + pay_data[0]['pay'] + ' руб)',
                        pay_data[1]['month'] + ' (' + pay_data[1]['pay'] + ' руб)',
                        pay_data[2]['month'] + ' (' + pay_data[2]['pay'] + ' руб)'
                    ],
                    show: true
                }
            },
            grid: {
                y: {
                    lines: [{value: 0}]
                }
            },
            legend: {
                show: false
            }
        });

        setTimeout(function () {
            chart.load({
                columns: [
                    [user, pay_data[0]['percent'], pay_data[1]['percent'], pay_data[2]['percent']],
                ]
            });
        }, 1000);
    }
});
