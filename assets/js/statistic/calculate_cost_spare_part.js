jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let calculate_cost_spare_part = page_statistic.find('.card-calculate-cost-spare-part');
    let calculate_cost_spare_part_form = calculate_cost_spare_part.find('form');
    let calculate_cost_spare_part_submit_button = calculate_cost_spare_part.find('.button-calculate-cost-spare-part');
    let calculate_cost_spare_part_loader = calculate_cost_spare_part.find('.button-loader');

    calculate_cost_spare_part_submit_button.click(function () {
        calculate_cost_spare_part_form.submit();
    });

    calculate_cost_spare_part_form.submit(function (e) {
        e.preventDefault();

        calculate_cost_spare_part_submit_button.hide(0, function () {
            calculate_cost_spare_part_loader.show();
        });

        $.ajax({
            method: calculate_cost_spare_part_form.attr('method'),
            url: calculate_cost_spare_part_form.attr('action'),
            data: calculate_cost_spare_part_form.serialize(),
            dataType: 'json',
        }).done(function (data) {
            calculate_cost_spare_part_loader.hide(0, function () {
                calculate_cost_spare_part_submit_button.show();
            });

            calculate_cost_spare_part.find('.card-body .badge').remove();

            calculate_cost_spare_part.find('.card-body').append($('<span>', {
                class: 'badge badge-primary badge-pill pay',
                text: data + ' руб.'
            }));
        }).fail(function (jqXHR) {
            calculate_cost_spare_part_loader.hide(0, function () {
                calculate_cost_spare_part_submit_button.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);

            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
