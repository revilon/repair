const c3 = require('c3');

jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let total_price_chart_element = page_statistic.find('#total_price_chart');

    if (!total_price_chart_element.length) {
        return;
    }

    $.ajax({
        dataType: 'json',
        url: total_price_chart_element.data('callback'),
    }).done(function (data) {
        page_statistic.find('.card-total-price .card-body-status').remove();
        page_statistic.find('.card-total-price .card-body-total-price-chart').show();
        let total_price_column = data['total_price_list'].slice(0);
        total_price_column.unshift('Общая стоимость ремонтов');

        c3.generate({
            bindto: total_price_chart_element.get(0),
            data: {
                columns: [
                    total_price_column
                ]
            },
            tooltip: {
                format: {
                    title: function (d) {
                        return data['month_list'][d];
                    }
                },
                contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                    let $$ = this, config = $$.config,
                        titleFormat = config.tooltip_format_title || defaultTitleFormat,
                        nameFormat = config.tooltip_format_name || function (name) {
                            return name;
                        },
                        valueFormat = config.tooltip_format_value || defaultValueFormat,
                        text, i, title, value, name, bgcolor;
                    for (i = 0; i < d.length; i++) {
                        if (! (d[i] && (d[i].value || d[i].value === 0))) {
                            continue;
                        }

                        if (!text) {
                            title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                            text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                        }

                        name = nameFormat(d[i].name);
                        value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
                        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

                        text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
                        text += "<td class='value'>" + data['total_price_format_list'][d[i].x] + " руб (" + data['efficiency_list'][d[i].x] + "%)</td>";
                        text += "</tr>";
                    }
                    return text + "</table>";
                }
            },
            zoom: {
                enabled: true
            },
            axis: {
                y: {
                    show: true,
                    label: {
                        position: 'outer-middle',
                        text: 'руб',
                    },
                },
                x: {
                    show: true,
                    type: 'category',
                    categories: data['month_short_list'],
                }
            }
        });
    }).fail(function (jqXHR) {
        page_statistic.find('.card-total-price .card-body-status .spinner-border').hide(function () {
            page_statistic.find('.card-total-price .card-body-status .error-load').show();
        });

        notyError(jqXHR['responseJSON']['error_message']);
    });
});
