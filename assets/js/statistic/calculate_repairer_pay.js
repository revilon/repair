jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let calculate_repairer_pay = page_statistic.find('.card-calculate-repairer-pay');
    let calculate_repairer_pay_form = calculate_repairer_pay.find('form');
    let calculate_repairer_pay_submit_button = calculate_repairer_pay.find('.button-calculate-repairer-pay');
    let calculate_repairer_pay_loader = calculate_repairer_pay.find('.button-loader');

    calculate_repairer_pay_submit_button.click(function () {
        calculate_repairer_pay_form.submit();
    });

    let calculate_repairer_pay_submit = function (e) {
        e.preventDefault();

        calculate_repairer_pay_submit_button.hide(0, function () {
            calculate_repairer_pay_loader.show();
        });

        $.ajax({
            method: calculate_repairer_pay_form.attr('method'),
            url: calculate_repairer_pay_form.attr('action'),
            data: calculate_repairer_pay_form.serialize(),
            dataType: 'json',
        }).done(function (data) {
            calculate_repairer_pay_loader.hide(0, function () {
                calculate_repairer_pay_submit_button.show();
            });

            calculate_repairer_pay.find('.list-group .list-group-item').remove();

            for (let item in data) {
                if (!data.hasOwnProperty(item)) {
                    continue;
                }

                calculate_repairer_pay.find('.list-group').append($('<li>', {
                    class: 'list-group-item d-flex justify-content-between align-items-center',
                    html: [
                        data[item]['repairerName'],
                        $('<span>', {
                            class: 'badge badge-primary badge-pill pay',
                            text: data[item]['repairerPay'] + ' руб.'
                        })
                    ]
                }));
            }
        }).fail(function (jqXHR) {
            calculate_repairer_pay_loader.hide(0, function () {
                calculate_repairer_pay_submit_button.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);

            notyError(jqXHR['responseJSON']['error_message']);
        });
    };

    calculate_repairer_pay_form.submit(calculate_repairer_pay_submit)
});
