jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let calculate_total_price = page_statistic.find('.card-calculate-total-price');
    let calculate_total_price_form = calculate_total_price.find('form');
    let calculate_total_price_submit_button = calculate_total_price.find('.button-calculate-total-price');
    let calculate_total_price_loader = calculate_total_price.find('.button-loader');

    calculate_total_price_submit_button.click(function () {
        calculate_total_price_form.submit();
    });

    calculate_total_price_form.submit(function (e) {
        e.preventDefault();

        calculate_total_price_submit_button.hide(0, function () {
            calculate_total_price_loader.show();
        });

        $.ajax({
            method: calculate_total_price_form.attr('method'),
            url: calculate_total_price_form.attr('action'),
            data: calculate_total_price_form.serialize(),
            dataType: 'json',
        }).done(function (data) {
            calculate_total_price_loader.hide(0, function () {
                calculate_total_price_submit_button.show();
            });

            calculate_total_price.find('.card-body .badge').remove();

            calculate_total_price.find('.card-body').append($('<span>', {
                class: 'badge badge-primary badge-pill pay',
                text: data + ' руб.'
            }));
        }).fail(function (jqXHR) {
            calculate_total_price_loader.hide(0, function () {
                calculate_total_price_submit_button.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);

            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
