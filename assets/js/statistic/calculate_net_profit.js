jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let calculate_net_profit = page_statistic.find('.card-calculate-net-profit');
    let calculate_net_profit_form = calculate_net_profit.find('form');
    let calculate_net_profit_submit_button = calculate_net_profit.find('.button-calculate-net-profit');
    let calculate_net_profit_loader = calculate_net_profit.find('.button-loader');

    calculate_net_profit_submit_button.click(function () {
        calculate_net_profit_form.submit();
    });

    calculate_net_profit_form.submit(function (e) {
        e.preventDefault();

        calculate_net_profit_submit_button.hide(0, function () {
            calculate_net_profit_loader.show();
        });

        $.ajax({
            method: calculate_net_profit_form.attr('method'),
            url: calculate_net_profit_form.attr('action'),
            data: calculate_net_profit_form.serialize(),
            dataType: 'json',
        }).done(function (data) {
            calculate_net_profit_loader.hide(0, function () {
                calculate_net_profit_submit_button.show();
            });

            calculate_net_profit.find('.card-body .badge').remove();

            calculate_net_profit.find('.card-body').append($('<span>', {
                class: 'badge badge-primary badge-pill pay',
                text: data + ' руб.'
            }));
        }).fail(function (jqXHR) {
            calculate_net_profit_loader.hide(0, function () {
                calculate_net_profit_submit_button.show();
            });

            formErrorShow(jqXHR['responseJSON']['error_list']);

            notyError(jqXHR['responseJSON']['error_message']);
        });
    });
});
