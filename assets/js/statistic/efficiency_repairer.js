const c3 = require('c3');

jQuery(function ($) {
    let page_statistic = $('.page-statistic');
    let efficiency_repairer_chart_element = page_statistic.find('#efficiency_repairer_chart');

    if (!efficiency_repairer_chart_element.length) {
        return;
    }

    $.ajax({
        dataType: 'json',
        url: efficiency_repairer_chart_element.data('callback'),
    }).done(function (data) {
        let repairer_name_list = data['repairer_name_list'];

        page_statistic.find('.card-efficiency-repairer .card-body-status').remove();
        page_statistic.find('.card-efficiency-repairer .card-body-efficiency-repairer-chart').show();

        c3.generate({
            bindto: efficiency_repairer_chart_element.get(0),
            data: {
                columns: data['efficiency_repairer_list'],
                type: 'bar',
                order: null,
            },
            tooltip: {
                contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                    let $$ = this, config = $$.config,
                        titleFormat = config.tooltip_format_title || defaultTitleFormat,
                        nameFormat = config.tooltip_format_name || function (name) {
                            return name;
                        },
                        valueFormat = config.tooltip_format_value || defaultValueFormat,
                        text, i, title, value, name, bgcolor;
                    for (i = 0; i < d.length; i++) {
                        if (! (d[i] && (d[i].value || d[i].value === 0))) {
                            continue;
                        }

                        if (!text) {
                            title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                            text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                        }

                        name = nameFormat(d[i].name);
                        value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
                        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

                        let repairer_name = repairer_name_list[name];

                        text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
                        text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + repairer_name + "</td>";
                        text += "<td class='value'>" + data['repairer_pay_list'][name][d[i].x] + " руб (" + value + "%)</td>";
                        text += "</tr>";
                    }
                    return text + "</table>";
                }
            },
            axis: {
                y: {
                    label: {
                        text: '%',
                        position: 'outer-middle'
                    },
                    show: true
                },
                x: {
                    type: 'category',
                    categories: data['month_list'],
                    show: true
                }
            },
            grid: {
                y: {
                    lines: [{value: 0}]
                }
            },
            legend: {
                show: false
            }
        });
    }).fail(function (jqXHR) {
        page_statistic.find('.card-efficiency-repairer .card-body-status .spinner-border').hide(function () {
            page_statistic.find('.card-efficiency-repairer .card-body-status .error-load').show();
        });

        notyError(jqXHR['responseJSON']['error_message']);
    });
});