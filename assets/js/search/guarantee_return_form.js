jQuery(function ($) {
    let page_repairer = $('.page-search');

    page_repairer.find('.guarantee-return-form').submit(function (e) {
        e.preventDefault();

        let form = $(e.target);
        let button_save = form.find('.button-guarantee-return-save');
        let button_save_loader = form.find('.button-guarantee-return-save-loader');

        button_save.hide(0, function () {
            button_save_loader.show();
        });

        $.ajax({
            method: form.attr('method'),
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            notySuccess('Успех!');

            window.location.reload();
        }).fail(function (jqXHR) {
            formErrorShow(jqXHR['responseJSON']['error_list']);

            button_save_loader.hide(0, function () {
                button_save.show();
            });

            notyError(jqXHR['responseJSON']['error_message']);
        })
    });
});