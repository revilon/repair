jQuery(function ($) {
    let page_repairer = $('.page-search');
    let receipt_ago = page_repairer.find('.receipt-ago');

    let search_action = function (e) {
        e.preventDefault();

        let button = $(e.target);

        button.closest('form').find('.button-search').hide(0, function () {
            button.closest('form').find('.button-search-loader').show();
        });
        button.closest('form').submit();
    };

    page_repairer.find('form .input-search').on('keydown', function (e) {
        if (e.keyCode === 13) {
            search_action(e);
        }
    });

    page_repairer.find('form .button-search').on('click', search_action);

    let input_search = page_repairer.find('.input-search');
    let input_search_val = parseInt(input_search.val());
    let ago_interval = receipt_ago.data('interval') * 1000;

    if (input_search_val) {
        setInterval(function () {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: receipt_ago.data('callback'),
                data: {orderId: input_search_val}
            }).done(function (data) {
                page_repairer.find('.text-ago').text(data['ago_text']);
            });
        }, ago_interval)
    }
});