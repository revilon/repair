require('datatables.net-bs4/css/dataTables.bootstrap4.min.css');
require('datatables.mark.js/dist/datatables.mark.min.css');

require('../../../node_modules/datatables.mark.js/dist/datatables.mark');
require('datatables.net-bs4');

jQuery(function ($) {
    let smart_table = $('#table_smart_search');

    smart_table.DataTable({
        responsive: true,
        scrollX: true,
        mark: true,
        bAutoWidth: false,
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: smart_table.data('callback'),
        columnDefs: [
            {targets: [0, 1, -1], searchable: false},
            {targets: -1, data: null, defaultContent: '<a href="#" class="btn btn-primary button-receipt-details">Подробнее</a>'},
        ],
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json',
        },
    });

    smart_table.on('draw.dt', function () {
        $('.button-receipt-details').each(function (index, item) {
            let element = $(item);
            let order_link_details = element.closest('tr').data('order-link-details');

            element.attr('href', order_link_details);
        })
    });

    smart_table.on('preInit.dt', function () {
        $('#table_smart_search_processing').addClass('badge badge-info');
    });
});
