jQuery(function ($) {
    let page_repairer = $('.page-repairer');

    page_repairer.find('.repairer-work-form').submit(function (e) {
        e.preventDefault();

        let form = $(e.target);
        let button_save = form.find('.button-save');
        let button_save_loader = form.find('.button-save-loader');

        button_save.hide(0, function () {
            button_save_loader.show();
        });

        $.ajax({
            method: form.attr('method'),
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            notySuccess('Успех!');

            window.location.reload();
        }).fail(function (jqXHR) {
            formErrorShow(jqXHR['responseJSON']['error_list']);

            button_save_loader.hide(0, function () {
                button_save.show();
            });

            notyError(jqXHR['responseJSON']['error_message']);
        })
    });

    let pay_repairer_input = page_repairer.find('.pay-repairer-input');
    let price_repair_input = page_repairer.find('.price-repair-input');
    let price_spare_part_input = page_repairer.find('.price-spare-part-input');

    let calculatePayRepairer = function () {
        let repairer_percent = pay_repairer_input.data('repairer-percent');
        let price_repair = price_repair_input.val() || 0;
        let price_spare_part = price_spare_part_input.val() || 0;

        let pay_repairer = Math.round((price_repair - price_spare_part) * repairer_percent);

        if (pay_repairer < 0) {
            pay_repairer = 0;
        }

        pay_repairer_input.val(pay_repairer);
    };

    price_repair_input.keyup(calculatePayRepairer);
    price_spare_part_input.keyup(calculatePayRepairer);
});