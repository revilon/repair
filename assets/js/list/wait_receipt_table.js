require('datatables.net-bs4/css/dataTables.bootstrap4.min.css');
require('datatables.mark.js/dist/datatables.mark.min.css');

require('../../../node_modules/datatables.mark.js/dist/datatables.mark');
require('datatables.net-bs4');

jQuery(function ($) {
    let receipt_wait_table = $('#receipts-wait');

    receipt_wait_table.DataTable({
        responsive: true,
        scrollX: true,
        mark: true,
        bAutoWidth: false,
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: receipt_wait_table.data('callback'),
        columnDefs: [
            {targets: [-1, 1, 3, 4, 5, 6, 7, 8], searchable: false},
            {targets: -1, data: null, defaultContent: '<a href="#" class="btn btn-primary button-receipt-details">Подробнее</a>'}
        ],
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json',
        },
    });

    receipt_wait_table.on('draw.dt', function () {
        $('.button-receipt-details').each(function (index, item) {
            let element = $(item);
            let order_link_details = element.closest('tr').data('order-link-details');

            element.attr('href', order_link_details);
        })
    });

    receipt_wait_table.on('preInit.dt', function () {
        $('#receipts-wait_processing').addClass('badge badge-info');
    });
});
