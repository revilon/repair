require('bootstrap/dist/css/bootstrap.min.css');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('c3/c3.min.css');
require('../../vendor/puikinsh/concept/assets/libs/css/style.css');
require('../css/custom.css');

require('bootstrap');
const $ = require('jquery');
require('./profile/chart.js');
require('./profile/calculate_pay.js');
require('./receipt/receipt_form.js');
require('./repairer/search.js');
require('./repairer/work_form.js');
require('./auth/auth_form.js');
require('./search/search_form.js');
require('./search/guarantee_return_form.js');
require('./list/all_receipt_table.js');
require('./list/wait_receipt_table.js');
require('./edit/search_form');
require('./edit/receipt_form.js');
require('./statistic/efficiency_repairer.js');
require('./statistic/calculate_repairer_pay.js');
require('./statistic/net_profit_chart.js');
require('./statistic/calculate_net_profit.js');
require('./statistic/cost_spare_part_chart.js');
require('./statistic/calculate_cost_spare_part.js');
require('./statistic/total_price_chart.js');
require('./statistic/calculate_total_price.js');
require('./search/table_smart_search');
require('./administration/create_user_form.js');
require('./administration/user_list_table.js');
require('./administration/edit_user_form.js');
const Noty = require('noty');

global.$ = global.jQuery = $;

global.notyError = function (message) {
    new Noty({
        type: 'error',
        layout: 'topRight',
        theme: 'bootstrap-v4',
        text: message,
        timeout: '4000',
        closeWith: ['click']
    }).show();
};
global.notySuccess = function (message) {
    new Noty({
        type: 'success',
        layout: 'topRight',
        theme: 'bootstrap-v4',
        text: message,
        timeout: '4000',
        closeWith: ['click']
    }).show();
};

$(function ($) {
    $('.datepicker-input').datepicker({
        format: 'dd.mm.yyyy',
        language: 'ru',
        orientation: 'bottom',
    });
});

global.formErrorShow = function (error_list) {
    if (!error_list) {
        return;
    }

    $('#' + Object.keys(error_list)[0]).closest('form').find('.is-invalid').removeClass('is-invalid');

    for (let input_id in error_list) {
        if (!error_list.hasOwnProperty(input_id)) {
            continue;
        }

        let input = $('#' + input_id);

        input.addClass('is-invalid');
        error_list[input_id].forEach(function (item) {
            input.closest('.input-group,.form-group').find('.invalid-feedback').remove();
            input.closest('.input-group,.form-group').append($('<div>', {
                'class': 'invalid-feedback',
                'text': item
            }));
        });
    }
};
